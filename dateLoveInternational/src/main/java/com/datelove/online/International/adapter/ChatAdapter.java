package com.datelove.online.International.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.activity.BuyServiceActivity;
import com.datelove.online.International.activity.PictureBrowseActivity;
import com.datelove.online.International.activity.UserInfoDetailActivity;
import com.datelove.online.International.bean.BaseModel;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.Message;
import com.datelove.online.International.bean.QaAnswer;
import com.datelove.online.International.bean.QaQuestion;
import com.datelove.online.International.bean.WriteMsg;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.MessageChangedEvent;
import com.datelove.online.International.event.RefreshChatEvent;
import com.datelove.online.International.utils.FileUtil;
import com.datelove.online.International.utils.GetBitmapUtils;
import com.datelove.online.International.utils.RecordUtil;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.adapter.recyclerview.MultiItemRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.dialog.OnDoubleDialogClickListener;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DensityUtil;
import com.library.utils.DialogUtil;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.library.widgets.ChatImageView;
import com.wonderkiln.blurkit.BlurLayout;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 聊天信息适配器，多item布局
 * Created by zhangdroid on 2016/8/17.
 */
public class ChatAdapter extends MultiItemRecyclerViewAdapter<Message> {
    // 消息类型常量
    /**
     * 管理员消息
     */
    private static final String MSG_TYPE_ADMIN = "3";
    /**
     * 文字消息
     */
    private static final String MSG_TYPE_TEXT = "4";
    /**
     * 语音消息
     */
    private static final String MSG_TYPE_VOICE = "7";
    /**
     * QA消息
     */
    private static final String MSG_TYPE_QA = "8";
    /**
     * 图片消息
     */
    private static final String MSG_TYPE_IMAGE = "10";

    // item type常量
    /**
     * 文字聊天（对方）
     */
    private static final int CHAT_TYPE_MSG_LEFT = -1;
    /**
     * 文字聊天（我方）
     */
    private static final int CHAT_TYPE_MSG_RIGHT = 1;
    /**
     * 语音聊天（对方）
     */
    private static final int CHAT_TYPE_VOICE_LEFT = -2;
    /**
     * 语音聊天（我方）
     */
    private static final int CHAT_TYPE_VOICE_RIGHT = 2;
    /**
     * QA聊天（对方）
     */
    private static final int CHAT_TYPE_QA_LEFT = -3;
    /**
     * 图片消息（对方）
     */
    private static final int CHAT_TYPE_IMAGE_LEFT = -4;
    /**
     * 图片消息（我方）
     */
    private static final int CHAT_TYPE_IMAGE_RIGHT = 4;

    // 语音播放动画
    private AnimationDrawable mVoiceAnimLeft;
    private AnimationDrawable mVoiceAnimRight;

    /**
     * 对方id
     */
    private String mUId;
    /**
     * 对方用户头像
     */
    private String mAvatarUrl;
    /**
     * 服务器时间戳毫秒数
     */
    private long mServerSystemTime;
    /**
     * 是否支付拦截
     */
    private boolean mIsIntercept;
    /**
     * 所有图片列表缓存，用来展示大图浏览
     */
    private List<Image> mAllImageList = new ArrayList<Image>();
    /**
     * 是否显示图片加载进度
     */
    private boolean mIsShowImgLoading;
    /**
     * 是否显示图片重发
     */
    private boolean mIsShowImgRetry;
    /**
     * 是否显示语音加载进度
     */
    private boolean mIsShowVoiceLoading;
    /**
     * 是否显示语音重发
     */
    private boolean mIsShowVoiceRetry;

    /**
     * 设置对方用户头像
     *
     * @param url
     */
    public void setAvatarUrl(String url) {
        this.mAvatarUrl = url;
    }

    public void setServerSystemTime(long serverSystemTime) {
        this.mServerSystemTime = serverSystemTime;
    }

    public void isIntercept(boolean isIntercept) {
        this.mIsIntercept = isIntercept;
    }

    public void setAllImageList(List<Image> allImageList) {
        this.mAllImageList = allImageList;
    }

    public void isShowImgLoading(boolean isShowImgLoading) {
        this.mIsShowImgLoading = isShowImgLoading;
    }

    public void isShowImgRetry(boolean isShowImgRetry) {
        this.mIsShowImgRetry = isShowImgRetry;
    }

    public void isShowVoiceLoading(boolean isShowVoiceLoading) {
        this.mIsShowVoiceLoading = isShowVoiceLoading;
    }

    public void isShowVoiceRetry(boolean isShowVoiceRetry) {
        this.mIsShowVoiceRetry = isShowVoiceRetry;
    }

    public ChatAdapter(Context context, int layoutResId) {
        this(context, layoutResId, null);
    }

    public ChatAdapter(Context context, int layoutResId, List<Message> list) {
        super(context, layoutResId, list);
        this.mContext = context;
        // 对方语音动画
        List<Drawable> lefList = new ArrayList<Drawable>();
        lefList.add(mContext.getResources().getDrawable(R.drawable.msg_voice_left_one));
        lefList.add(mContext.getResources().getDrawable(R.drawable.msg_voice_left_two));
        lefList.add(mContext.getResources().getDrawable(R.drawable.msg_voice_left_three));
        mVoiceAnimLeft = Utils.getFrameAnim(lefList, true, 300);
        // 我方语音动画
        List<Drawable> rightList = new ArrayList<Drawable>();
        rightList.add(mContext.getResources().getDrawable(R.drawable.msg_voice_right_one));
        rightList.add(mContext.getResources().getDrawable(R.drawable.msg_voice_right_two));
        rightList.add(mContext.getResources().getDrawable(R.drawable.msg_voice_right_three));
        mVoiceAnimRight = Utils.getFrameAnim(rightList, true, 300);
    }

    @Override
    protected int getItemViewType(int position, Message message) {
        System.out.println("5555555555555:::"+position);
        String userId = message.getUid();
        if (!TextUtils.isEmpty(userId)) {
            if (!userId.equals(UserInfoXml.getUID())) {
                mUId = userId;
            }
            String msgType = message.getMsgType();
            if (userId.equals(UserInfoXml.getUID())) {// 己方消息
                if (!TextUtils.isEmpty(msgType)) {
                    if (MSG_TYPE_TEXT.equals(msgType)) {// 文本消息
                        return CHAT_TYPE_MSG_RIGHT;
                    } else if (MSG_TYPE_VOICE.equals(msgType)) {// 语音消息
                        return CHAT_TYPE_VOICE_RIGHT;
                    } else if (MSG_TYPE_IMAGE.equals(msgType)) {// 图片消息
                        return CHAT_TYPE_IMAGE_RIGHT;
                    }
                }
            } else { // 对方消息
                if (!TextUtils.isEmpty(msgType)) {
                    if (MSG_TYPE_ADMIN.equals(msgType)) {// 管理员消息
                        return CHAT_TYPE_MSG_LEFT;
                    } else if (MSG_TYPE_TEXT.equals(msgType)) {// 文本消息
                        return CHAT_TYPE_MSG_LEFT;
                    } else if (MSG_TYPE_VOICE.equals(msgType)) {// 语音消息
                        return CHAT_TYPE_VOICE_LEFT;
                    } else if (MSG_TYPE_QA.equals(msgType)) {// QA消息
                        return CHAT_TYPE_QA_LEFT;
                    } else if (MSG_TYPE_IMAGE.equals(msgType)) {// 图片消息
                        return CHAT_TYPE_IMAGE_LEFT;
                    }
                }
            }
        }
        return 0;
    }

    @Override
    protected int getItemLayoutResId(int viewType) {
        int layoutResId = 0;
        switch (viewType) {
            case CHAT_TYPE_MSG_LEFT:// 对方文字消息
                layoutResId = R.layout.item_chat_msg_left;
                break;

            case CHAT_TYPE_MSG_RIGHT:// 我方文字消息
                layoutResId = R.layout.item_chat_msg_right;
                break;

            case CHAT_TYPE_VOICE_LEFT:// 对方语音消息
                layoutResId = R.layout.item_chat_voice_left;
                break;

            case CHAT_TYPE_VOICE_RIGHT:// 我方语音消息
                layoutResId = R.layout.item_chat_voice_right;
                break;

            case CHAT_TYPE_QA_LEFT:// 对方QA消息
                layoutResId = R.layout.item_chat_qa;
                break;

            case CHAT_TYPE_IMAGE_LEFT:// 对方图片消息
                layoutResId = R.layout.item_chat_image_left;
                break;

            case CHAT_TYPE_IMAGE_RIGHT:// 我方图片消息
                layoutResId = R.layout.item_chat_image_right;
                break;

        }
        return layoutResId;
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, Message bean) {
        if (bean != null) {
            String userId = bean.getUid();
            if (!TextUtils.isEmpty(userId)) {
                String msgType = bean.getMsgType();
                if (userId.equals(UserInfoXml.getUID())) {// 己方消息
                    if (!TextUtils.isEmpty(msgType)) {
                        switch (msgType) {
                            case MSG_TYPE_TEXT:// 文本消息
                                setRightMsg(viewHolder, bean);
                                break;

                            case MSG_TYPE_VOICE:// 语音消息
                                setRightVoiceMsg(viewHolder, bean);
                                break;

                            case MSG_TYPE_IMAGE://图片消息
                                setRightImageMsg(viewHolder, bean);
                                break;
                        }
                    }
                } else { // 对方消息
                    if (!TextUtils.isEmpty(msgType)) {
                        if (!TextUtils.isEmpty(msgType)) {
                            switch (msgType) {
                                case MSG_TYPE_ADMIN:// 管理员消息
                                    setLeftMsg(viewHolder, bean, true);
                                    break;

                                case MSG_TYPE_TEXT:// 文本消息
                                    setLeftMsg(viewHolder, bean, false);
                                    break;

                                case MSG_TYPE_VOICE:// 语音消息
                                    setLeftVoiceMsg(viewHolder, bean);
                                    break;

                                case MSG_TYPE_QA:// QA消息
                                    setQaMesssage(viewHolder, bean);
                                    break;

                                case MSG_TYPE_IMAGE://图片消息
                                    returnBitmap(bean.getChatImage().getThumbnailUrlM(), viewHolder, bean);

                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 设置对方文本消息
     *
     * @param viewHolder
     * @param message    {Message}对象
     * @param isAdmin    是否为管理员消息
     */
    private void setLeftMsg(RecyclerViewHolder viewHolder, final Message message, boolean isAdmin) {
        if (message != null) {
//            final BlurLayout blurLayout = (BlurLayout) viewHolder.getView(R.id.blurLayout);
            // 消息发送时间
            viewHolder.setText(R.id.chat_qa_msg_time_left, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_qa_msg_avatar_left);
            if (isAdmin) {
            } else {
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                        .url(mAvatarUrl).transform(new CircleTransformation()).imageView(ivAvatar).build());
                // 点击头像可查看用户详情
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), null, UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                    }
                });
            }

            // 聊天内容
            String content = message.getContent();
            if (!TextUtils.isEmpty(content)) {
                final TextView textview = (TextView) viewHolder.getView(R.id.chat_qa_msg_question);
                textview.setText(content);
                textview.post(new Runnable(){
                    public void run() {
                     //这里获取宽高
                        int width = textview.getWidth();
                        int height = textview.getHeight();
//                        blurLayout.setMinimumWidth(width);
//                        blurLayout.setMinimumHeight(height);
                    }
                });

//                textview.buildDrawingCache();
//                Bitmap bmp = textview.getDrawingCache();
//                FastBlur.blur(bmp, textview, hover_view, context);

            }
        }
    }

    /**
     * 设置己方文本消息
     *
     * @param viewHolder
     * @param message
     */
    private void setRightMsg(RecyclerViewHolder viewHolder, final Message message) {
        if (message != null) {
            // 消息发送时间
            viewHolder.setText(R.id.chat_msg_time_right, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_qa_msg_avatar_Right);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(UserInfoXml.getAvatarUrl()).transform(new CircleTransformation()).imageView(ivAvatar).build());
            // 点击头像可查看用户详情
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), null, UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                }
            });


            // 聊天内容
            String content = message.getContent();
            if (!TextUtils.isEmpty(content)) {
                viewHolder.setText(R.id.chat_msg_right, content);
            }
        }
    }

    /**
     * 设置对方语音消息
     *
     * @param viewHolder
     * @param message
     */
    private void setLeftVoiceMsg(final RecyclerViewHolder viewHolder, final Message message) {
        if (message != null) {
            // 消息发送时间
            viewHolder.setText(R.id.chat_voice_lasttime_left, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_voice_avatar_left);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(mAvatarUrl).transform(new CircleTransformation()).imageView(ivAvatar).build());
            // 点击头像可查看用户详情
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), "0", UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                }
            });
            // 将语音下载到本地(根据用户id存放到不同的目录下)
            String downloadFileDir = FileUtil.RECORD_DIRECTORY_PATH + File.separator + message.getUid();
            String downloadFileName = FileUtil.createFileNameByDate(message.getCreateDate()) + ".mp3";
            final String downloadPath = downloadFileDir + File.separator + downloadFileName;
            FileUtil.downloadFile(message.getAudioUrl(), downloadFileDir, downloadFileName);
            // 设置语音信息
            String audioTime = message.getAudioTime();
            if (!TextUtils.isEmpty(audioTime)) {
                viewHolder.setText(R.id.chat_voice_left_tv, audioTime + "''");
            }
            final ImageView ivUnread = (ImageView) viewHolder.getView(R.id.chat_voice_left_unread);
            String readState = message.getRecevStatus();
            if (!TextUtils.isEmpty(readState) && "0".equals(readState)) {
                ivUnread.setVisibility(View.VISIBLE);
            } else {
                ivUnread.setVisibility(View.GONE);
            }
            // 点击播放语音
            final ImageView ivAnim = (ImageView) viewHolder.getView(R.id.chat_voice_left_iv);
            ivAnim.setImageResource(R.drawable.msg_voice_left_three);
            viewHolder.getView(R.id.chat_voice_left).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 更新状态为已读
                    String readState = message.getRecevStatus();
                    if (!TextUtils.isEmpty(readState) && "0".equals(readState)) {
                        message.setRecevStatus("1");
                        ivUnread.setVisibility(View.GONE);
                        updateVoiceMsgStatus(message.getId());
                    }

                    // 当前是否正在播放语音
                    if (RecordUtil.getInstance().isPlaying()) {
                        // 先停止正在播放的语音
                        RecordUtil.getInstance().stop();
                        if (mVoiceAnimLeft.isRunning()) {
                            mVoiceAnimLeft.stop();
                        }
                        if (mVoiceAnimRight.isRunning()) {
                            mVoiceAnimRight.stop();
                        }
                    }

                    File file = new File(downloadPath);
                    if (file.exists()) {
                        // 播放/停止语音动画
                        if (!mVoiceAnimLeft.isRunning()) {
                            ivAnim.setImageDrawable(mVoiceAnimLeft);
                            mVoiceAnimLeft.start();
                        }
                        // 播放/暂停语音
                        RecordUtil.getInstance().toggle(downloadPath, new RecordUtil.OnPlayerListener() {

                            @Override
                            public void onCompleted() {
                                if (mVoiceAnimLeft.isRunning()) {
                                    mVoiceAnimLeft.stop();
                                    ivAnim.setImageResource(R.drawable.msg_voice_left_three);
                                }
                            }

                            @Override
                            public void onPaused() {
                                if (mVoiceAnimLeft.isRunning()) {
                                    mVoiceAnimLeft.stop();
                                    ivAnim.setImageResource(R.drawable.msg_voice_left_three);
                                }
                            }
                        });
                    } else {
                        ToastUtil.showShortToast(mContext, mContext.getResources().getString(R.string.not_exist));
                    }
                }
            });
        }
    }

    /**
     * 设置我方语音消息
     *
     * @param viewHolder
     * @param message
     */
    private void setRightVoiceMsg(RecyclerViewHolder viewHolder, final Message message) {
        if (message != null) {
            // 消息发送时间
            viewHolder.setText(R.id.chat_voice_lasttime_right, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_voice_avatar_right);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(UserInfoXml.getAvatarUrl()).transform(new CircleTransformation()).imageView(ivAvatar).build());
            // 点击头像可查看用户详情
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), "0", UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                }
            });

            // 设置语音信息
            String audioTime = message.getAudioTime();
            if (!TextUtils.isEmpty(audioTime)) {
                viewHolder.setText(R.id.chat_voice_right_tv, audioTime + "''");
            }
            String tempPath;
            String audioUrl = message.getAudioUrl();
            if (Utils.isHttpUrl(audioUrl)) {
                // 将语音下载到本地(根据用户id存放到不同的目录下)
                String downloadFileDir = FileUtil.RECORD_DIRECTORY_PATH + File.separator + message.getUid();
                String downloadFileName = FileUtil.createFileNameByDate(message.getCreateDate()) + ".mp3";
                tempPath = downloadFileDir + File.separator + downloadFileName;
                FileUtil.downloadFile(audioUrl, downloadFileDir, downloadFileName);
            } else {
                tempPath = audioUrl;
            }
            final String localPath = tempPath;

            // 语音加载进度
            final ImageView ivLoading = (ImageView) viewHolder.getView(R.id.chat_voice_msg_loading);
            AnimationDrawable animationDrawable = Util.getFrameAnim(Utils.getLoadingDrawableList(mContext), true, 50);
            animationDrawable.start();
            ivLoading.setImageDrawable(animationDrawable);
            ivLoading.setVisibility(mIsShowVoiceLoading ? View.VISIBLE : View.GONE);
            // 图片重发
            final ImageView ivRetry = (ImageView) viewHolder.getView(R.id.chat_voice_msg_retry);
            ivRetry.setVisibility(mIsShowVoiceRetry ? View.VISIBLE : View.GONE);
            ivRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 点击重发
                    DialogUtil.showDoubleBtnDialog(((FragmentActivity) mContext).getSupportFragmentManager(),
                            null, mContext.getString(R.string.retry_message), mContext.getString(R.string.positive),
                            mContext.getString(R.string.negative), true, new OnDoubleDialogClickListener() {
                                @Override
                                public void onPositiveClick(View view) {
                                    mIsShowVoiceLoading = true;
                                    mIsShowVoiceRetry = false;
                                    ivLoading.setVisibility(View.VISIBLE);
                                    ivRetry.setVisibility(View.GONE);
                                    sendVoiceMsg(new File(localPath), message);
                                }

                                @Override
                                public void onNegativeClick(View view) {

                                }
                            });
                }
            });
            // 点击播放语音
            final ImageView ivAnim = (ImageView) viewHolder.getView(R.id.chat_voice_right_iv);
            ivAnim.setImageResource(R.drawable.msg_voice_right_three);
            viewHolder.getView(R.id.chat_voice_right).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 当前是否正在播放语音
                    if (RecordUtil.getInstance().isPlaying()) {
                        // 先停止正在播放的语音
                        RecordUtil.getInstance().stop();
                        if (mVoiceAnimLeft.isRunning()) {
                            mVoiceAnimLeft.stop();
                        }
                        if (mVoiceAnimRight.isRunning()) {
                            mVoiceAnimRight.stop();
                        }
                    }
                    File file = new File(localPath);
                    if (file.exists()) {
                        // 播放/停止语音动画
                        if (!mVoiceAnimRight.isRunning()) {
                            ivAnim.setImageDrawable(mVoiceAnimRight);
                            mVoiceAnimRight.start();
                        }
                        // 播放/暂停语音
                        RecordUtil.getInstance().toggle(localPath, new RecordUtil.OnPlayerListener() {

                            @Override
                            public void onCompleted() {
                                if (mVoiceAnimRight.isRunning()) {
                                    mVoiceAnimRight.stop();
                                    ivAnim.setImageResource(R.drawable.msg_voice_right_three);
                                }
                            }

                            @Override
                            public void onPaused() {
                                if (mVoiceAnimRight.isRunning()) {
                                    mVoiceAnimRight.stop();
                                    ivAnim.setImageResource(R.drawable.msg_voice_right_three);
                                }
                            }
                        });
                    } else {
                        ToastUtil.showShortToast(mContext, mContext.getResources().getString(R.string.not_exist));
                    }
                }
            });
        }
    }

    /**
     * 设置对方QA消息
     *
     * @param viewHolder
     * @param message
     */
    private void setQaMesssage(RecyclerViewHolder viewHolder, final Message message) {
        if (message != null) {
            // 消息发送时间
            viewHolder.setText(R.id.chat_qa_msg_time_left, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_qa_msg_avatar_left);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(mAvatarUrl).transform(new CircleTransformation()).imageView(ivAvatar).build());
            // 点击头像可查看用户详情
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), null, UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                }
            });

            // QA问题
            String content = message.getContent();
            if (!TextUtils.isEmpty(content)) {
                viewHolder.setText(R.id.chat_qa_msg_question, content);
            }
            // QA答案列表
            QaQuestion qaQuestion = message.getQaQuestion();
            if (qaQuestion != null) {
                List<QaAnswer> qaAnswerList = qaQuestion.getListQaAnswer();
                if (!Util.isListEmpty(qaAnswerList)) {
                    RecyclerView answerListView = (RecyclerView) viewHolder.getView(R.id.chat_qa_msg_answer_list);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    answerListView.setLayoutManager(linearLayoutManager);
                    final QaAnswerAdapter qaAnswerAdapter = new QaAnswerAdapter(mContext, R.layout.item_chat_qa_answer_list, qaAnswerList);
                    answerListView.setAdapter(qaAnswerAdapter);
                    qaAnswerAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                            if (!mIsIntercept) {
                                QaAnswer qaAnswer = qaAnswerAdapter.getItemByPosition(position);
                                if (qaAnswer != null) {
                                    sendQaMessage(qaAnswer.getId());
                                }
                            } else {
                                BuyServiceActivity.toBuyServiceActivity((Activity) mContext, BuyServiceActivity.INTENT_FROM_INTERCEPT,
                                        IConfigConstant.PAY_TAG_FROM_CHAT, IConfigConstant.PAY_WAY_ALL);
                            }
                        }
                    });
                }
            }
        }
    }

    /**
     * 设置对方图片消息
     *
     * @param viewHolder
     * @param message
     */
    private void setLeftImageMsg(RecyclerViewHolder viewHolder, final Message message, final Bitmap bitmap) {
        if (message != null) {
            viewHolder.setText(R.id.chat_image_msg_time_left, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_image_msg_avatar_left);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(mAvatarUrl).transform(new CircleTransformation()).imageView(ivAvatar).build());
            // 点击头像可查看用户详情
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), "0", UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                }
            });
            final Image image = message.getChatImage();
            if (image != null) {
                ChatImageView imageView = (ChatImageView) viewHolder.getView(R.id.chat_image_msg_left);
                // 设置图片规格：1/3屏幕宽度，宽高比3：4
                int width = DensityUtil.getScreenWidth(mContext) / 3;
                if (bitmap != null) {
                    int width1 = bitmap.getWidth();
                    int height = bitmap.getHeight();
                    Double p = height * 1.0 / width1;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, (int) (p * width));
                    layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.chat_image_msg_avatar_left);
                    layoutParams.addRule(RelativeLayout.BELOW, R.id.chat_image_msg_time_left);
//                 Bitmap blurBitmap =fastblur(mContext,bitmap,8);
//                    Bitmap blurBitmap = doBlur(bitmap, 8, true);
//                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                    imageView.setImageBitmap(blurBitmap);

                    imageView.setLayoutParams(layoutParams);
                } else {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                    layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.chat_image_msg_avatar_left);
                    layoutParams.addRule(RelativeLayout.BELOW, R.id.chat_image_msg_time_left);
                    imageView.setLayoutParams(layoutParams);
                }




                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(image.getThumbnailUrlM())
                        .imageView(imageView).placeHolder(R.drawable.img_default).build());
                // 点击图片浏览大图
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PictureBrowseActivity.toPictureBrowseActivity(mContext, mAllImageList.indexOf(image), mAllImageList);
                    }
                });
            }
        }
    }

    /**
     * 设置我方图片消息
     *
     * @param viewHolder
     * @param message
     */
    private void setRightImageMsg(RecyclerViewHolder viewHolder, final Message message) {
        if (message != null) {
            viewHolder.setText(R.id.chat_image_msg_time_right, TimeUtil.getLocalTime(mContext, mServerSystemTime, message.getCreateDateMills()));
            // 头像
            ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.chat_image_msg_avatar_right);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(UserInfoXml.getAvatarUrl()).transform(new CircleTransformation()).imageView(ivAvatar).build());
            // 点击头像可查看用户详情
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, message.getUid(), "0", UserInfoDetailActivity.SOURCE_FROM_CHAT_CONTENT);
                }
            });
            final Image image = message.getChatImage();
            if (image != null) {
                ChatImageView imageView = (ChatImageView) viewHolder.getView(R.id.chat_image_msg_right);
                // 设置图片规格：1/3屏幕宽度，宽高比3：4
                int width = DensityUtil.getScreenWidth(mContext) / 3;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                imageView.setLayoutParams(layoutParams);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                String imageUrl = image.getThumbnailUrlM();
                if (Utils.isHttpUrl(imageUrl)) {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imageUrl)
                            .imageView(imageView).placeHolder(R.drawable.img_default).build());
                } else {
                    ImageLoaderUtil.getInstance().loadLocalImage(mContext, imageView, Uri.fromFile(new File(imageUrl)));
                }
                // 点击图片浏览大图
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PictureBrowseActivity.toPictureBrowseActivity(mContext, mAllImageList.indexOf(image), mAllImageList);
                    }
                });
            }
            // 图片加载进度
            final ImageView ivLoading = (ImageView) viewHolder.getView(R.id.chat_image_msg_loading);
            AnimationDrawable animationDrawable = Util.getFrameAnim(Utils.getLoadingDrawableList(mContext), true, 50);
            animationDrawable.start();
            ivLoading.setImageDrawable(animationDrawable);
            ivLoading.setVisibility(mIsShowImgLoading ? View.VISIBLE : View.GONE);
            // 图片重发
            final ImageView ivRetry = (ImageView) viewHolder.getView(R.id.chat_image_msg_retry);
            ivRetry.setVisibility(mIsShowImgRetry ? View.VISIBLE : View.GONE);
            ivRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 点击重发
                    DialogUtil.showDoubleBtnDialog(((FragmentActivity) mContext).getSupportFragmentManager(),
                            null, mContext.getString(R.string.retry_message), mContext.getString(R.string.positive),
                            mContext.getString(R.string.negative), true, new OnDoubleDialogClickListener() {
                                @Override
                                public void onPositiveClick(View view) {
                                    mIsShowImgLoading = true;
                                    mIsShowImgRetry = false;
                                    ivLoading.setVisibility(View.VISIBLE);
                                    ivRetry.setVisibility(View.GONE);
                                    sendPhotoMsg(new File(message.getChatImage().getImageUrl()), message);
                                }

                                @Override
                                public void onNegativeClick(View view) {

                                }
                            });
                }
            });
        }
    }

    /**
     * 更新语音信已读状态
     *
     * @param msgId
     */
    private void updateVoiceMsgStatus(final String msgId) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_UPDATE_VOICE_MSG_STATUS)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("msgId", msgId)
                .build().execute(new Callback<BaseModel>() {
            @Override
            public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                String resultJson = response.body().string();
                if (!TextUtils.isEmpty(resultJson)) {
                    return JSON.parseObject(resultJson, WriteMsg.class);
                }
                return null;
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                // 失败后再次更新
                updateVoiceMsgStatus(msgId);
            }

            @Override
            public void onResponse(BaseModel response, int id) {
                if (response != null) {
                    String isSucceed = response.getIsSucceed();
                    if (!TextUtils.isEmpty(isSucceed) && "0".equals(isSucceed)) {
                        // 失败后再次更新
                        updateVoiceMsgStatus(msgId);
                    }
                }
            }
        });
    }

    /**
     * 发送QA消息
     *
     * @param qaId
     */
    private void sendQaMessage(final String qaId) {
        final Dialog dialog = Utils.showLoadingDialog(mContext, true);
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SEND_TEXT_MSG)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", mUId)
                .addParams("writeMsgType", "1")
                .addParams("qaAnswerId", qaId)
                .build()
                .execute(new Callback<WriteMsg>() {
                    @Override
                    public WriteMsg parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, WriteMsg.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.send_fail));
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onResponse(WriteMsg response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                // 发送事件，刷新当前聊天记录
                                EventBus.getDefault().post(new RefreshChatEvent());
                                // 发送事件，更新聊天列表
                                EventBus.getDefault().post(new MessageChangedEvent());
                            } else {
                                ToastUtil.showShortToast(mContext, mContext.getString(R.string.send_fail));
                            }
                        } else {
                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.send_fail));
                        }
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                });
    }

    /**
     * 发送语音消息
     */
    private void sendVoiceMsg(File file, final Message message) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SEND_VOICE_MSG)
                .addHeader("token", PlatformInfoXml.getToken())
                .addHeader("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addHeader("uid", mUId)
                .addHeader("writeMsgType", "1")
                .addHeader("audioSecond", message.getAudioTime())
                .addFile("file", file.getName(), file)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        sendVoiceFailed(message);
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                // 发送事件，刷新当前聊天记录
                                EventBus.getDefault().post(new RefreshChatEvent());
                                // 发送事件，更新聊天列表
                                EventBus.getDefault().post(new MessageChangedEvent());
                            } else {
                                sendVoiceFailed(message);
                            }
                        } else {
                            sendVoiceFailed(message);
                        }
                    }
                });
    }

    /**
     * 发送语音失败，刷新状态
     */
    private void sendVoiceFailed(Message message) {
        mIsShowVoiceLoading = false;
        mIsShowVoiceRetry = true;
        updateItem(getItemCount() - 1, message);
        ToastUtil.showShortToast(mContext, mContext.getString(R.string.send_fail));
    }

    /**
     * 发送图片消息
     */
    private void sendPhotoMsg(File file, final Message message) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SEND_PHOTO_MSG)
                .addHeader("token", PlatformInfoXml.getToken())
                .addHeader("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addHeader("recevUserId", mUId)
                .addFile("request", file.getName(), file)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        sendPhotoFailed(message);
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                // 发送事件，刷新当前聊天记录
                                EventBus.getDefault().post(new RefreshChatEvent());
                                // 发送事件，更新聊天列表
                                EventBus.getDefault().post(new MessageChangedEvent());
                            } else {
                                sendPhotoFailed(message);
                            }
                        } else {
                            sendPhotoFailed(message);
                        }
                    }
                });
    }

    /**
     * 发送图片失败，刷新状态
     */
    private void sendPhotoFailed(Message message) {
        mIsShowImgLoading = false;
        mIsShowImgRetry = true;
        updateItem(getItemCount() - 1, message);
        ToastUtil.showShortToast(mContext, mContext.getString(R.string.send_fail));
    }

    /**
     * 根据图片的url路径获得Bitmap对象
     *
     * @param url
     * @return
     */
    public void returnBitmap(final String url, final RecyclerViewHolder viewHolder, final Message bean) {
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                URL fileUrl = null;
                Bitmap bitmap = null;
                try {
                    fileUrl = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                try {
                    HttpURLConnection conn = (HttpURLConnection) fileUrl
                            .openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    bitmap = BitmapFactory.decodeStream(is);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bitmap;

            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                Bitmap bitmap = (Bitmap) o;
                setLeftImageMsg(viewHolder, bean, bitmap);
            }
        }.execute();
    }



    /**毛玻璃效果**/
    public static Bitmap doBlur(Bitmap sentBitmap, int radius, boolean canReuseInBitmap) {

        // Stack Blur v1.0 from
        // http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
        //
        // Java Author: Mario Klingemann <mario at quasimondo.com>
        // http://incubator.quasimondo.com
        // created Feburary 29, 2004
        // Android port : Yahel Bouaziz <yahel at kayenko.com>
        // http://www.kayenko.com
        // ported april 5th, 2012

        // This is a compromise between Gaussian Blur and Box blur
        // It creates much better looking blurs than Box Blur, but is
        // 7x faster than my Gaussian Blur implementation.
        //
        // I called it Stack Blur because this describes best how this
        // filter works internally: it creates a kind of moving stack
        // of colors whilst scanning through the image. Thereby it
        // just has to add one new block of color to the right side
        // of the stack and remove the leftmost color. The remaining
        // colors on the topmost layer of the stack are either added on
        // or reduced by one, depending on if they are on the right or
        // on the left side of the stack.
        //
        // If you are using this algorithm in your code please add
        // the following line:
        //
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

        Bitmap bitmap;
        if (canReuseInBitmap) {
            bitmap = sentBitmap;
        } else {
            bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        }

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }










    public Bitmap fastblur(Context context, Bitmap sentBitmap, int radius) {


        if (Build.VERSION.SDK_INT > 16) {
            Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

            final RenderScript rs = RenderScript.create(context);
            final Allocation input = Allocation.createFromBitmap(rs, sentBitmap, Allocation.MipmapControl.MIPMAP_NONE,
                    Allocation.USAGE_SCRIPT);
            final Allocation output = Allocation.createTyped(rs, input.getType());
            final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setRadius(radius /* e.g. 3.f */);
            script.setInput(input);
            script.forEach(output);
            output.copyTo(bitmap);
            return bitmap;
        }

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int temp = 256 * divsum;
        int dv[] = new int[temp];
        for (i = 0; i < temp; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16)
                        | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
        return (bitmap);
    }
}

