package com.datelove.online.International.activity;

import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.adapter.MyAdapter;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.Person;
import com.datelove.online.International.event.SelectCountryEvent;
import com.datelove.online.International.utils.Cheeses;
import com.datelove.online.International.utils.QuickIndex;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Administrator on 2017/9/29.
 */

public class SelectCountryActivity extends BaseTitleActivity {

    private Handler handler = new Handler();
    private ArrayList<Person> personsList;
    private ListView listView;
    private TextView tv_hint;
    private EditText etSearch;
    QuickIndex quickIndex;
    private TextView tv_UnitedStates;
    private TextView tv_Australia;
    private TextView tv_India;
    private TextView tv_Germany;
    private TextView tv_France;
    private TextView tv_Paistan;
    private TextView tv_spain;
    private TextView tv_mexico;
    private TextView tv_united_kindom;
    private String text;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_select_country;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.country);
    }

    @Override
    protected void initViewsAndVariables() {
        //模糊查询
        LinearLayout mLlSelectCountry = (LinearLayout) findViewById(R.id.ll_select_country);
        mLlSelectCountry.setVisibility(View.VISIBLE);
        etSearch =(EditText)findViewById(R.id.et_search);
        etSearch.addTextChangedListener(textWatcher);
        // 设置快速检索条监听
         quickIndex = (QuickIndex) findViewById(R.id.quickindex);

    }

    @Override
    protected void addListeners() {
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        quickIndex.setOnLetterChangedListener(new QuickIndex.OnLetterChangedListener() {
            @Override
            public void onLetterChanged(char letter) {
                // 滚动到第一个letter字母处
                for (int i = 0; i < personsList.size(); i++) {
                    char personLetter = personsList.get(i).getLetter();
                    if (personLetter == letter) {
                        listView.setSelection(i);
                        break;
                    }
                }

                // 显示字母提示
                handler.removeCallbacksAndMessages(null);
                tv_hint.setVisibility(View.VISIBLE);
                tv_hint.setText(String.valueOf(letter));
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        tv_hint.setVisibility(View.GONE);
                    }
                }, 1000);
            }
        });

        // 生成用户列表
        personsList = new ArrayList<Person>();
        for (int i = 0; i < Cheeses.GOUNTRY.length; i++) {
            String name = Cheeses.GOUNTRY[i];
            char c = name.charAt(0);
//            char letter = StringUtils.getPinYin(name).charAt(0);
//			Person person = new Person(name, letter);
            Person person = new Person(name, c);
            personsList.add(person);
        }

        Collections.sort(personsList);
        System.out.println("MainActivity.onCreate,list="+personsList);

        // 填充列表
        listView = (ListView) findViewById(R.id.listview);
        //header
        View headerView = LayoutInflater.from(SelectCountryActivity.this).inflate(R.layout.item_select_country_header,null);
        //可以添加多个HeaderView
        listView.addHeaderView(headerView);
        listView.setAdapter(new MyAdapter(personsList,SelectCountryActivity.this) );

        // 提示框
        tv_hint = (TextView) findViewById(R.id.tv_hint);
//        热门国家
         tv_UnitedStates = (TextView) findViewById(R.id.tv_united_states);
        tv_Australia = (TextView) findViewById(R.id.tv_australia);
        tv_India = (TextView) findViewById(R.id.tv_india);
        tv_Germany = (TextView) findViewById(R.id.tv_germany);
        tv_France = (TextView) findViewById(R.id.tv_france);
        tv_Paistan = (TextView) findViewById(R.id.tv_paistan);
        tv_spain = (TextView) findViewById(R.id.tv_spain);
        tv_mexico = (TextView) findViewById(R.id.tv_mexico);
        tv_united_kindom = (TextView) findViewById(R.id.tv_united_kindom);


        tv_UnitedStates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_UnitedStates.getText();
                finish();

            }
        });
        tv_Australia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_Australia.getText();
                finish();
            }
        });
        tv_India.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_India.getText();
                finish();
            }
        });
        tv_Germany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_Germany.getText();
                finish();
            }
        });
        tv_France.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_France.getText();
                finish();
            }
        });
        tv_Paistan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_Paistan.getText();
                finish();
            }
        });
        tv_spain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_spain.getText();
                finish();
            }
        });
        tv_mexico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_mexico.getText();
                finish();
            }
        });
        tv_united_kindom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 text = (String) tv_united_kindom.getText();
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().post(new SelectCountryEvent(text));

    }

    private String str;
    public TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            Log.d("TAG","afterTextChanged--------------->");
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub
            Log.d("TAG","beforeTextChanged--------------->");
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            str = etSearch.getText().toString().toUpperCase();
            Log.d("TAG","onTextChanged--------------->"+str);
            // 滚动到第一个letter字母处
            if(!TextUtils.isEmpty(str)){
                for (int i = 0; i < personsList.size(); i++) {
                    char personLetter = personsList.get(i).getLetter();
                    char c = str.charAt(0);
                    if (personLetter==c) {
                        listView.setSelection(i);
                        break;
                    }
                }
            }
        }
    };
}
