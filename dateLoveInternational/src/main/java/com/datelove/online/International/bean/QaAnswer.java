package com.datelove.online.International.bean;

/**
 * Created by Administrator on 2016/11/25.
 */

public class QaAnswer {
    private String id;
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "QaAnswer{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
