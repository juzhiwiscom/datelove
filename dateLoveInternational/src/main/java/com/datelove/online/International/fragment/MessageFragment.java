package com.datelove.online.International.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.activity.MutualLikeActivity;
import com.datelove.online.International.adapter.MessageAdapter;
import com.datelove.online.International.base.BaseTitleFragment;
import com.datelove.online.International.bean.ChatInfo;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.MessageChangedEvent;
import com.datelove.online.International.event.UpdateMessageCountEvent;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.utils.SharedPreferenceUtil;
import com.library.widgets.RefreshRecyclerView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;


/**
 * 信箱
 */
public class MessageFragment extends BaseTitleFragment {
    @BindView(R.id.message_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    @BindView(R.id.tv_loading)
    TextView tvLoading;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    @BindView(R.id.iv_loading_heart)
    ImageView ivLoadingHeart;
    @BindView(R.id.rl_match)
    RelativeLayout rlMatch;
    private MessageAdapter mMessageAdapter;
    private int pageNum = 1;
    private final String pageSize = "30";
    private int mTotalUnreadMsgCnt;
    private int i = 0;


    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            this.update();
//            handler.postDelayed(this, 1000*10);// 间隔1秒
        }

        void update() {
            //刷新msg的内容
            loadMessageList();

        }
    };
    private boolean isMember = false;
    private boolean pageNumIs1;

    /**
     * 停止刷新
     **/
    public void stopRefish() {
        handler.removeCallbacks(runnable); //停止刷新
    }

    /**
     * 一进入此界面开始首次刷新
     **/
    public void startFirstRefish() {
        handler.postDelayed(runnable, 100); //开始刷新
    }

    /**
     * 开始刷新
     **/
    public void startRefish() {
        handler.postDelayed(runnable, 1000 * 10); //开始刷新
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_message;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_mail));
        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.lucency_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.alpha(0));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(getActivity()));
        mMessageAdapter = new MessageAdapter(getActivity(), R.layout.item_fragment_message);
        mRefreshRecyclerView.setAdapter(mMessageAdapter);
        loadMessageList();

    }


    @Override
    protected void addListeners() {
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                i++;
                if (i == 1) {
                    flLoading.setVisibility(View.INVISIBLE);
                    Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                    ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                } else {
                    flLoading.setVisibility(View.VISIBLE);
                    tvLoading.setVisibility(View.VISIBLE);
                    Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                    ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                }
                refreshChatMsg();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                flLoading.setVisibility(View.VISIBLE);
                tvLoading.setVisibility(View.VISIBLE);
                Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                pageNum++;
                loadMessageList();
            }
        }, getActivity(), true);
//        跳转到互相喜欢的界面（和最近访客共用一个类）
        rlMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MutualLikeActivity.class);
                startActivity(intent);
            }
        });
///**对触摸事件的监听，根据手指动作判断要不要进行自动刷新**/
//        mRefreshRecyclerView.setIsTouchListener(new RefreshRecyclerView.StopRefishListener() {
//            @Override
//            public void isTouch(boolean istouch) {
//                if(istouch){
//                   stopRefish();
//                }else {
//                    startRefish();
//                }
//            }
//        });
//        mMessageAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                Chat chat = mMessageAdapter.getItemByPosition(position);
//                if (chat != null) {
//                    ChatMsg chatMsg = chat.getChat();
//                    if (chatMsg != null) {
//                        int unreadMsgCnt = chatMsg.getUnreadCount();
//                        if (unreadMsgCnt > 0) {// 标记未读消息为已读
////                            chatMsg.setUnreadCount(0);
//                            chat.setChat(chatMsg);
//                            mMessageAdapter.updateItem(position, chat);
//                            // 更新总的未读消息数
////                            mTotalUnreadMsgCnt = mTotalUnreadMsgCnt - unreadMsgCnt;
//                            EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
//                        }
//                    }
//                    UserBase userBase = chat.getUserBaseEnglish();
//                    if (userBase != null) {
//                        Image image = userBase.getImage();
//                        String imgUrl = "";
//                        if (image != null) {
//                            imgUrl = image.getThumbnailUrl();
//                        }
////                        Intent intent = new Intent(getActivity(), InterceptDialogActivity.class);
////                        getActivity().startActivity(intent);
//
//
//                        ChatActivity.toChatActivity(getActivity(), userBase.getId(), userBase.getNickName(), imgUrl);
//                    }
//                }
//            }
//        });
    }

    private void refreshChatMsg() {
        pageNum = 1;
        loadMessageList();
    }

    /**
     * 加载聊天消息列表
     */
    private void loadMessageList() {
//  防止多次请求，pageNum=1时还没请求就变成了其他数字
        if (pageNum == 1) {
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_GET_ALL_CHAT_LIST)
                    .addHeader("token", PlatformInfoXml.getToken())
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .addParams("pageNum", String.valueOf(1))
                    .addParams("pageSize", pageSize)
                    .build()
                    .execute(new Callback<ChatInfo>() {
                        @Override
                        public ChatInfo parseNetworkResponse(Response response, int id) throws Exception {
                            String resultJson = response.body().string();
                            if (!TextUtils.isEmpty(resultJson)) {
                                return JSON.parseObject(resultJson, ChatInfo.class);
                            }
                            return null;
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            stopRefresh(3);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    flLoading.setVisibility(View.GONE);
                                    tvLoading.setVisibility(View.GONE);
                                }
                            }, 1000);
                        }

                        @Override
                        public void onResponse(final ChatInfo response, int id) {
                            if (response != null) {
//                            mTotalUnreadMsgCnt = response.getTotalUnread();
                                //非会员显示的未读消息数量
                                mTotalUnreadMsgCnt = response.getTotalUnread24H();


                                String showWriteMsgIntercept = response.getShowWriteMsgIntercept();

                                if (!TextUtils.isEmpty(showWriteMsgIntercept) && showWriteMsgIntercept.equals("2")) {
                                    /**设置信息照片等不拦截**/
                                    SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", false);
                                    //会员显示的未读消息数量
                                    mTotalUnreadMsgCnt = response.getTotalUnread();
                                } else {
                                    if (UserInfoXml.isMale()) {
                                        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
//                                    String totalUnread24H = response.getTotalUnread24H();
                                    } else {
//                                    女用户显示的未读消息数量
                                        mTotalUnreadMsgCnt = response.getTotalUnread();
                                        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", false);
                                    }

                                }
                                EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
                                // 延时0.5秒显示
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mMessageAdapter != null) {

                                            mMessageAdapter.setServerSystemTime(response.getSystemTime());
                                            mMessageAdapter.replaceAll(response.getListChat());
                                        }
                                    }
                                }, 500);
                            }
                            stopRefresh(1);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (flLoading == null || flLoading.getVisibility() == View.GONE) {

                                    } else {
                                        flLoading.setVisibility(View.GONE);
                                        tvLoading.setVisibility(View.GONE);
                                    }

                                }
                            }, 1000);
                        }
                    });
        } else {
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_GET_ALL_CHAT_LIST)
                    .addHeader("token", PlatformInfoXml.getToken())
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .addParams("pageNum", String.valueOf(pageNum))
                    .addParams("pageSize", pageSize)
                    .build()
                    .execute(new Callback<ChatInfo>() {
                        @Override
                        public ChatInfo parseNetworkResponse(Response response, int id) throws Exception {
                            String resultJson = response.body().string();
                            if (!TextUtils.isEmpty(resultJson)) {
                                return JSON.parseObject(resultJson, ChatInfo.class);
                            }
                            return null;
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            stopRefresh(3);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    flLoading.setVisibility(View.GONE);
                                    tvLoading.setVisibility(View.GONE);
                                }
                            }, 1000);
                        }

                        @Override
                        public void onResponse(final ChatInfo response, int id) {
                            if (response != null) {
//                            mTotalUnreadMsgCnt = response.getTotalUnread();
                                //非会员显示的未读消息数量
                                mTotalUnreadMsgCnt = response.getTotalUnread24H();


                                String showWriteMsgIntercept = response.getShowWriteMsgIntercept();

                                if (!TextUtils.isEmpty(showWriteMsgIntercept) && showWriteMsgIntercept.equals("2")) {
                                    /**设置信息照片等不拦截**/
                                    SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", false);
                                    //会员显示的未读消息数量
                                    mTotalUnreadMsgCnt = response.getTotalUnread();
                                } else {
                                    if (UserInfoXml.isMale()) {
                                        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
//                                    String totalUnread24H = response.getTotalUnread24H();
                                    } else {
//                                    女用户显示的未读消息数量
                                        mTotalUnreadMsgCnt = response.getTotalUnread();
                                        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", false);
                                    }

                                }
                                EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
                                // 延时0.5秒显示
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mMessageAdapter != null) {

                                            mMessageAdapter.setServerSystemTime(response.getSystemTime());
//                                            if (pageNum == 1 || pageNumIs1) {
//                                                pageNumIs1=false;
//                                                mMessageAdapter.replaceAll(response.getListChat());
//                                            } else {
                                            mMessageAdapter.appendToList(response.getListChat());
//                                            }
//                                        int totalUnRead=0;
//                                        for(int i=0;i<response.getListChat().size();i++){
//                                            totalUnRead=totalUnRead+response.getListChat().get(i).getChat().getUnreadCount();
//                                        }
//                                        EventBus.getDefault().post(new UpdateMessageCountEvent(totalUnRead));
                                        }
                                    }
                                }, 500);
                            }
                            stopRefresh(1);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (flLoading == null || flLoading.getVisibility() == View.GONE) {

                                    } else {
                                        flLoading.setVisibility(View.GONE);
                                        tvLoading.setVisibility(View.GONE);
                                    }

                                }
                            }, 1000);
                        }
                    });
        }

    }


    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Subscribe
    public void onEvent(MessageChangedEvent event) {
        refreshChatMsg();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
