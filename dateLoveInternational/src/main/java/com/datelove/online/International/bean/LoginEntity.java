package com.datelove.online.International.bean;

import java.util.List;

/**
 * Created by jzrh on 2016/6/25.
 */
public class LoginEntity extends BaseModel {
    private String checkInTemplate;
    private String introduceTemplate;
    private String isInterceptSteps2;
    private String isShowOneYuanDialog;
    private String isShowPraiseDialog;
    private String isShowService;
    private String isShowUploadImg;
    public String isShowUploadAudioInformation; //判断女用户是否上传了语音
    private String sessionId;
    private String token;
    private User userEnglish;
    private List<String> leaveTemplates;
    private List<String> recallTags;
    private String isShowHeadNotice;// 老页眉开关
    private String isShowHeadNotice2;// 新页眉开关
    private String isShowCommendUser;// 用户推荐开关
    private String searchInterceptor;//部分用户拦截设置：0 不拦截 1 拦截

    public String getCheckInTemplate() {
        return checkInTemplate;
    }

    public void setCheckInTemplate(String checkInTemplate) {
        this.checkInTemplate = checkInTemplate;
    }

    public String getIntroduceTemplate() {
        return introduceTemplate;
    }

    public void setIntroduceTemplate(String introduceTemplate) {
        this.introduceTemplate = introduceTemplate;
    }

    public String getIsInterceptSteps2() {
        return isInterceptSteps2;
    }

    public void setIsInterceptSteps2(String isInterceptSteps2) {
        this.isInterceptSteps2 = isInterceptSteps2;
    }

    public String getIsShowOneYuanDialog() {
        return isShowOneYuanDialog;
    }

    public void setIsShowOneYuanDialog(String isShowOneYuanDialog) {
        this.isShowOneYuanDialog = isShowOneYuanDialog;
    }

    public String getIsShowPraiseDialog() {
        return isShowPraiseDialog;
    }

    public void setIsShowPraiseDialog(String isShowPraiseDialog) {
        this.isShowPraiseDialog = isShowPraiseDialog;
    }

    public String getIsShowService() {
        return isShowService;
    }

    public void setIsShowService(String isShowService) {
        this.isShowService = isShowService;
    }

    public String getIsShowUploadImg() {
        return isShowUploadImg;
    }

    public void setIsShowUploadImg(String isShowUploadImg) {
        this.isShowUploadImg = isShowUploadImg;
    }
    public String getIsShowUploadAudioInformation() {
        return isShowUploadAudioInformation;
    }

    public void setIsShowUploadAudioInformation(String isShowUploadAudioInformation) {
        this.isShowUploadAudioInformation = isShowUploadAudioInformation;
    }
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUserEnglish() {
        return userEnglish;
    }

    public void setUserEnglish(User userEnglish) {
        this.userEnglish = userEnglish;
    }

    public List<String> getLeaveTemplates() {
        return leaveTemplates;
    }

    public void setLeaveTemplates(List<String> leaveTemplates) {
        this.leaveTemplates = leaveTemplates;
    }

    public List<String> getRecallTags() {
        return recallTags;
    }

    public void setRecallTags(List<String> recallTags) {
        this.recallTags = recallTags;
    }

    public String getIsShowHeadNotice() {
        return isShowHeadNotice;
    }

    public void setIsShowHeadNotice(String isShowHeadNotice) {
        this.isShowHeadNotice = isShowHeadNotice;
    }

    public String getIsShowHeadNotice2() {
        return isShowHeadNotice2;
    }

    public void setIsShowHeadNotice2(String isShowHeadNotice2) {
        this.isShowHeadNotice2 = isShowHeadNotice2;
    }

    public String getIsShowCommendUser() {
        return isShowCommendUser;
    }

    public void setIsShowCommendUser(String isShowCommendUser) {
        this.isShowCommendUser = isShowCommendUser;
    }
    public String getSearchInterceptor() {
        return searchInterceptor;
    }

    public void setSearchInterceptor(String searchInterceptor) {
        this.searchInterceptor = searchInterceptor;
    }

    @Override
    public String toString() {
        return "LoginEntity{" +
                "checkInTemplate='" + checkInTemplate + '\'' +
                ", introduceTemplate='" + introduceTemplate + '\'' +
                ", isInterceptSteps2='" + isInterceptSteps2 + '\'' +
                ", isShowOneYuanDialog='" + isShowOneYuanDialog + '\'' +
                ", isShowPraiseDialog='" + isShowPraiseDialog + '\'' +
                ", isShowService='" + isShowService + '\'' +
                ", isShowUploadImg='" + isShowUploadImg + '\'' +
                ", isShowUploadAudioInformation='" + isShowUploadAudioInformation + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", token='" + token + '\'' +
                ", userEnglish=" + userEnglish +
                ", leaveTemplates=" + leaveTemplates +
                ", recallTags=" + recallTags +
                ", isShowHeadNotice='" + isShowHeadNotice + '\'' +
                ", isShowHeadNotice2='" + isShowHeadNotice2 + '\'' +
                ", isShowCommendUser='" + isShowCommendUser + '\'' +
                ", searchInterceptor='" + searchInterceptor + '\'' +
                '}';
    }

}
