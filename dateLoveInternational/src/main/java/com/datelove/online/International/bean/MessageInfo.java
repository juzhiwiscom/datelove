package com.datelove.online.International.bean;

import java.util.List;

/**
 * 所有聊天记录信息
 * Created by zhangdroid on 2016/10/29.
 */
public class MessageInfo {
    private long systemTime;// 当前服务器时间戳
    private long lastTime;// 最新一条消息时间戳
    private List<MsgBox> listMsgBox;// 聊天消息记录列表
    private int totalCount;
    private int pageSize;
    private int pageNum;
    private int lastMsgBoxId;
    private String unreadMsg;

    public String getUnreadMsg() {
        return unreadMsg;
    }

    public void setUnreadMsg(String unreadMsg) {
        this.unreadMsg = unreadMsg;
    }

    public long getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(long systemTime) {
        this.systemTime = systemTime;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }

    public List<MsgBox> getListMsgBox() {
        return listMsgBox;
    }

    public void setListMsgBox(List<MsgBox> listMsgBox) {
        this.listMsgBox = listMsgBox;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getLastMsgBoxId() {
        return lastMsgBoxId;
    }

    public void setLastMsgBoxId(int lastMsgBoxId) {
        this.lastMsgBoxId = lastMsgBoxId;
    }

    @Override
    public String toString() {
        return "MessageInfo{" +
                "systemTime=" + systemTime +
                ", lastTime=" + lastTime +
                ", listMsgBox=" + listMsgBox +
                ", totalCount=" + totalCount +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", lastMsgBoxId=" + lastMsgBoxId +
                '}';
    }
}
