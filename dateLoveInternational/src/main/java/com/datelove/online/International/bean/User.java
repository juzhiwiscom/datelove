package com.datelove.online.International.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 用户对象
 * Created by zhangdroid on 2016/6/23.
 */
public class User implements Parcelable {
    private UserBase userBaseEnglish;// 用户对象基础信息
    private MatcherInfo matcherInfo;// 征友匹配信息对象
    private List<Image> listImage; // 图片对象列表

    private String account;// 账号
    private String password;// 密码
    private String loginTime;// 登陆时间
    private String cardNo;// 身份证号
    private String mobile;// 手机号码
    private String name;// 真实姓名
    private String birthday;// 生日
    private int sign;// 星座
    private int occupation; // 职业
    private String listHobby;// 兴趣爱好
    private int relationship;// 婚姻状况
    private String homePlaceState;// 籍贯
    private int bloodId;// 血型
    private int loverTypeId;// 喜欢的类型
    private int sexBefMarried;// 可以接受的亲密行为
    private int houseId;// 住房状况
    private int liveWithParentId;// 是否和父母同住
    private int charm;// 魅力部位
    private int diffAreaLoveId; // 是否接受异地恋
    private int weight;// 体重

    private int isBeanUser;// 1->表示为豆币用户,0->未开通
    private int isMonthly;// 1->表示开通“写信包月”,0->未开通
    private int isVip;// 1->VIP,0->非VIP
    private int level;//诚信等级
    private int isAuthentication;//1->已验证身份，0->未验证
    private int onlineState;// 在线状态，0->不在线，1->在线
    private int isFollow;// 是否关注,0->未关注，1->已关注
    private int isBlackList;// 是否拉黑,0->未拉黑，1->拉黑
    private int isSayHello;// 是否打了招呼，1->打了招呼，0->未打招呼
    private int isMatched;// 是否配对，1->已匹配，0->未匹配
    private int followNum;// 关注数
    private int beanCurrencyCount;// 豆币数
    private int likeValueCurrentCount;// 当前like值

    private String nameState;// 昵称审核状态的名字（获取我的空间时返回）
    private int monologueState;// 内心独白审核状态（获取我的空间时返回），默认为0,1->审核中
    private int isMatchmakerUser;// 是否为红娘用户,0->不是，1->是
    private int isVerifyIdentity;// 是否验证身份，0->未认证,1->认证
    private int isVerifyServiceman;// 是否验证军官证,0->未验证,1-验证
    private int isShowLoveInsurance;// 是否显示恋爱无忧险,0->不显示,1->显示
    private int infoLevel;// 资料完成度
    private int isBuyInsurance;// 是否购买过恋爱无忧险,0->未购买,1->已购买
    private int isVerifyQQ;// 是否验证QQ，0->未认证，1->认证
    private int isVerifyMobile;// 是否验证手机号,0->未验证,1-验证

    private String pets;//宠物(类型：String )
    private String sports;//运动(类型：String )
    private int exerciseHabits;//锻炼习惯(类型：int )
    private int smoke;//是否吸烟(类型：int )
    private int drink;//是否喝酒(类型：int )
    private int wantsKids;//是否要小孩,(类型：int )
    private int haveKids;//是否有小孩(类型：int )
    private int howManyKids;//有多少小孩(类型：int )
    private int eyeColor;//眼睛颜色(类型：int )
    private int hairColor;//头发颜色(类型：int )
    private String ethnicity;//种族(类型：String )
    private int faith;//信仰(类型：int )
    private int politicalViews;//政治观点

    private String books;//喜欢的书和漫画
    private String characteristics;//个性特征
    private String foods;//喜欢的食物
    private String movies;//喜欢的电影
    private String music;//喜欢的音乐
    private String travel;//旅行

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    public MatcherInfo getMatcherInfo() {
        return matcherInfo;
    }

    public void setMatcherInfo(MatcherInfo matcherInfo) {
        this.matcherInfo = matcherInfo;
    }

    public List<Image> getListImage() {
        return listImage;
    }

    public void setListImage(List<Image> listImage) {
        this.listImage = listImage;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getOccupation() {
        return occupation;
    }

    public void setOccupation(int occupation) {
        this.occupation = occupation;
    }

    public String getListHobby() {
        return listHobby;
    }

    public void setListHobby(String listHobby) {
        this.listHobby = listHobby;
    }

    public int getRelationship() {
        return relationship;
    }

    public void setRelationship(int relationship) {
        this.relationship = relationship;
    }

    public String getHomePlaceState() {
        return homePlaceState;
    }

    public void setHomePlaceState(String homePlaceState) {
        this.homePlaceState = homePlaceState;
    }

    public int getBloodId() {
        return bloodId;
    }

    public void setBloodId(int bloodId) {
        this.bloodId = bloodId;
    }

    public int getLoverTypeId() {
        return loverTypeId;
    }

    public void setLoverTypeId(int loverTypeId) {
        this.loverTypeId = loverTypeId;
    }

    public int getSexBefMarried() {
        return sexBefMarried;
    }

    public void setSexBefMarried(int sexBefMarried) {
        this.sexBefMarried = sexBefMarried;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public int getLiveWithParentId() {
        return liveWithParentId;
    }

    public void setLiveWithParentId(int liveWithParentId) {
        this.liveWithParentId = liveWithParentId;
    }

    public int getCharm() {
        return charm;
    }

    public void setCharm(int charm) {
        this.charm = charm;
    }

    public int getDiffAreaLoveId() {
        return diffAreaLoveId;
    }

    public void setDiffAreaLoveId(int diffAreaLoveId) {
        this.diffAreaLoveId = diffAreaLoveId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getIsBeanUser() {
        return isBeanUser;
    }

    public void setIsBeanUser(int isBeanUser) {
        this.isBeanUser = isBeanUser;
    }

    public int getIsMonthly() {
        return isMonthly;
    }

    public void setIsMonthly(int isMonthly) {
        this.isMonthly = isMonthly;
    }

    public int getIsVip() {
        return isVip;
    }

    public void setIsVip(int isVip) {
        this.isVip = isVip;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getIsAuthentication() {
        return isAuthentication;
    }

    public void setIsAuthentication(int isAuthentication) {
        this.isAuthentication = isAuthentication;
    }

    public int getOnlineState() {
        return onlineState;
    }

    public void setOnlineState(int onlineState) {
        this.onlineState = onlineState;
    }

    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public int getIsBlackList() {
        return isBlackList;
    }

    public void setIsBlackList(int isBlackList) {
        this.isBlackList = isBlackList;
    }

    public int getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(int isSayHello) {
        this.isSayHello = isSayHello;
    }

    public int getIsMatched() {
        return isMatched;
    }

    public void setIsMatched(int isMatched) {
        this.isMatched = isMatched;
    }

    public int getFollowNum() {
        return followNum;
    }

    public void setFollowNum(int followNum) {
        this.followNum = followNum;
    }

    public int getBeanCurrencyCount() {
        return beanCurrencyCount;
    }

    public void setBeanCurrencyCount(int beanCurrencyCount) {
        this.beanCurrencyCount = beanCurrencyCount;
    }

    public int getLikeValueCurrentCount() {
        return likeValueCurrentCount;
    }

    public void setLikeValueCurrentCount(int likeValueCurrentCount) {
        this.likeValueCurrentCount = likeValueCurrentCount;
    }

    public String getNameState() {
        return nameState;
    }

    public void setNameState(String nameState) {
        this.nameState = nameState;
    }

    public int getMonologueState() {
        return monologueState;
    }

    public void setMonologueState(int monologueState) {
        this.monologueState = monologueState;
    }

    public int getIsMatchmakerUser() {
        return isMatchmakerUser;
    }

    public void setIsMatchmakerUser(int isMatchmakerUser) {
        this.isMatchmakerUser = isMatchmakerUser;
    }

    public int getIsVerifyIdentity() {
        return isVerifyIdentity;
    }

    public void setIsVerifyIdentity(int isVerifyIdentity) {
        this.isVerifyIdentity = isVerifyIdentity;
    }

    public int getIsVerifyServiceman() {
        return isVerifyServiceman;
    }

    public void setIsVerifyServiceman(int isVerifyServiceman) {
        this.isVerifyServiceman = isVerifyServiceman;
    }

    public int getIsShowLoveInsurance() {
        return isShowLoveInsurance;
    }

    public void setIsShowLoveInsurance(int isShowLoveInsurance) {
        this.isShowLoveInsurance = isShowLoveInsurance;
    }

    public int getInfoLevel() {
        return infoLevel;
    }

    public void setInfoLevel(int infoLevel) {
        this.infoLevel = infoLevel;
    }

    public int getIsBuyInsurance() {
        return isBuyInsurance;
    }

    public void setIsBuyInsurance(int isBuyInsurance) {
        this.isBuyInsurance = isBuyInsurance;
    }

    public int getIsVerifyQQ() {
        return isVerifyQQ;
    }

    public void setIsVerifyQQ(int isVerifyQQ) {
        this.isVerifyQQ = isVerifyQQ;
    }

    public int getIsVerifyMobile() {
        return isVerifyMobile;
    }

    public void setIsVerifyMobile(int isVerifyMobile) {
        this.isVerifyMobile = isVerifyMobile;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public int getExerciseHabits() {
        return exerciseHabits;
    }

    public void setExerciseHabits(int exerciseHabits) {
        this.exerciseHabits = exerciseHabits;
    }

    public int getSmoke() {
        return smoke;
    }

    public void setSmoke(int smoke) {
        this.smoke = smoke;
    }

    public int getDrink() {
        return drink;
    }

    public void setDrink(int drink) {
        this.drink = drink;
    }

    public int getWantsKids() {
        return wantsKids;
    }

    public void setWantsKids(int wantsKids) {
        this.wantsKids = wantsKids;
    }

    public int getHaveKids() {
        return haveKids;
    }

    public void setHaveKids(int haveKids) {
        this.haveKids = haveKids;
    }

    public int getHowManyKids() {
        return howManyKids;
    }

    public void setHowManyKids(int howManyKids) {
        this.howManyKids = howManyKids;
    }

    public int getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(int eyeColor) {
        this.eyeColor = eyeColor;
    }

    public int getHairColor() {
        return hairColor;
    }

    public void setHairColor(int hairColor) {
        this.hairColor = hairColor;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getFaith() {
        return faith;
    }

    public void setFaith(int faith) {
        this.faith = faith;
    }

    public int getPoliticalViews() {
        return politicalViews;
    }

    public void setPoliticalViews(int politicalViews) {
        this.politicalViews = politicalViews;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getFoods() {
        return foods;
    }

    public void setFoods(String foods) {
        this.foods = foods;
    }

    public String getMovies() {
        return movies;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getTravel() {
        return travel;
    }

    public void setTravel(String travel) {
        this.travel = travel;
    }

    @Override
    public String toString() {
        return "User{" +
                "userBaseEnglish=" + userBaseEnglish +
                ", matcherInfo=" + matcherInfo +
                ", listImage=" + listImage +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", loginTime='" + loginTime + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", name='" + name + '\'' +
                ", birthday='" + birthday + '\'' +
                ", sign=" + sign +
                ", occupation=" + occupation +
                ", listHobby='" + listHobby + '\'' +
                ", relationship=" + relationship +
                ", homePlaceState='" + homePlaceState + '\'' +
                ", bloodId=" + bloodId +
                ", loverTypeId=" + loverTypeId +
                ", sexBefMarried=" + sexBefMarried +
                ", houseId=" + houseId +
                ", liveWithParentId=" + liveWithParentId +
                ", charm=" + charm +
                ", diffAreaLoveId=" + diffAreaLoveId +
                ", weight=" + weight +
                ", isBeanUser=" + isBeanUser +
                ", isMonthly=" + isMonthly +
                ", isVip=" + isVip +
                ", level=" + level +
                ", isAuthentication=" + isAuthentication +
                ", onlineState=" + onlineState +
                ", isFollow=" + isFollow +
                ", isBlackList=" + isBlackList +
                ", isSayHello=" + isSayHello +
                ", isMatched=" + isMatched +
                ", followNum=" + followNum +
                ", beanCurrencyCount=" + beanCurrencyCount +
                ", likeValueCurrentCount=" + likeValueCurrentCount +
                ", nameState='" + nameState + '\'' +
                ", monologueState=" + monologueState +
                ", isMatchmakerUser=" + isMatchmakerUser +
                ", isVerifyIdentity=" + isVerifyIdentity +
                ", isVerifyServiceman=" + isVerifyServiceman +
                ", isShowLoveInsurance=" + isShowLoveInsurance +
                ", infoLevel=" + infoLevel +
                ", isBuyInsurance=" + isBuyInsurance +
                ", isVerifyQQ=" + isVerifyQQ +
                ", isVerifyMobile=" + isVerifyMobile +
                ", pets='" + pets + '\'' +
                ", sports='" + sports + '\'' +
                ", exerciseHabits=" + exerciseHabits +
                ", smoke=" + smoke +
                ", drink=" + drink +
                ", wantsKids=" + wantsKids +
                ", haveKids=" + haveKids +
                ", howManyKids=" + howManyKids +
                ", eyeColor=" + eyeColor +
                ", hairColor=" + hairColor +
                ", ethnicity='" + ethnicity + '\'' +
                ", faith=" + faith +
                ", politicalViews=" + politicalViews +
                ", books='" + books + '\'' +
                ", characteristics='" + characteristics + '\'' +
                ", foods='" + foods + '\'' +
                ", movies='" + movies + '\'' +
                ", music='" + music + '\'' +
                ", travel='" + travel + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.userBaseEnglish, flags);
        dest.writeString(this.account);
        dest.writeString(this.password);
        dest.writeString(this.birthday);
        dest.writeString(this.name);
        dest.writeString(this.cardNo);
        dest.writeString(this.mobile);
        dest.writeString(this.listHobby);
        dest.writeString(this.loginTime);
        dest.writeInt(this.occupation);
        dest.writeInt(this.bloodId);
        dest.writeInt(this.sign);
        dest.writeInt(this.houseId);
        dest.writeInt(this.diffAreaLoveId);
        dest.writeInt(this.loverTypeId);
        dest.writeInt(this.sexBefMarried);
        dest.writeInt(this.liveWithParentId);
        dest.writeInt(this.charm);
        dest.writeInt(this.weight);
        dest.writeInt(this.wantsKids);
        dest.writeInt(this.isBeanUser);
        dest.writeInt(this.isMonthly);
        dest.writeInt(this.level);
        dest.writeInt(this.isAuthentication);
        dest.writeInt(this.onlineState);
        dest.writeInt(this.isFollow);
        dest.writeInt(this.isBlackList);
        dest.writeInt(this.isSayHello);
        dest.writeInt(this.followNum);
        dest.writeInt(this.beanCurrencyCount);
        dest.writeString(this.nameState);
        dest.writeInt(this.monologueState);
        dest.writeInt(this.isMatchmakerUser);
        dest.writeInt(this.isVerifyIdentity);
        dest.writeInt(this.isVerifyServiceman);
        dest.writeInt(this.isShowLoveInsurance);
        dest.writeInt(this.infoLevel);
        dest.writeInt(this.isBuyInsurance);
        dest.writeInt(this.isVerifyQQ);
        dest.writeInt(this.isVerifyMobile);
        dest.writeTypedList(this.listImage);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.userBaseEnglish = in.readParcelable(UserBase.class.getClassLoader());
        this.account = in.readString();
        this.password = in.readString();
        this.birthday = in.readString();
        this.name = in.readString();
        this.cardNo = in.readString();
        this.mobile = in.readString();
        this.listHobby = in.readString();
        this.loginTime = in.readString();
        this.occupation = in.readInt();
        this.bloodId = in.readInt();
        this.sign = in.readInt();
        this.houseId = in.readInt();
        this.diffAreaLoveId = in.readInt();
        this.loverTypeId = in.readInt();
        this.sexBefMarried = in.readInt();
        this.liveWithParentId = in.readInt();
        this.charm = in.readInt();
        this.weight = in.readInt();
        this.wantsKids = in.readInt();
        this.isBeanUser = in.readInt();
        this.isMonthly = in.readInt();
        this.level = in.readInt();
        this.isAuthentication = in.readInt();
        this.onlineState = in.readInt();
        this.isFollow = in.readInt();
        this.isBlackList = in.readInt();
        this.isSayHello = in.readInt();
        this.followNum = in.readInt();
        this.beanCurrencyCount = in.readInt();
        this.nameState = in.readString();
        this.monologueState = in.readInt();
        this.isMatchmakerUser = in.readInt();
        this.isVerifyIdentity = in.readInt();
        this.isVerifyServiceman = in.readInt();
        this.isShowLoveInsurance = in.readInt();
        this.infoLevel = in.readInt();
        this.isBuyInsurance = in.readInt();
        this.isVerifyQQ = in.readInt();
        this.isVerifyMobile = in.readInt();
        this.listImage = in.createTypedArrayList(Image.CREATOR);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
