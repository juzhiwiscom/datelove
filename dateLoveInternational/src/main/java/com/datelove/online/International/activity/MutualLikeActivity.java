package com.datelove.online.International.activity;

import android.graphics.Color;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.adapter.MutualLikeAdapter;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.PeiDuiBean;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.MatchInfoChangeEvent;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.library.widgets.RefreshRecyclerView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 匹配成功
 */
public class MutualLikeActivity extends BaseTitleActivity {
    @BindView(R.id.search_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    @BindView(R.id.search_fail)
    TextView mTvSearchFail;
//    @BindView(R.id.listview)
//    ListView listview;
    Unbinder unbinder;
    private boolean fromMatch=false;

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("wwwwwwwwSearchFragment");
    }

    private MutualLikeAdapter mSearchAdapter;
    private int pageNum = 1;
    private final String pageSize = "10";
    /**
     * 搜索随机因子，避免搜索重复
     */
    private String randomNum = "0";
    /**
     * 记录当前查看的用户处于列表中的索引
     */
    private int mCurrentPosition = -1;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_search;
    }

    @Override
    protected String getCenterTitle() {
        return null;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.mutual_like));
        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(MutualLikeActivity.this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MutualLikeActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        mSearchAdapter = new MutualLikeAdapter(MutualLikeActivity.this, R.layout.item_fragment_search);
        mRefreshRecyclerView.setAdapter(mSearchAdapter);
    }

    @Override
    protected void addListeners() {
        /**加载读信拦截对话框**/
        final boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);

        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(MutualLikeActivity.this, MatchInfoActivity.class, false);
            }
        });
        mTvSearchFail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(MutualLikeActivity.this, MatchInfoActivity.class, false);
            }
        });
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                    pageNum++;
                    loadSearchData();
            }
        });
        mSearchAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                mCurrentPosition = position;
                PeiDuiBean.MatcherList matchUser = mSearchAdapter.getItemByPosition(position);
                if (matchUser != null) {
                    matchUser.setIsRead(1);
                    mSearchAdapter.updateItem(position,matchUser);
                    ChatActivity.toChatActivity(MutualLikeActivity.this, matchUser.getUserBaseEnglish().getId(), matchUser.getUserBaseEnglish().getNickName(), matchUser.getUserBaseEnglish().getImage().imageUrl);
                }
            }
        });
    }

    private void refresh() {
        pageNum = 1;
        loadSearchData();
    }
//
    private void loadSearchData() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_MATCHER_LIST)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", "15")
                .build()
                .execute(new Callback<PeiDuiBean>() {
                    @Override
                    public PeiDuiBean parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, PeiDuiBean.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(final PeiDuiBean response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                final List<PeiDuiBean.MatcherList> matcherList = response.getMatcherList();
                                if (Util.isListEmpty(matcherList)) {
                                    mTvSearchFail.setVisibility(View.VISIBLE);
                                }
                                // 延时1秒显示
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSearchAdapter != null) {
                                            if (pageNum == 1) {
                                                mSearchAdapter.replaceAll(matcherList);
                                            }
// else {
//                                                mSearchAdapter.appendToList(seeMeList);
//                                            }
                                        }
                                    }
                                }, 1000);
                            }
                        } else {
                            mTvSearchFail.setVisibility(View.VISIBLE);
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
        dismissLoading();
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(MatchInfoChangeEvent event) {
        showLoading();
        refresh();
    }
}
