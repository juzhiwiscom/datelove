package com.datelove.online.International.activity;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.adapter.CareAdapter;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.Care;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.RegistEvent;
import com.datelove.online.International.event.UpdateCareEvent;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.widgets.RefreshRecyclerView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/22.
 */
public class MyCareActivity extends BaseTitleActivity {
    @BindView(R.id.care_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;

    private CareAdapter mSearchAdapter;
    private int pageNum = 1;
    private final String pageSize = "30";

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_care;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_care);
    }

    @Override
    protected void initViewsAndVariables() {
        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(MyCareActivity.this));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyCareActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        mSearchAdapter = new CareAdapter(MyCareActivity.this, R.layout.item_care);
        mRefreshRecyclerView.setAdapter(mSearchAdapter);
    }

    @Override
    protected void addListeners() {
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                pageNum++;
                loadCareUserList();
            }
        });
        mSearchAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                User user = mSearchAdapter.getItemByPosition(position);
                if (user != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(MyCareActivity.this, user.getUserBaseEnglish().getId(),
                            null, UserInfoDetailActivity.SOURCE_CARE);
                }
            }
        });
    }

    private void refresh() {
        pageNum = 1;
        loadCareUserList();
    }

    private void loadCareUserList() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_FOLLOW_LIST)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .build()
                .execute(new Callback<Care>() {
                    @Override
                    public Care parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, Care.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(final Care response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (mSearchAdapter != null) {
                                    if (pageNum == 1) {
                                        mSearchAdapter.replaceAll(response.getUserEnglishList());
                                    } else {
                                        mSearchAdapter.appendToList(response.getUserEnglishList());
                                    }
                                }
                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
        dismissLoading();
    }
    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        // 关闭MainActivity事件
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(UpdateCareEvent event) {
        refresh();
    }


}
