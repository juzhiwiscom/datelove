package com.datelove.online.International.bean;

/**
 * 推荐用户
 * Created by zhangdroid on 2017/4/19.
 */
public class RecommendUser {
    public String isSucceed;
    private int cycle;// 轮询周期（秒）
    private String sentence;// 用户描述
    private User user;// 用户对象

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public int getCycle() {
        return cycle;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "RecommendUser{" +
                "isSucceed='" + isSucceed + '\'' +
                ", cycle='" + cycle + '\'' +
                ", sentence='" + sentence + '\'' +
                ", user=" + user +
                '}';
    }

}
