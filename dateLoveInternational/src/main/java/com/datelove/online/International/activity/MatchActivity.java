package com.datelove.online.International.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseActivity;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.Util;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 匹配页面
 * Created by zhangdroid on 2016/7/9.
 */
public class MatchActivity extends BaseActivity {
    @BindView(R.id.match_my_avatar)
    ImageView mIvMyAvatar;// 我的头像
    @BindView(R.id.match_other_avatar)
    ImageView mIvOtherAvatar;// 对方头像
    @BindView(R.id.match_content)
    TextView mTvContent;// 内容
    @BindView(R.id.match_chat)
    Button mBtnChat;// 发消息
    @BindView(R.id.match_continue)
    Button mBtnContinue;// 继续
    @BindView(R.id.rl_match)
    RelativeLayout rlMatch;

    private User mMatchUser;
    private String mUserId;
    private String mNickname;
    private String mAvatarUrl;
    private static final String INTENT_KEY_MATCH_USER = "intent_key_matchUser";

    public static void toMatchActivity(Activity from, User matchUser) {
        Util.gotoActivity(from, MatchActivity.class, false, INTENT_KEY_MATCH_USER, matchUser);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_match;
    }

    @Override
    protected void initViewsAndVariables() {
//        rlMatch.getBackground().mutate().setAlpha(80);
//        rlMatch.getBackground().setAlpha(80);
        ImageLoaderUtil.getInstance().loadImage(MatchActivity.this, new ImageLoader.Builder().url(UserInfoXml.getAvatarUrl())
                .placeHolder(R.drawable.placeholder_circle).transform(new CircleTransformation()).imageView(mIvMyAvatar).build());
        Intent intent = getIntent();
        if (intent != null) {
            mMatchUser = intent.getParcelableExtra(INTENT_KEY_MATCH_USER);
            if (mMatchUser != null) {
                UserBase userBase = mMatchUser.getUserBaseEnglish();
                if (userBase != null) {
                    mUserId = userBase.getId();
                    Image image = userBase.getImage();
                    if (image != null) {// 头像
                        mAvatarUrl = image.getThumbnailUrl();
                    }
                    ImageLoaderUtil.getInstance().loadImage(MatchActivity.this, new ImageLoader.Builder().url(mAvatarUrl)
                            .placeHolder(R.drawable.placeholder_circle).transform(new CircleTransformation()).imageView(mIvOtherAvatar).build());
                    mNickname = userBase.getNickName();
                    if (!TextUtils.isEmpty(mNickname)) {
                        mTvContent.setText(getString(R.string.match_success_tip, mNickname));
                    }
                }
            }
        }
    }

    @Override
    protected void addListeners() {
        mBtnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatActivity.toChatActivity(MatchActivity.this, mUserId, mNickname, mAvatarUrl);
//                ChatActivity.toChatActivity(MatchActivity.this,"1", mUserId, mNickname, mAvatarUrl,null,ChatActivity.MENU_FROM_RIGHT_MATCH);

                finish();
            }
        });
        mBtnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
