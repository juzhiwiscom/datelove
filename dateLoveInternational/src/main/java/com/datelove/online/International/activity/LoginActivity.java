package com.datelove.online.International.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseFragmentActivity;
import com.datelove.online.International.bean.FindPwdBean;
import com.datelove.online.International.bean.LoginEntity;
import com.datelove.online.International.constant.CommonData;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.FinishEvent;
import com.datelove.online.International.utils.AppsflyerUtils;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.CountryFidUtils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.dialog.OnSingleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/18.
 */
public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.login_account)
    EditText login_account;
    @BindView(R.id.login_password)
    EditText login_password;
    @BindView(R.id.register)
    TextView register;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.forget_password)
    TextView forget_password;
    @BindView(R.id.iv_loading_heart)
    ImageView ivLoadingHeart;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    @BindView(R.id.tv_loading)
    TextView tvLoading;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViewsAndVariables() {
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(getResources().getColor(R.color.register_bg));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (UserInfoXml.getUsername() != null) {
            login_account.setText(UserInfoXml.getUsername());
        }
        if (UserInfoXml.getPassword() != null) {
            login_password.setText(UserInfoXml.getPassword());
        }
    }

    @Override
    protected void addListeners() {
        register.setOnClickListener(this);
        login.setOnClickListener(this);
        forget_password.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                Util.gotoActivity(LoginActivity.this, RegisterActivity.class, false);
                break;

            case R.id.login:
                login();
                break;

            case R.id.forget_password:
                findPwd();
                break;
        }
    }

    public void login() {
        String account = login_account.getText().toString();
        String password = login_password.getText().toString();
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            ToastUtil.showShortToast(LoginActivity.this, getString(R.string.login_error));
            return;
        }
        getCountryLanguageFromAccount(account);
        showLoad();
        OkHttpUtils.post()
                .url(IUrlConstant.URL_LOGIN)
                .addHeader("token", TextUtils.isEmpty(PlatformInfoXml.getToken()) ? "" : PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pushToken", PlatformInfoXml.getPushToken())
                .addParams("account", account)
                .addParams("password", password)
                .build()
                .execute(new LoginCallBack());
    }

    private void showLoad() {
        flLoading.setVisibility(View.VISIBLE);
        tvLoading.setVisibility(View.VISIBLE);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_show_loading);
        ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
    }

    /**
     * 根据用户账号解析出国家语言等相关platform信息
     *
     * @param account
     */
    private void getCountryLanguageFromAccount(String account) {
        String countryAbbr = account.substring(0, 2).toUpperCase();
        String country = CommonData.getCountryLanguageMap().get(countryAbbr);
        if (country.contains(" ")) {
            country = country.replaceAll(" ", "");
        }
        if (!TextUtils.equals(PlatformInfoXml.getPlatformInfo().getCountry(), country)) {

            PlatformInfoXml.setCountry(country);
            PlatformInfoXml.setFid(CountryFidUtils.getCountryFidMap().get(country));
        }
        String languageAbbr = account.substring(2, 4).toUpperCase();
//        印度的国家和语言简写重复
        String language = CommonData.getCountryLanguageMap().get(languageAbbr);
        if(language.equals("France")){
            language="French";
        }
        if (!TextUtils.equals(PlatformInfoXml.getPlatformInfo().getLanguage(), language)) {
            PlatformInfoXml.setLanguage(language);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    public class LoginCallBack extends Callback<LoginEntity> {
        @Override
        public LoginEntity parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, LoginEntity.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            dismissLoad();
            ToastUtil.showShortToast(LoginActivity.this, getString(R.string.login_fail));
        }

        @Override
        public void onResponse(LoginEntity response, int id) {
            if (response != null) {
                if (!TextUtils.isEmpty(response.getMsg())) {
                    ToastUtil.showShortToast(LoginActivity.this, response.getMsg());
                }
                String isSucceed = response.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                    PlatformInfoXml.setToken(response.getToken());
                    UserInfoXml.setUserInfo(response.getUserEnglish());
                    // 设置页眉和互推开关
                    UserInfoXml.setIsShowHeadMsg(response.getIsShowHeadNotice());
                    UserInfoXml.setIsShowHeadMsgNew(response.getIsShowHeadNotice2());
                    UserInfoXml.setIsShowRecommendUser(response.getIsShowCommendUser());
                    UserInfoXml.setSearchInterceptor(response.getSearchInterceptor());
//                    保存性别，为了在个人资料中更换背景图
                    SharedPreferenceUtil.setStringValue(LoginActivity.this,"REGISTER_GENDER","genter",response.getUserEnglish().getUserBaseEnglish().getSex()+"");
                    // appsflyer登录
                    AppsflyerUtils.login();
                    CommonRequestUtil.initParamsDict(new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {
                            gotoMainActivity();
                        }

                        @Override
                        public void onFail() {
                            gotoMainActivity();
                        }
                    });
                } else {
                    dismissLoad();
                    ToastUtil.showShortToast(LoginActivity.this, getString(R.string.login_fail));
                }
            } else {
                dismissLoad();
                ToastUtil.showShortToast(LoginActivity.this, getString(R.string.login_fail));
            }
        }
    }

    private void gotoMainActivity() {
        // 发送事件，finish其它Activity
        EventBus.getDefault().post(new FinishEvent());
        dismissLoad();
        Util.gotoActivity(LoginActivity.this, MainActivity.class, true);
    }

    private void dismissLoad() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flLoading.setVisibility(View.GONE);
                tvLoading.setVisibility(View.GONE);
            }
        }, 1000);
    }

    private void findPwd() {
        if (!TextUtils.isEmpty(PlatformInfoXml.getToken()) && !TextUtils.isEmpty(UserInfoXml.getUsername()) && !TextUtils.isEmpty(UserInfoXml.getPassword())) {
            showLoad();
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_FIND_PWD)
                    .addHeader("token", PlatformInfoXml.getToken())
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .build()
                    .execute(new FindPwdCallBack());
        } else {
            ToastUtil.showShortToast(LoginActivity.this, getString(R.string.never_login));
        }
    }

    public class FindPwdCallBack extends Callback<FindPwdBean> {
        @Override
        public FindPwdBean parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, FindPwdBean.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            dismissLoad();
        }

        @Override
        public void onResponse(FindPwdBean response, int id) {
            if (response != null) {
                if (response.getAccount() != null && response.getPassword() != null) {
                    DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), getString(R.string.find_pwd), getString(R.string.account) + response.getAccount() + "\n" + getString(R.string.password) + response.getPassword(),
                            getString(R.string.positive), true, new OnSingleDialogClickListener() {
                                @Override
                                public void onPositiveClick(View view) {
                                }
                            });
                }
            }
            dismissLoad();
        }
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

}
