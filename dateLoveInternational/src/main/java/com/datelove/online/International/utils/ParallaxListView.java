package com.datelove.online.International.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.ListView;

import com.datelove.online.International.base.BaseApplication;
import com.library.utils.DensityUtil;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener;
/**
 * 视差特效
 * Midified by tian on 2017/11/02.
 */
public class ParallaxListView extends RecyclerView {
	
	private ImageView parallaxImageView;
	private int maxHeight;
	private int originH;

	public ParallaxListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ParallaxListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ParallaxListView(Context context) {
		super(context);
	}

	// 允许从外部传递视差效果的控件进来
	public void setParallaxImageView(ImageView parallaxImageView) {
		this.parallaxImageView = parallaxImageView;
		
		Drawable drawable = parallaxImageView.getDrawable();
//		maxHeight = drawable.getIntrinsicHeight();// 获取图片的最大高度
        maxHeight=DensityUtil.dip2px(BaseApplication.getGlobalContext(), 500);
		originH = parallaxImageView.getHeight();// 获取图片的起始高度
		System.out.println("ParallaxListView.setParallaxImageView,originH="+originH);
	}
	
	@Override
	// 当列表被滑动到两端尽头的时候被调用
	// deltaY 两次滑动间的变化大小
	protected boolean overScrollBy(int deltaX, int deltaY, int scrollX,
			int scrollY, int scrollRangeX, int scrollRangeY,
			int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
		
		System.out.println("deltaY="+deltaY+";scrollY="+scrollY+";scrollRangeY="+scrollRangeY+";maxOverScrollY="+maxOverScrollY+";isTouchEvent="+isTouchEvent);
		
		if (deltaY < 0 && isTouchEvent) {
			int newHeight = parallaxImageView.getHeight() + Math.abs(deltaY / 2);
			if (newHeight < maxHeight) {
				parallaxImageView.getLayoutParams().height = newHeight;
				parallaxImageView.requestLayout();
			}
		}
		
		return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
				scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		
		switch (ev.getAction()) {
		case MotionEvent.ACTION_UP:
			// 将图片从当前高度弹回最初的高度
			final int currentH = parallaxImageView.getHeight();
			
			// 动态生成一个值的变化
			ValueAnimator animator = ValueAnimator.ofFloat(1);
			animator.addUpdateListener(new AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator value) {
					System.out.println("ParallaxListView.onAnimationUpdate,time="+value.getCurrentPlayTime()+";Fraction="+value.getAnimatedFraction()+";value="+value.getAnimatedValue());
					
					int newHeight = evaluate(value.getAnimatedFraction(), currentH, originH);
					parallaxImageView.getLayoutParams().height = newHeight;
					parallaxImageView.requestLayout();
				}
			});
			animator.setDuration(200);
			animator.setInterpolator(new OvershootInterpolator(4));
			animator.start();
			break;

		default:
			break;
		}
		
		return super.onTouchEvent(ev);// 一定要使用super来返回，因为列表的滚动要由ListView处理
	}
	
    public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
        int startInt = startValue;
        return (int)(startInt + fraction * (endValue - startInt));
    }
}
