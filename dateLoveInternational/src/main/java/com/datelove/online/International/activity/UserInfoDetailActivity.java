package com.datelove.online.International.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.datelove.online.International.R;
import com.datelove.online.International.bean.FateUser;
import com.datelove.online.International.bean.HeadMsgNotice;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.MsgBox;
import com.datelove.online.International.bean.NewHeadMsgNotice;
import com.datelove.online.International.bean.RecommendUser;
import com.datelove.online.International.bean.SayHello;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.bean.UserDetail;
import com.datelove.online.International.constant.CommonData;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.dialog.ReportDialog;
import com.datelove.online.International.event.HeadMsgEvent;
import com.datelove.online.International.event.MessageChangedEvent;
import com.datelove.online.International.event.SayHelloEvent;
import com.datelove.online.International.event.UnlikeEvent;
import com.datelove.online.International.event.UpdateCareEvent;
import com.datelove.online.International.event.UpdateNextUser;
import com.datelove.online.International.haoping.ActionSheetDialog;
import com.datelove.online.International.haoping.OnOperItemClickL;
import com.datelove.online.International.utils.Cheeses;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.FastBlurUtil;
import com.datelove.online.International.utils.ParallaxListView;
import com.datelove.online.International.utils.ParamsUtils;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.HeightUtils;
import com.library.utils.LogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.library.widgets.CircleImageView;
import com.library.widgets.CollapsingTextView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 用户详情页
 * Created by zhangdroid on 2016/10/22.
 */
public class UserInfoDetailActivity extends AppCompatActivity {
    public static final String INTENT_KEY_UID = "intent_key_uId";
    public static final String INTENT_KEY_ISRECORD = "intent_key_isRecord";
    public static final String INTENT_KEY_SOURCETAG = "intent_key_sourceTag";

    /**
     * 缘分页面
     */
    public static final String SOURCE_FROM_FATE = "1";
    /**
     * 搜索页面
     */
    public static final String SOURCE_FROM_SEARCH = "2";
    /**
     * 聊天列表页面
     */
    public static final String SOURCE_FROM_CHAT_LIST = "3";
    /**
     * 聊天内容页面
     */
    public static final String SOURCE_FROM_CHAT_CONTENT = "4";
    /**
     * 附近页面
     */
    public static final String SOURCE_FROM_NEARBY = "5";
    /**
     * 谁看过我
     */
    public static final String SOURCE_FROM_SEEME = "6";
    /**
     * 关注
     */
    public static final String SOURCE_CARE = "7";
    /**
     * 頁眉
     */
    public static final String SOURCE_HEAD_MSG = "8";
    /**
     * 推荐用户
     */
    public static final String SOURCE_FROM_RECOMMEND_USER = "9";
    private static Integer bitmapNum;
    static List<Bitmap> getBitmapList = new ArrayList<Bitmap>();

    @BindView(R.id.userDetail_CollapsingToolbarLayout)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.userDetail_toolbar)
    Toolbar mToolbar;
    // FAB 下一位用户
    @BindView(R.id.userDetail_fab_next)
    FloatingActionButton mFabNext;
    // 用户头像和照片墙
    @BindView(R.id.userDetail_avatar)
    ImageView mIvUserAvatar;
    @BindView(R.id.userDetail_pic_wall)
    ParallaxListView mPicRecyclerView;
    // 展示的基本信息
    @BindView(R.id.userDetail_base_info)
    TextView mTvBaseInfo;

    // 关注、打招呼和聊天
    @BindView(R.id.userDetail_un_like)
    Button mBtnFollow;
    @BindView(R.id.userDetail_sayHello)
    Button mBtnSayHello;
    @BindView(R.id.userDetail_chat)
    Button mBtnChat;
    // 内心独白
    @BindView(R.id.userDetail_monologue_ll)
    LinearLayout mLlMonologue;
    @BindView(R.id.userDetail_monologue)
    CollapsingTextView mMonologue;
    // 用户个人资料
    // 国家
    @BindView(R.id.userDetail_country_ll)
    LinearLayout mLlCountry;
    @BindView(R.id.userDetail_country)
    TextView mTvCountry;
    @BindView(R.id.userDetail_country_divider)
    View mCountryDivider;
    // 地区
    @BindView(R.id.userDetail_area_ll)
    LinearLayout mLlArea;
    @BindView(R.id.userDetail_area)
    TextView mTvArea;
    @BindView(R.id.userDetail_area_divider)
    View mAreaDivider;
    // 星座
    @BindView(R.id.userDetail_sign_ll)
    LinearLayout mLlSign;
    @BindView(R.id.userDetail_sign)
    TextView mTvSign;
    @BindView(R.id.userDetail_sign_divider)
    View mSignDivider;
    // 学历
    @BindView(R.id.userDetail_education_ll)
    LinearLayout mLlEducation;
    @BindView(R.id.userDetail_education)
    TextView mTvEducation;
    @BindView(R.id.userDetail_education_divider)
    View mEducationDivider;
    // 收入
    @BindView(R.id.userDetail_income_ll)
    LinearLayout mLlIncome;
    @BindView(R.id.userDetail_income)
    TextView mTvIncome;
    @BindView(R.id.userDetail_income_divider)
    View mIncomeDivider;
    // 职业
    @BindView(R.id.userDetail_occupation_ll)
    LinearLayout mLlOccupation;
    @BindView(R.id.userDetail_occupation)
    TextView mTvOccupation;
    @BindView(R.id.userDetail_occupation_divider)
    View mOccupationDivider;
    // 是否想要小孩
    @BindView(R.id.userDetail_wantBaby_ll)
    LinearLayout mLlWantBaby;
    @BindView(R.id.userDetail_wantBaby)
    TextView mTvWantBaby;
    @BindView(R.id.userDetail_want_baby_divider)
    View mWantBabyDivider;
    // 喜欢的运动
    @BindView(R.id.userDetail_sport_ll)
    LinearLayout mLlSport;
    @BindView(R.id.userDetail_sport)
    TextView mTvSport;
    @BindView(R.id.userDetail_sport_divider)
    View mSportDivider;
    // 喜欢的宠物
    @BindView(R.id.userDetail_pets_ll)
    LinearLayout mLlPets;
    @BindView(R.id.userDetail_pets)
    TextView mTvPets;
    @BindView(R.id.userDetail_pets_divider)
    View mPetsDivider;
    // 种族
    @BindView(R.id.userDetail_ethnicity_ll)
    LinearLayout mLlEthnicity;
    @BindView(R.id.userDetail_ethnicity)
    TextView mTvEthnicity;
    @BindView(R.id.userDetail_ethnicity_divider)
    View mEthnicityDivider;
    // 锻炼习惯
    @BindView(R.id.userDetail_exerciseHabits_ll)
    LinearLayout mLlExerciseHabits;
    @BindView(R.id.userDetail_exerciseHabits)
    TextView mTvExerciseHabits;
    @BindView(R.id.userDetail_exerciseHabits_divider)
    View mExerciseHabitsDivider;
    // 兴趣爱好
    @BindView(R.id.userDetail_hobby_ll)
    LinearLayout mLlHobby;
    @BindView(R.id.userDetail_hobby)
    TextView mTvHobby;
    @BindView(R.id.userDetail_hobby_divider)
    View mHobbyDivider;

    // 页眉
    @BindView(R.id.head_msg_container)
    RelativeLayout mRlHeadMsgContainer;
    @BindView(R.id.head_msg_content)
    TextView mTvHeadMsgContent;
    @BindView(R.id.head_msg_clear)
    ImageView mIvHeadMsgClear;
    // 新页眉
    @BindView(R.id.headMsg_container)
    FrameLayout mFlHeadMsgNew;
    @BindView(R.id.headMsg_unread)
    RelativeLayout mRlHeadMsgUnread;
    @BindView(R.id.headMsg_avatar)
    CircleImageView mIvHeadMsgAvatar;
    @BindView(R.id.headMsg_nickname)
    TextView mTvNickname;
    @BindView(R.id.headMsg_age)
    TextView mTvAge;
    @BindView(R.id.headMsg_height)
    TextView mTvHeight;
    @BindView(R.id.headMsg_content)
    TextView mTvMsgContent;
    @BindView(R.id.headMsg_check)
    TextView mTvMsgCheck;
    // 访客
    @BindView(R.id.headMsg_visitor)
    RelativeLayout mRlHeadMsgVisitor;
    @BindView(R.id.visitor_avatar)
    CircleImageView mIvVisitorAvatar;
    @BindView(R.id.visitor_content)
    TextView mTvVisitorContent;
    @BindView(R.id.visitor_ignore)
    TextView mTvVisitorIgnore;
    @BindView(R.id.visitor_look)
    TextView mTvVisitorLook;
    // 推荐用户
    @BindView(R.id.recommend_user_container)
    LinearLayout mLlRecommendUser;
    @BindView(R.id.recommend_user_avatar)
    CircleImageView mIvRecommendAvatar;
    @BindView(R.id.recommend_user_content)
    TextView mTvRecommendContent;
    @BindView(R.id.recommend_user_ignore)
    TextView mTvRecommendIgnore;
    @BindView(R.id.recommend_user_look)
    TextView mTvRecommendLook;
//    @BindView(R.id.iv_loading_heart)
//    ImageView ivLoadingHeart;
    @BindView(R.id.fl_loading)
     FloatingActionButton flLoading;
//    @BindView(R.id.tv_loading)
//    TextView tvLoading;
  @BindView(R.id.tv_introduce)
   TextView tvIntroduce;
    @BindView(R.id.rl_introduce)
    RelativeLayout rlIntroduce;
    @BindView(R.id.fl_bottom)
    FrameLayout mFlBootm;

    private String isRecord;
    private String sourceTag;
    private String uId;
    private String nickname;
    private String avatarUrl;
    private String country;
    /**
     * 进度加载对话框
     */
    private Dialog mLoadingDialog;
    private ImageWallAdapter mImageWallAdapter;
    private int mIsFollow=0;
    private int mIsSayHello=0;
    private int pageNum = 0;
    private final String pageSize = "1";

    private static final int HEAD_MSG_REFRESH_PERIOD = 20 * 1000;
    private static final int MSG_TYPE_LOAD_DATA = 1;
    private static final int MSG_TYPE_HEAD_MSG = 2;
    private static final int MSG_TYPE_HEAD_MSG_NEW = 3;
    private static final int MSG_TYPE_RECOMMEND_USER = 4;
    private static final int MSG_TYPE_SHOW_UNREAD_MSG = 5;
    private static final int MSG_TYPE_DISMISS_UNREAD_MSG = 6;
    private static final int MSG_TYPE_DISMISS_VISITOR = 7;
    private static final int MSG_TYPE_HIDE_RECOMMEND_USER = 8;
    private boolean mReceiverTag = false;   //广播接受者标识
    /**
     * 页眉对象缓存
     */
    private HeadMsgNotice mHeadMsgNotice;
    /**
     * 新页眉对象缓存
     */
    private NewHeadMsgNotice mNewHeadMsgNotice;
    /**
     * 推荐用户对象缓存
     */
    private RecommendUser mRecommendUser;
    /**
     * 新页眉最后一条未读消息毫秒数
     */
    private long mLastMsgTime;
    /**
     * 推荐用户轮询周期（秒）
     */
    private int cycle;
    /**
     * 新页眉中当前正在显示的未读消息索引
     */
    private int mCurrDisplayItem = 0;
    private TimerHandler mTimerHandler;
    private Vibrator vibrator;
    boolean isShake = true;
    //用来判断是否第一次进入此类，如果是，就使用通过eventBus传过来的mCurrDisplayItem值
    private int count;
    private boolean isIntercept;
    private String getCountry=PlatformInfoXml.getPlatformInfo().getCountry().trim();
    private int isBlack=0;
    private User newUser;

    private class TimerHandler extends Handler {
        private WeakReference<UserInfoDetailActivity> mWeakReference;

        public TimerHandler(UserInfoDetailActivity userInfoDetailActivity) {
            this.mWeakReference = new WeakReference<UserInfoDetailActivity>(userInfoDetailActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            if (null == this) { //走到了onDestory,则不再进行后续消息处理
                return;
            }
            if (UserInfoDetailActivity.this.isFinishing()) { //Activity正在停止，则不再后续处理
                return;
            }
            final UserInfoDetailActivity userInfoDetailActivity = mWeakReference.get();
            switch (msg.what) {
                case MSG_TYPE_LOAD_DATA:// 加载数据
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.loadData();
                    }
                    break;

                case MSG_TYPE_HEAD_MSG:// 页眉刷新
//                    if (userInfoDetailActivity != null) {
//                        userInfoDetailActivity.getHeadMsg();
//                    }
//                    sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG, HEAD_MSG_REFRESH_PERIOD);
                    break;
                case MSG_TYPE_HEAD_MSG_NEW:// 新页眉刷新
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.getNewHeadMsg();
                    }
                    break;
                case MSG_TYPE_RECOMMEND_USER:// 推荐用户刷新
                    int cycle = 0;
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.getRecommendUsr();
                        cycle = userInfoDetailActivity.cycle;
                    }
//                    sendEmptyMessageDelayed(MSG_TYPE_RECOMMEND_USER, cycle < 20 ? HEAD_MSG_REFRESH_PERIOD : cycle * 1000);
                    break;

                case MSG_TYPE_SHOW_UNREAD_MSG:// 显示未读消息页眉
                    // 先隐藏上一条
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mFlHeadMsgNew, userInfoDetailActivity.mRlHeadMsgUnread);
                        userInfoDetailActivity.mFlHeadMsgNew.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // 如果存在下一条，则显示
                                if (userInfoDetailActivity.mNewHeadMsgNotice != null && userInfoDetailActivity.mCurrDisplayItem < userInfoDetailActivity.mNewHeadMsgNotice.getUnreadMsgBoxList().size() - 1) {
                                    userInfoDetailActivity.mCurrDisplayItem++;
                                    userInfoDetailActivity.setNewHeadMsg();
                                } else {// 继续轮询
                                    sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
                                }
                            }
                        }, 500);
                    }
                    break;
                case MSG_TYPE_DISMISS_UNREAD_MSG:// 隐藏未读消息页眉
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mFlHeadMsgNew, userInfoDetailActivity.mRlHeadMsgUnread);
                    }
                    break;
                case MSG_TYPE_HIDE_RECOMMEND_USER://隐藏推荐用户页眉
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mLlRecommendUser, userInfoDetailActivity.mLlRecommendUser);
                    }
                    sendEmptyMessageDelayed(MSG_TYPE_RECOMMEND_USER, HEAD_MSG_REFRESH_PERIOD);
                    break;
                case MSG_TYPE_DISMISS_VISITOR:// 隐藏访客页眉
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mFlHeadMsgNew, userInfoDetailActivity.mRlHeadMsgVisitor);
                    }
                    sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
                    break;
            }
        }

    }

    public static void toUserInfoDetailActivity(Activity from, String uId, String isRecord, String sourceTag) {

        Map<String, String> map = new HashMap<String, String>();
        map.put(INTENT_KEY_UID, uId);
        map.put(INTENT_KEY_ISRECORD, TextUtils.isEmpty(isRecord) ? "0" : isRecord);
        map.put(INTENT_KEY_SOURCETAG, sourceTag);
        Util.gotoActivity(from, UserInfoDetailActivity.class, false, map);

    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        if (intent != null) {
            uId = intent.getStringExtra(INTENT_KEY_UID);
            isRecord = intent.getStringExtra(INTENT_KEY_ISRECORD);
            sourceTag = intent.getStringExtra(INTENT_KEY_SOURCETAG);
        }

        // 初始化照片墙RecyclerView
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPicRecyclerView.setLayoutManager(linearLayoutManager);
        mImageWallAdapter = new ImageWallAdapter(this, R.layout.item_pic_wall);
        mPicRecyclerView.setAdapter(mImageWallAdapter);
        // 已付费男用户和女用户显示聊天
        mBtnChat.setVisibility(View.GONE);
        if (UserInfoXml.isMale()) {
            if (UserInfoXml.isMonthly()) {
                mBtnFollow.setVisibility(View.GONE);
                mBtnSayHello.setVisibility(View.GONE);
//                mBtnChat.setVisibility(View.VISIBLE);
            } else {
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnSayHello.setVisibility(View.VISIBLE);
//                mBtnChat.setVisibility(View.GONE);
            }
        } else {
            mBtnFollow.setVisibility(View.GONE);
            mBtnSayHello.setVisibility(View.GONE);
//            mBtnChat.setVisibility(View.VISIBLE);
        }

        addListeners();
        // 加载用户详情
        showLoad();
        loadUserDetailInfo();
        // 延时加载数据
        mTimerHandler = new TimerHandler(this);
        mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_LOAD_DATA, 1000 * 3);
        //注册广播
//        registerReceiver(mHomeKeyEventReceiver, new IntentFilter(
//                Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        if (!mReceiverTag){     //在注册广播接受者的时候 判断是否已被注册,避免重复多次注册广播
            IntentFilter mFileter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            mReceiverTag = true;    //标识值 赋值为 true 表示广播已被注册
            this.registerReceiver(mHomeKeyEventReceiver, mFileter);
        }
        // 当布局完成之后才能获取到控件高度
        mIvUserAvatar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mIvUserAvatar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                System.out.println("MainActivity.onGlobalLayout,");

                mPicRecyclerView.setParallaxImageView(mIvUserAvatar);// 设置要被视差效果处理的控件
            }
        });
        /**加载读信拦截对话框**/
        isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
        if (CommonData.getCountryList().contains(PlatformInfoXml.getCountry())) {
            getSelfIntroduce();
        }
    }


    /**
     * 加载页眉和推荐用户
     */
    private void loadData() {
        // 获取页眉
//        if (UserInfoXml.isShowHeadMsg()) {
//            getHeadMsg();
//            mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG, HEAD_MSG_REFRESH_PERIOD);
//        }
        // 获取新页眉
        if (UserInfoXml.isShowHeadMsgNew()) {
            getNewHeadMsg();
        }
        // 获取推荐用户
        if (UserInfoXml.isShowRecommendUser()) {
            getRecommendUsr();
//            mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_RECOMMEND_USER, HEAD_MSG_REFRESH_PERIOD);
        }
    }

    private void getHeadMsg() {
        CommonRequestUtil.getHeadMsg(new CommonRequestUtil.OnGetHeadMsgListener() {
            @Override
            public void onSuccess(HeadMsgNotice headMsgNotice) {
                if (headMsgNotice != null) {
                    mHeadMsgNotice = headMsgNotice;
                    String message = headMsgNotice.getMessage();
                    if (!TextUtils.isEmpty(message)) {
                        mRlHeadMsgContainer.setVisibility(View.VISIBLE);
                        mTvHeadMsgContent.setText(message);
                    } else {
                        mRlHeadMsgContainer.setVisibility(View.GONE);
                    }
                    String noticeType = headMsgNotice.getNoticeType();
                    if (!TextUtils.isEmpty(noticeType) && "1".equals(noticeType)) {
                        // 有未读消息时刷新消息列表
                        EventBus.getDefault().post(new MessageChangedEvent());
                    }
                }
            }

            @Override
            public void onFail() {

            }
        });
    }

    /**
     * 获取新页眉
     */
    private void getNewHeadMsg() {
        String lastMsgTime = new SharedPreferenceUtil(UserInfoDetailActivity.this).takeLastTime();
        if (!TextUtils.isEmpty(lastMsgTime)) {
            mLastMsgTime = Long.parseLong(lastMsgTime);
        }
        CommonRequestUtil.getNewHeadMsg(mLastMsgTime, new CommonRequestUtil.OnGetNewHeadMsgListener() {

            @Override
            public void onSuccess(NewHeadMsgNotice newHeadMsgNotice) {
                if (newHeadMsgNotice != null) {
                    mNewHeadMsgNotice = newHeadMsgNotice;
                    mLastMsgTime = newHeadMsgNotice.getLastMsgTime();
                    //将时间戳保存下来
                    new SharedPreferenceUtil(UserInfoDetailActivity.this).saveLastTime(mLastMsgTime);
//                    if(i!=0){
                    mCurrDisplayItem = 0;
//                    }
//                  i++;
                    String noticeType = newHeadMsgNotice.getNoticeType();
                    if (!TextUtils.isEmpty(noticeType)) {
                        switch (noticeType) {
                            case "0":// 没有页眉，继续轮询
                                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
                                break;
                            case "1":// 未读消息
                                // 刷新聊天列表
                                EventBus.getDefault().post(new MessageChangedEvent());
                                setNewHeadMsg();
                                break;

                            case "2":// 访客
                                FateUser fateUser = newHeadMsgNotice.getRemoteYuanfenUserBase();
                                if (fateUser != null) {
                                    UserBase userBase = fateUser.getUserBaseEnglish();
                                    if (userBase != null) {
                                        ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                                .url(userBase.getImage().getThumbnailUrl()).imageView(mIvVisitorAvatar).build());
                                        mTvVisitorContent.setText(getString(R.string.visitor_tip, userBase.getNickName()));
                                    }
                                }
                                mRlHeadMsgUnread.setVisibility(View.GONE);
                                mRlHeadMsgVisitor.setVisibility(View.VISIBLE);
                                showWithAnim(mFlHeadMsgNew);
                                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_DISMISS_VISITOR, newHeadMsgNotice.getDisplaySecond() * 1000);
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFail() {
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
            }
        });
    }

    /**
     * 按照未读消息列表顺序显示未读信
     */
    private void setNewHeadMsg() {
        if (mNewHeadMsgNotice != null) {
            List<MsgBox> msgBoxList = mNewHeadMsgNotice.getUnreadMsgBoxList();
            if (!Util.isListEmpty(msgBoxList) && mCurrDisplayItem < msgBoxList.size()) {
                MsgBox msgBox = msgBoxList.get(mCurrDisplayItem);
                if (msgBox != null) {
                    UserBase userBase = msgBox.getUserBaseEnglish();
                    if (userBase != null) {
                        Image image = userBase.getImage();
                        if (image != null) {
                            ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                    .url(image.getThumbnailUrl()).imageView(mIvHeadMsgAvatar).build());
                        }
                        mTvNickname.setText(userBase.getNickName());
                        mTvAge.setText(TextUtils.concat(String.valueOf(userBase.getAge())));
                        mTvAge.setSelected(userBase.getSex() == 0);
                        if (userBase.getSex() == 0) {
                            mTvAge.setBackgroundResource(R.drawable.shape_round_rectangle_blue_bg);
                        } else {
                            mTvAge.setBackgroundResource(R.drawable.shape_notification_age_women);
                        }
                        mTvHeight.setText(HeightUtils.getInchCmByCm(userBase.getHeightCm()));
                    }
                    String msgContent = msgBox.getMsg();
                    if (!TextUtils.isEmpty(msgContent)) {
//                        mTvMsgContent.setText(msgContent);
                        mTvMsgContent.setText(getString(R.string.get_msg));
                    }
                    mRlHeadMsgUnread.setVisibility(View.VISIBLE);
                    mRlHeadMsgVisitor.setVisibility(View.GONE);
                    showWithAnim(mFlHeadMsgNew);
                }
                // 一段时间后显示下一条未读消息
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_SHOW_UNREAD_MSG, mNewHeadMsgNotice.getDisplaySecond() * 1000);
            }
        }
    }

    private void showWithAnim(View view) {

        if (isShake) {
            view.setVisibility(View.VISIBLE);
            vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(200);
            view.startAnimation(AnimationUtils.loadAnimation(UserInfoDetailActivity.this, R.anim.anim_in));
        }


    }

    private void dismissWithAnim(final View view, final View view2) {
        Animation outAnimation = AnimationUtils.loadAnimation(UserInfoDetailActivity.this, R.anim.anim_out);
        view.startAnimation(outAnimation);
        outAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 获取推荐用户
     */
    private void getRecommendUsr() {
        CommonRequestUtil.getRecommendUser(cycle, new CommonRequestUtil.OnGetRecommendUserListener() {
            @Override
            public void onSuccess(RecommendUser recommendUser) {
                if (recommendUser != null) {
                    mRecommendUser = recommendUser;
                    cycle = recommendUser.getCycle();
                    User user = recommendUser.getUser();
                    if (user != null) {
                        UserBase userBase = user.getUserBaseEnglish();
                        if (userBase != null) {
                            // 显示推荐用户
                            mLlRecommendUser.setVisibility(View.VISIBLE);

                            mTvRecommendContent.setText(TextUtils.concat(userBase.getNickName(), " ", recommendUser.getSentence()));
                            ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                    .url(userBase.getImage().getThumbnailUrl()).imageView(mIvRecommendAvatar).build());
                            showWithAnim(mLlRecommendUser);
                        }
                    }
                }
                // 一段时间后隐藏推荐用户
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HIDE_RECOMMEND_USER, 5 * 1000);
            }

            @Override
            public void onFail() {
                // 一段时间后隐藏推荐用户
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HIDE_RECOMMEND_USER, 5 * 1000);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tool_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_report:// 举报
                actionSheetDialogNoTitle();
//                ReportDialog.newInstance(getString(R.string.report_title) + nickname, uId).show(getSupportFragmentManager(), "report");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addListeners() {
        // 左菜单点击事件处理
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // 点击查看图片大图
//        延迟两秒钟显示，为了防止在mainActivity中多次快速点击时误入
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIvUserAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int size = getBitmapList.size();
                        PictureBrowseActivity.toPictureBrowseActivity(UserInfoDetailActivity.this, 0, mImageWallAdapter.getAdapterData());
                    }
                });
            }
        },2000);

        mImageWallAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                boolean interceptLock = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                if (interceptLock && position!=0) {
                    BuyServiceActivity.toBuyServiceActivity(UserInfoDetailActivity.this, BuyServiceActivity.INTENT_FROM_MONTH,
                            IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_MONTH);
                } else {
                    PictureBrowseActivity.toPictureBrowseActivity(UserInfoDetailActivity.this, position, mImageWallAdapter.getAdapterData());
                }
            }
        });
        // 查看页眉消息
        mRlHeadMsgContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHeadMsgNotice != null) {
                    mRlHeadMsgContainer.setVisibility(View.GONE);
                    String noticeType = mHeadMsgNotice.getNoticeType();
                    if (!TextUtils.isEmpty(noticeType)) {
                        switch (noticeType) {
                            case "1": // 未读消息
                                // 切换到聊天tab
                                EventBus.getDefault().post(new HeadMsgEvent());
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 200);
                                break;

                            case "2":// 正在看我
                                showLoad();
                                uId = mHeadMsgNotice.getRemoteUserId();
                                sourceTag = SOURCE_HEAD_MSG;
                                loadUserDetailInfo();
                                break;
                        }
                    }
                }
            }
        });
        mIvHeadMsgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRlHeadMsgContainer.setVisibility(View.GONE);
            }
        });
        // 新页眉
        mRlHeadMsgUnread.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // 查看未读消息，进入聊天页面
                mFlHeadMsgNew.setVisibility(View.GONE);
                if (mNewHeadMsgNotice != null && mCurrDisplayItem < mNewHeadMsgNotice.getUnreadMsgBoxList().size()) {
                    List<MsgBox> msgBoxList = mNewHeadMsgNotice.getUnreadMsgBoxList();
                    if (!Util.isListEmpty(msgBoxList)) {
                        MsgBox msgBox = msgBoxList.get(mCurrDisplayItem);
                        if (msgBox != null) {
                            UserBase userBase = msgBox.getUserBaseEnglish();
                            if (userBase != null) {
                                ChatActivity.toChatActivity(UserInfoDetailActivity.this, userBase.getId(), userBase.getNickName(), userBase.getImage().getThumbnailUrl());
                                finish();
                            }
                        }
                    }
                }
            }
        });
        mTvVisitorLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏新页眉
                mRlHeadMsgVisitor.setVisibility(View.GONE);
                mFlHeadMsgNew.setVisibility(View.GONE);
                // 查看用户详情
                if (mNewHeadMsgNotice != null) {
                    FateUser fateUser = mNewHeadMsgNotice.getRemoteYuanfenUserBase();
                    if (fateUser != null) {
                        UserBase userBase = fateUser.getUserBaseEnglish();
                        if (userBase != null) {
                            showLoad();
                            uId = userBase.getId();
                            sourceTag = SOURCE_HEAD_MSG;
                            loadUserDetailInfo();
                        }
                    }
                }
            }
        });
        mTvVisitorIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏新页眉
                mRlHeadMsgVisitor.setVisibility(View.GONE);
                mFlHeadMsgNew.setVisibility(View.GONE);
            }
        });
        // 推荐用户
        mTvRecommendLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRecommendUser != null) {
                    UserBase userBase = mRecommendUser.getUser().getUserBaseEnglish();
                    if (userBase != null) {
                        // 隐藏推荐用户
                        mLlRecommendUser.setVisibility(View.GONE);
                        Image image = userBase.getImage();
                        if (UserInfoXml.isMale()) {
                            if (UserInfoXml.isMonthly()) {// 已付费男用户进入聊天页
                                ChatActivity.toChatActivity(UserInfoDetailActivity.this, userBase.getId(), userBase.getNickName(),
                                        image == null ? null : image.getThumbnailUrl());
                            } else {// 未付费男用户，进入对方空间页
                                showLoad();
                                uId = userBase.getId();
                                sourceTag = SOURCE_FROM_RECOMMEND_USER;
                                loadUserDetailInfo();
                            }
                        } else {// 女用户进入聊天页
                            ChatActivity.toChatActivity(UserInfoDetailActivity.this, userBase.getId(), userBase.getNickName(),
                                    image == null ? null : image.getThumbnailUrl());
                        }
                    }
                }
            }
        });
        mTvRecommendIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏推荐用户
                mLlRecommendUser.setVisibility(View.GONE);
            }
        });
    }

    /**
     * 显示加载对话框
     */
//    protected void showLoad() {
//        mLoadingDialog = Utils.showLoadDialog(this, true);
//    }
    private void showLoad() {
//        flLoading.setVisibility(View.GONE);
//        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(UserInfoDetailActivity.this, R.anim.anim_show_loading);
//        flLoading.startAnimation(hyperspaceJumpAnimation);
    }

    /**
     * 隐藏加载对话框
     */
//    protected void dismissLoad() {
//        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
//            mLoadingDialog.dismiss();
//        }
//    }
    private  void dismissLoad() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flLoading.setVisibility(View.GONE);
//                tvLoading.setVisibility(View.GONE);
            }
        }, 500);
    }

    @OnClick({R.id.userDetail_fab_next, R.id.userDetail_un_like, R.id.userDetail_sayHello, R.id.userDetail_chat})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.userDetail_fab_next:// 下一位用户
                if(!(getCountry.equals(getString(R.string.united_states).trim()) || getCountry.equals("America"))){
                    count++;
                    if(count>=10 && isIntercept){
//                        弹出拦截对话框
                        interceptDialog();
                    }else {
                        loadNextUser();
                    }
                }else{
                    loadNextUser();
                }
                /**加载读信拦截对话框**/
                isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                if (CommonData.getCountryList().contains(PlatformInfoXml.getCountry())) {
                    getSelfIntroduce();
                }
                
                break;

            case R.id.userDetail_un_like:
                CommonRequestUtil.unLike(uId,new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onFail() {

                        }
                    });
                EventBus.getDefault().post(new UnlikeEvent());
                EventBus.getDefault().post(new UpdateNextUser(false));
                finish();

//                if (mIsFollow == 1) {// 取消关注
//                    CommonRequestUtil.cancelFollow(uId, true, new CommonRequestUtil.OnCommonListener() {
//                        @Override
//                        public void onSuccess() {
//                            mIsFollow = 0;
//                            mBtnFollow.setSelected(false);
//                            mBtnFollow.setText(getString(R.string.follow));
////                            更新关注列表
//                            EventBus.getDefault().post(new UpdateCareEvent());
//                        }
//
//                        @Override
//                        public void onFail() {
//
//                        }
//                    });
//                } else {// 关注
//                    CommonRequestUtil.follow(uId, true, new CommonRequestUtil.OnCommonListener() {
//                        @Override
//                        public void onSuccess() {
//                            mIsFollow = 1;
//                            mBtnFollow.setSelected(true);
//                            mBtnFollow.setText(getString(R.string.cancel_follow));
//                            EventBus.getDefault().post(new UpdateCareEvent());
//
//                        }
//
//                        @Override
//                        public void onFail() {
//
//                        }
//                    });
//                }
                break;

            case R.id.userDetail_sayHello:// 打招呼
                if(mIsSayHello==1){
                    finish();
                }else{
                    CommonRequestUtil.sayHello(this, uId, CommonRequestUtil.SAY_HELLO_TYPE_USER_INFO, true, new CommonRequestUtil.OnSayHelloListener() {

                        @Override
                        public void onSuccess(SayHello sayHello) {
                            if(sayHello.getIsSucceed().equals("1")){
                                ToastUtil.showShortToast(UserInfoDetailActivity.this, getString(R.string.sayHello_success));
                                // 发送事件，更新状态
                                EventBus.getDefault().post(new SayHelloEvent());
                                EventBus.getDefault().post(new UpdateNextUser(true));
                            }
                            mIsSayHello = 1;
                            mBtnSayHello.setSelected(true);
                            finish();
                        }

                        @Override
                        public void onFail() {
                            finish();

                        }
                    });
                }

                break;

            case R.id.userDetail_chat:// 聊天
                ChatActivity.toChatActivity(this, uId, nickname, avatarUrl);
                break;
        }
    }

    /**
     * 设置用户信息
     */
    private void setUserInfo(User user) {
        if (user != null) {
            UserBase userBase = user.getUserBaseEnglish();
            if (userBase != null) {
                // 更新ID
                uId = userBase.getId();
                // 设置用户名为标题
                nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    mCollapsingToolbarLayout.setTitle(nickname);
                }
                // 设置头像
                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getImageUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                .url(imgUrl).imageView(mIvUserAvatar).build());
                    } else {
                        mIvUserAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
                    }
                } else {
                    mIvUserAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
                }
                // 设置年龄，身高，婚姻状况为展示基本信息
                StringBuilder stringBuilder = new StringBuilder();
                int age = userBase.getAge();
                if (age < 18) {
                    age = 18;
                }
                stringBuilder.append(age);
//                        .append(getString(R.string.years));
                String heightCm = userBase.getHeightCm();
                if (!TextUtils.isEmpty(heightCm)) {
                    stringBuilder.append(" | ")
                            .append(HeightUtils.getInchCmByCm(heightCm));
                }
                if(ParamsUtils.getMarriageMap()!=null){
                    String marriage = ParamsUtils.getMarriageMap().get(String.valueOf(userBase.getMaritalStatus()));
                    if (!TextUtils.isEmpty(marriage)) {
                        if (!TextUtils.isEmpty(heightCm)) {
                            stringBuilder.append(" | ");
                        }
                        stringBuilder.append(marriage);
                    }
                }

                mTvBaseInfo.setText(stringBuilder.toString());

                // 国家
                 country = userBase.getCountry();
                if ("America".equals(country)) {
                    country = "United States";
                }
                if (country.contains(" ")) {
                    country = country.replaceAll(" ", "");
                }
                for(int i = 0; i< Cheeses.EN_GOUNTRY.length; i++){
                    if(country.equals(Cheeses.EN_GOUNTRY[i])){
                        country=Cheeses.EN_GOUNTRY[i];
                    }
                }
                setTextInfo(mLlCountry, mTvCountry, mCountryDivider, country);
                // 内心独白
                String monologue = userBase.getMonologue();
                if (!TextUtils.isEmpty(monologue)) {
                    if (CommonData.getCountryList().contains(country) || CommonData.getSpanishCountryList().contains(country)) {
                        mMonologue.setText(monologue);
                        mLlMonologue.setVisibility(View.VISIBLE);
                    }else{
                        mLlMonologue.setVisibility(View.GONE);
                    }
                } else {
                    mLlMonologue.setVisibility(View.GONE);
                }
                // 地区
                setTextInfo(mLlArea, mTvArea, mAreaDivider, userBase.getArea().getProvinceName());
                // 星座
                setTextInfo(mLlSign, mTvSign, mSignDivider, ParamsUtils.getConstellationMap().get(String.valueOf(userBase.getSign())));
                // 学历
//                setTextInfo(mLlEducation, mTvEducation, mEducationDivider, ParamsUtils.getEducationMap().get(String.valueOf(userBase.getEducation())));
                // 收入
                setTextInfo(mLlIncome, mTvIncome, mIncomeDivider, ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome())));
                // 职业
                setTextInfo(mLlOccupation, mTvOccupation, mOccupationDivider, ParamsUtils.getWorkMap().get(String.valueOf(userBase.getOccupation())));
            }
            // 设置照片墙
            setUserPicWall(user.getListImage());
            // 设置用户详细信息
            setUserDetailInfo(user);
//            是否拉黑
             isBlack = user.getIsBlackList();
            // 设置关注和打招呼
            mIsFollow = user.getIsFollow();
//            设置是否打过招呼
            mIsSayHello=user.getIsSayHello();
            if(mIsSayHello==1 || mIsSayHello==-1){//已打过招呼
                mFlBootm.setVisibility(View.GONE);
                mBtnFollow.setVisibility(View.GONE);
                mBtnSayHello.setVisibility(View.GONE);
            }else {
                mFlBootm.setVisibility(View.VISIBLE);
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnSayHello.setVisibility(View.VISIBLE);
            }
//            if (mIsFollow == 1) {// 已关注
//                mBtnFollow.setSelected(true);
//                mBtnFollow.setText(getString(R.string.cancel_follow));
//            } else {
//                mBtnFollow.setSelected(false);
//                mBtnFollow.setText(getString(R.string.follow));
//            }
//            mIsSayHello = user.getIsSayHello();
//            if (mIsSayHello == 1) {// 已关注
//                mBtnSayHello.setEnabled(false);
//                mBtnSayHello.setSelected(true);
//                Utils.setDrawableLeft(mBtnSayHello, R.drawable.icon_say_hello_gray);
//            } else {
//                mBtnSayHello.setEnabled(true);
//                mBtnSayHello.setSelected(false);
//                Utils.setDrawableLeft(mBtnSayHello, R.drawable.icon_say_hello);
//            }
        }
    }

    /**
     * 设置用户照片墙
     */
    private void setUserPicWall(List<Image> imageList) {
        if (!Util.isListEmpty(imageList) && mImageWallAdapter != null) {
            mImageWallAdapter.replaceAll(imageList);
        }
    }

    /**
     * 设置用户详细资料
     */
    private void setUserDetailInfo(User user) {
        // 是否想要小孩
        setTextInfo(mLlWantBaby, mTvWantBaby, mWantBabyDivider, ParamsUtils.getWantBabyMap().get(String.valueOf(user.getWantsKids())));
        // 喜欢的运动
        setTextInfo(mLlSport, mTvSport, mSportDivider, getValue(user.getSports(), ParamsUtils.getSportMap()));
        // 喜欢的宠物
        if(!CommonData.getCountryList().contains(country)){
            mLlPets.setVisibility(View.GONE);
        }else{
            setTextInfo(mLlPets, mTvPets, mPetsDivider, getValue(user.getPets(), ParamsUtils.getPetsMap()));
        }
        // 种族
        setTextInfo(mLlEthnicity, mTvEthnicity, mEthnicityDivider, ParamsUtils.getEthnicityMap().get(user.getEthnicity()));
        // 锻炼习惯
        setTextInfo(mLlExerciseHabits, mTvExerciseHabits, mExerciseHabitsDivider, ParamsUtils.getExerciseHabitsMap().get(String.valueOf(user.getExerciseHabits())));
        // 兴趣爱好
        setTextInfo(mLlHobby, mTvHobby, mHobbyDivider, getValue(user.getListHobby(), ParamsUtils.getInterestMap()));
    }

    private void setTextInfo(LinearLayout llContainer, TextView tvInfo, View dividerView, String text) {
        if (!TextUtils.isEmpty(text) && !"null".equals(text) && !"".equals(text)) {
            tvInfo.setText(text);
            llContainer.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            llContainer.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }
    }

    private String getValue(String key, Map<String, String> map) {
        if (key != null) {
            String[] spit = key.split("\\|");
            StringBuffer value = new StringBuffer();
            for (int i = 0; i < spit.length; i++) {
                if (i < spit.length - 1) {
                    value.append(map.get(spit[i]) + ",");
                } else {
                    value.append(map.get(spit[i]));
                }
            }
            return value.toString();
        }
        return null;
    }

    private class ImageWallAdapter extends CommonRecyclerViewAdapter<Image> {

        public ImageWallAdapter(Context context, int layoutResId) {
            this(context, layoutResId, null);
        }

        public ImageWallAdapter(Context context, int layoutResId, List<Image> list) {
            super(context, layoutResId, list);
        }

        @Override
        protected void convert(final int position, RecyclerViewHolder viewHolder, Image bean) {
            final ImageView ivPic = (ImageView) viewHolder.getView(R.id.item_user_pic);
            ImageView ivLock = (ImageView) viewHolder.getView(R.id.iv_small_lock);
            final boolean interceptLock = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
            if (interceptLock) {
                if (position == 0) {
                    ivLock.setVisibility(View.GONE);
                } else {
                    ivLock.setVisibility(View.VISIBLE);
                }
            } else {
                ivLock.setVisibility(View.GONE);
            }

            if (bean != null) {
                final String imgUrl = bean.getImageUrl();
                if (!TextUtils.isEmpty(imgUrl)) {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                            .url(imgUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).imageView(ivPic).build(), new ImageLoaderUtil.OnImageLoadListener() {
                        @Override
                        public void onCompleted() {
                            if (position != 0 && interceptLock) {
                                returnBitmap(imgUrl, ivPic);
                            }
                        }

                        @Override
                        public void onFailed() {

                        }
                    });

                } else {
                    ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
//                    if(position!=0 && interceptLock){
//                        returnBitmap(imgUrl, ivPic);
//                    }
                }
            } else {
                ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
            }
        }
    }

    private void loadUserDetailInfo() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_USER_INFO)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", TextUtils.isEmpty(uId) ? "" : uId)
                .addParams("isRecord", TextUtils.isEmpty(isRecord) ? "0" : isRecord)
                .addParams("sourceTag", TextUtils.isEmpty(sourceTag) ? "1" : sourceTag)
                .build()
                .execute(new Callback<UserDetail>() {
                    @Override
                    public UserDetail parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, UserDetail.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(UserDetail response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                newUser = response.getUserEnglish();
                                setUserInfo(response.getUserEnglish());
                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void loadNextUser() {
        showLoad();
        pageNum++;
        OkHttpUtils.post()
                .url(IUrlConstant.URL_NEXT_USER)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .build()
                .execute(new StringCallback() {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (!TextUtils.isEmpty(response)) {
                            JSONObject jsonObject = (JSONObject) JSON.parse(response);
                            String listUser = jsonObject.getString("listUser");
                            if (!TextUtils.isEmpty(listUser)) {
                                List<User> userList = JSON.parseArray(listUser, User.class);
                                if (!Util.isListEmpty(userList)) {
                                    setUserInfo(userList.get(0));
                                } else {
                                    loadNextUser();
                                }
                            } else {
                                loadNextUser();
                            }
                        } else {
                            loadNextUser();
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int seconds) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissLoad();
            }
        }, seconds * 1000);
    }
    //自定义弹出对话框
    private void actionSheetDialogNoTitle() {
        final ActionSheetDialog dialog;
        String praised = mIsFollow==1 ? getString(R.string.cancel_follow) : getString(R.string.follow);
        if (isBlack == 1) {
            final String[] stringItems2 = {praised, getString(R.string.profile_quxiaolahei), getString(R.string.profile_jubo)};
            dialog = new ActionSheetDialog(UserInfoDetailActivity.this, stringItems2, null);
        } else {
            final String[] stringItems1 = {praised, getString(R.string.profile_lahei), getString(R.string.profile_jubo)};
            dialog = new ActionSheetDialog(UserInfoDetailActivity.this, stringItems1, null);
        }
        dialog.isTitleShow(false).show();
        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        setFollow();
                        break;
                    case 1:
                        setBlack();
                        break;
                    case 2:
                        ReportDialog.newInstance(getString(R.string.report_title) + nickname, uId).show(getSupportFragmentManager(), "report");
                        break;
                }
                dialog.dismiss();
            }
        });
    }
    //是否关注
    private void setFollow() {
        if (mIsFollow == 1) {// 取消关注
            CommonRequestUtil.cancelFollow(uId, true, new CommonRequestUtil.OnCommonListener() {
                @Override
                public void onSuccess() {
                    mIsFollow = 0;
//                            更新关注列表
                    EventBus.getDefault().post(new UpdateCareEvent());
                }

                @Override
                public void onFail() {

                }
            });
        } else {// 关注
            CommonRequestUtil.follow(uId, true, new CommonRequestUtil.OnCommonListener() {
                @Override
                public void onSuccess() {
                    mIsFollow = 1;
                    EventBus.getDefault().post(new UpdateCareEvent());
                }

                @Override
                public void onFail() {

                }
            });
        }
    }
    /**
     * 是否拉黑
     **/
    public void setBlack(){
        if(isBlack==0){
            CommonRequestUtil.pull2Black(uId, true, new CommonRequestUtil.OnCommonListener() {
                @Override
                public void onSuccess() {
                    isBlack = 1;
                }

                @Override
                public void onFail() {

                }
            });
        }else{
            CommonRequestUtil.cancelPull2Black(uId, true, new CommonRequestUtil.OnCommonListener() {
                @Override
                public void onSuccess() {
                    isBlack = 0;
                }

                @Override
                public void onFail() {

                }
            });
        }

    }
    /**
     * 毛玻璃效果
     **/
    public void dimBitmao(ImageView imageView, Bitmap originBitmap) {

        int scaleRatio = 7;
        int blurRadius = 8;
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(originBitmap,
                originBitmap.getWidth() / scaleRatio,
                originBitmap.getHeight() / scaleRatio,
                false);
        Bitmap blurBitmap = FastBlurUtil.doBlur1(scaledBitmap, blurRadius, true);
//        Bitmap bitmap = CircleTransformation.transform1(blurBitmap);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
        imageView.setImageBitmap(blurBitmap);
//        Log.v("=============imageView=",imageView.toString());
//        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
//                .url(imgUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE)
//                .transform(new CircleTransformation()).imageView(ivPic).build());
    }

    /**
     * 根据图片的url路径获得Bitmap对象
     *
     * @param url
     * @return
     */
    public void returnBitmap(final String url, final ImageView photoView) {
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                URL fileUrl = null;
                Bitmap bitmap = null;
                try {
                    fileUrl = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                try {
                    HttpURLConnection conn = (HttpURLConnection) fileUrl
                            .openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    bitmap = BitmapFactory.decodeStream(is);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bitmap;

            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                Bitmap bitmap = (Bitmap) o;

                if (bitmap != null) {
                    getBitmapList.add(bitmap);
                    dimBitmao(photoView, bitmap);
//                    Log.v("===========bitmap=",bitmap.toString());
                } else {
//                    Log.d("============","bitmap===null");
                }


            }
        }.execute();
    }

    public void interceptDialog() {
        final String[] stringItems = {getString(R.string.buy_vip), getString(R.string.buy_vip_service)};
        final ActionSheetDialog dialog = new ActionSheetDialog(UserInfoDetailActivity.this, stringItems, mIvUserAvatar);
        if(!dialog.isShowing()){
            dialog.isTitleShow(false).show();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnOperItemClickL(new OnOperItemClickL() {
                @Override
                public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 1 || position == 0) {
//                    展示十个用户，之后跳转到付费界面
                        BuyServiceActivity.toBuyServiceActivity(UserInfoDetailActivity.this, BuyServiceActivity.INTENT_FROM_INTERCEPT,
                                IConfigConstant.PAY_TAG_FROM_CHAT, IConfigConstant.PAY_WAY_ALL);
                    }
                    dialog.dismiss();
                }
            });
        }
    }


    private void getSelfIntroduce() {
//        int answer_1 = SharedPreferenceUtil.getIntValue(UserInfoDetailActivity.this, "QUESTION", "answer_1", 0);
//        int answer_2 = SharedPreferenceUtil.getIntValue(UserInfoDetailActivity.this, "QUESTION", "answer_2", 0);
//        int answer_3 = SharedPreferenceUtil.getIntValue(UserInfoDetailActivity.this, "QUESTION", "answer_3", 0);
//        int answer_4 = SharedPreferenceUtil.getIntValue(UserInfoDetailActivity.this, "QUESTION", "answer_4", 0);
//        int answer_5 = SharedPreferenceUtil.getIntValue(UserInfoDetailActivity.this, "QUESTION", "answer_5", 0);
        Random rand = new Random();
        int answer_1 = rand.nextInt(3);
        int answer_2 = rand.nextInt(3);
        int answer_3 = rand.nextInt(3);
        int answer_4 = rand.nextInt(3);
        int answer_5 = rand.nextInt(4);
        String answer1;
        String answer2;
        String answer3;
        String answer4;
        String answer5;
        String answer6;
        if (answer_1 == 0) {
            answer1 = getString(R.string.result1_a);
        } else if (answer_1 == 1) {
            answer1 = getString(R.string.result1_b);
        } else {
            answer1 = getString(R.string.result1_c);
        }
        if (answer_2 == 0) {
            answer2 = getString(R.string.result2_a);
        } else if (answer_2 == 1) {
            answer2 = getString(R.string.result2_b);
        } else {
            answer2 = "";
        }
        if (answer_3 == 0) {
            answer3 = getString(R.string.result3_a);
        } else if (answer_3 == 1) {
            answer3 = getString(R.string.result3_b);
        } else {
            answer3 = "";
        }
        if (answer_4 == 0) {
            answer4 = getString(R.string.result4_a);
        } else if (answer_4 == 1) {
            answer4 = getString(R.string.result4_b);
        } else {
            answer4 = getString(R.string.result4_c);
        }
        if (answer_5 == 0) {
            answer5 = getString(R.string.result5_a);
        } else if (answer_5 == 1) {
            answer5 = getString(R.string.result5_b);
        } else if (answer_5 == 2) {
            answer5 = getString(R.string.result5_c);
        } else {
            answer5 = getString(R.string.result5_d);
        }
        answer6 = getString(R.string.result);
//        if(answer_1 == 1 && answer_2 == 1 && answer_3 == 1 && answer_4 == 1 && answer_5 == 1){
//            rlIntroduce.setVisibility(View.GONE);
//        }else{
            rlIntroduce.setVisibility(View.GONE);
//            tvIntroduce.setText(answer1 + answer2 + answer3 + answer4 + answer5 + answer6);
//        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        isShake = true;
    }

    //
    @Override
    protected void onPause() {
        super.onPause();
//        isShake=false;

    }

    @Override
    protected void onStop() {
        super.onStop();
        isShake = false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isShake = true;
        mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_LOAD_DATA, 1000 * 3);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 删除广播注册
        if (mReceiverTag) {   //判断广播是否注册
            mReceiverTag = false;   //Tag值 赋值为false 表示该广播已被注销
            this.unregisterReceiver(mHomeKeyEventReceiver);   //注销广播
        }
        mTimerHandler.removeMessages(MSG_TYPE_HEAD_MSG);
        mTimerHandler.removeMessages(MSG_TYPE_HEAD_MSG_NEW);
        mTimerHandler.removeMessages(MSG_TYPE_RECOMMEND_USER);
        mTimerHandler.removeMessages(MSG_TYPE_HIDE_RECOMMEND_USER);
        mTimerHandler.removeMessages(MSG_TYPE_SHOW_UNREAD_MSG);
        mTimerHandler.removeMessages(MSG_TYPE_DISMISS_UNREAD_MSG);
        mTimerHandler.removeMessages(MSG_TYPE_DISMISS_VISITOR);
        EventBus.getDefault().unregister(this);

    }

    /**
     * 监听是否点击了home键将客户端推到后台
     */
    private BroadcastReceiver mHomeKeyEventReceiver = new BroadcastReceiver() {
        String SYSTEM_REASON = "reason";
        String SYSTEM_HOME_KEY = "homekey";
        String SYSTEM_HOME_KEY_LONG = "recentapps";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_REASON);
                if (TextUtils.equals(reason, SYSTEM_HOME_KEY)) {
                    //表示按了home键,程序到了后台
                    isShake = false;
                    mTimerHandler.removeMessages(MSG_TYPE_HEAD_MSG);
                    mTimerHandler.removeMessages(MSG_TYPE_HEAD_MSG_NEW);
                    mTimerHandler.removeMessages(MSG_TYPE_RECOMMEND_USER);
                    mTimerHandler.removeMessages(MSG_TYPE_HIDE_RECOMMEND_USER);
                    mTimerHandler.removeMessages(MSG_TYPE_SHOW_UNREAD_MSG);
                    mTimerHandler.removeMessages(MSG_TYPE_DISMISS_UNREAD_MSG);
                    mTimerHandler.removeMessages(MSG_TYPE_DISMISS_VISITOR);
                } else if (TextUtils.equals(reason, SYSTEM_HOME_KEY_LONG)) {
                    //表示长按home键,显示最近使用的程序列表
                }
            }
        }
    };

    //
//    @Subscribe
//    public void onEvent(YeMeiIntoUserInfoDetailEvent event) {
//        mCurrDisplayItem=event.getUnreadMsgCount();
//
//    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Message msg) {
//    if (msg.what == 3333) {
//        mCurrDisplayItem= (int) msg.obj;
//    }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SayHelloEvent event) {
        Log.v("setIsSayHello","UserInfo===");

        mIsSayHello = 1;
        mBtnSayHello.setSelected(true);
    }

}
