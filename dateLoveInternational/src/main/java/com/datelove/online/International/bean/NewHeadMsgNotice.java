package com.datelove.online.International.bean;

import java.util.List;

/**
 * 新页眉对象
 * Created by zhangdroid on 2017/4/19.
 */
public class NewHeadMsgNotice {
    private String isSucceed;
    private int cycle;// 轮询周期
    private int displaySecond;// 显示时长（秒）
    private long lastMsgTime;// 最后一条未读消息时间毫秒数
    // 通知类型：0 没有页眉通知
    //    1 未读信通知（跳转到信箱列表页）
    //    2 谁正在看我通知（推荐，同城上线，新注册，打招呼，符合你的条件）（跳转到对方空间页）
    //    3 支付失败、照片未审核通过通知（跳转到支付失败、照片未审核通过信箱详情页）
    //    4 诚信认证-身份证（军官证）审核失败通知（跳转到身份证（军官证）认证页面）
    //    5 照片通过审核，查看相册（跳转到“我”页面）；
    //    6 广告通知（跳转到advertiesUrl指定html5页面）
    private String noticeType;
    private List<MsgBox> unreadMsgBoxList;// 未读消息列表
    private FateUser remoteYuanfenUserBase;// 访客对象

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public int getCycle() {
        return cycle;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }

    public int getDisplaySecond() {
        return displaySecond;
    }

    public void setDisplaySecond(int displaySecond) {
        this.displaySecond = displaySecond;
    }

    public long getLastMsgTime() {
        return lastMsgTime;
    }

    public void setLastMsgTime(long lastMsgTime) {
        this.lastMsgTime = lastMsgTime;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public List<MsgBox> getUnreadMsgBoxList() {
        return unreadMsgBoxList;
    }

    public void setUnreadMsgBoxList(List<MsgBox> unreadMsgBoxList) {
        this.unreadMsgBoxList = unreadMsgBoxList;
    }

    public FateUser getRemoteYuanfenUserBase() {
        return remoteYuanfenUserBase;
    }

    public void setRemoteYuanfenUserBase(FateUser remoteYuanfenUserBase) {
        this.remoteYuanfenUserBase = remoteYuanfenUserBase;
    }

    @Override
    public String toString() {
        return "NewHeadMsgNotice{" +
                "isSucceed='" + isSucceed + '\'' +
                ", cycle=" + cycle +
                ", displaySecond=" + displaySecond +
                ", lastMsgTime=" + lastMsgTime +
                ", noticeType='" + noticeType + '\'' +
                ", unreadMsgBoxList=" + unreadMsgBoxList +
                ", remoteYuanfenUserBase=" + remoteYuanfenUserBase +
                '}';
    }

}
