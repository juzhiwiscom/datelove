package com.datelove.online.International.bean;

/**
 * Created by jzrh on 2016/7/2.
 */
public class UpImage {
    public Image image;
    public String isSucceed;
    public String msg;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "UpImage{" +
                "image=" + image +
                ", isSucceed='" + isSucceed + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
