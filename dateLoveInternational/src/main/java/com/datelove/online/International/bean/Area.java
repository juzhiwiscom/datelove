package com.datelove.online.International.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 区域对象
 * Created by zhangdroid on 2016/6/23.
 */
public class Area implements Parcelable{
    String country;
    int provinceId;// 省份id
    String provinceName;// 省份名称
    int cityId;// 城市id
    String cityName;// 城市名称
    int areaId;// 区id
    String areaName;// 区名称
    int areaIndex;// 区域排序序号，用于IOS定位
    int cityIndex;// 城市排序序号，用于IOS定位

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getAreaIndex() {
        return areaIndex;
    }

    public void setAreaIndex(int areaIndex) {
        this.areaIndex = areaIndex;
    }

    public int getCityIndex() {
        return cityIndex;
    }

    public void setCityIndex(int cityIndex) {
        this.cityIndex = cityIndex;
    }

    @Override
    public String toString() {
        return "Area{" +
                "country='" + country + '\'' +
                ", provinceId=" + provinceId +
                ", provinceName='" + provinceName + '\'' +
                ", cityId=" + cityId +
                ", cityName='" + cityName + '\'' +
                ", areaId=" + areaId +
                ", areaName='" + areaName + '\'' +
                ", areaIndex=" + areaIndex +
                ", cityIndex=" + cityIndex +
                '}';
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.country);
        dest.writeInt(this.provinceId);
        dest.writeString(this.provinceName);
        dest.writeInt(this.cityId);
        dest.writeString(this.cityName);
        dest.writeInt(this.areaId);
        dest.writeString(this.areaName);
        dest.writeInt(this.areaIndex);
        dest.writeInt(this.cityIndex);
    }

    public Area() {
    }

    protected Area(Parcel in) {
        this.country = in.readString();
        this.provinceId = in.readInt();
        this.provinceName = in.readString();
        this.cityId = in.readInt();
        this.cityName = in.readString();
        this.areaId = in.readInt();
        this.areaName = in.readString();
        this.areaIndex = in.readInt();
        this.cityIndex = in.readInt();
    }

    public static final Parcelable.Creator<Area> CREATOR = new Parcelable.Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel source) {
            return new Area(source);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };
}
