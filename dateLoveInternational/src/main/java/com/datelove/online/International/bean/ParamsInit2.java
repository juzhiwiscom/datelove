package com.datelove.online.International.bean;

import java.util.Map;

/**
 * Created by Administrator on 2017/9/12.
 */

public class ParamsInit2 {
    /**
     * goldHostFacebook : facebook@jzhrh.com
     * relationshipList : {"1":"未婚","2":"已婚","3":"离异"}
     * educationList : {"初中及以下":1,"高中及高职":2,"专科":3,"大学":4,"硕士及以上":5}
     * incomeList : {"1":"20000以下","2":"20000-40000","3":"40000-60000","4":"60000-100000","5":"100000以上"}
     * isSucceed : 1
     * occupationList : {"1":"在校学生","2":"法律","3":"IT/互联网/通讯","4":"媒体/公关","5":"金融","6":"政府机关","7":"咨询"}
     */

    private String goldHostFacebook;
    private Map<String, String> relationshipList;
    private Map<String, String> educationList;
    private Map<String, String> incomeList;
    private String isSucceed;
    private Map<String, String> occupationList;

    public String getGoldHostFacebook() {
        return goldHostFacebook;
    }

    public void setGoldHostFacebook(String goldHostFacebook) {
        this.goldHostFacebook = goldHostFacebook;
    }

    public Map<String, String> getRelationshipList() {
        return relationshipList;
    }

    public void setRelationshipList(Map<String, String> relationshipList) {
        this.relationshipList = relationshipList;
    }

    public Map<String, String> getEducationList() {
        return educationList;
    }

    public void setEducationList(Map<String, String> educationList) {
        this.educationList = educationList;
    }

    public Map<String, String> getIncomeList() {
        return incomeList;
    }

    public void setIncomeList(Map<String, String> incomeList) {
        this.incomeList = incomeList;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public Map<String, String> getOccupationList() {
        return occupationList;
    }

    public void setOccupationList(Map<String, String> occupationList) {
        this.occupationList = occupationList;
    }

    @Override
    public String toString() {
        return "ParamsInit{" +
                "goldHostFacebook=" + goldHostFacebook +
                ", relationshipList=" + relationshipList +
                ", educationList=" + educationList +
                ", incomeList=" + incomeList +
                ", isSucceed=" + isSucceed +
                ", occupationList=" + occupationList +
                '\'' +
                '}';
    }

}
