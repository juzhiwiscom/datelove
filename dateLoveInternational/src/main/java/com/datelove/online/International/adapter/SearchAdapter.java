package com.datelove.online.International.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.activity.MatchActivity;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.SayHello;
import com.datelove.online.International.bean.SearchUser;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.event.MessageChangedEvent;
import com.datelove.online.International.event.ShowInterceptEvent;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.ParamsUtils;
import com.datelove.online.International.utils.Utils;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DensityUtil;
import com.library.utils.HeightUtils;
import com.library.utils.Util;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * 搜索适配器
 * Created by zhangdroid on 2016/10/29.
 */
public class SearchAdapter extends CommonRecyclerViewAdapter<SearchUser> {

    private boolean isFirst=true;
    private int i;
    private boolean isIntercept;
    private Activity mContext;

    public SearchAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public SearchAdapter(Context context, int layoutResId, List list) {
        super(context, layoutResId, list);
    }
    public SearchAdapter(Activity context, int layoutResId, boolean isIntercept) {
        super(context, layoutResId, null);
        this.isIntercept=isIntercept;
        this.mContext=context;
    }

    @Override
    protected void convert(final int position, RecyclerViewHolder viewHolder, final SearchUser bean) {
        if (bean != null) {
            if(isIntercept){
                i++;
                if( i>12 && position==9){

                    isFirst=true;
                    EventBus.getDefault().post(new ShowInterceptEvent(true));
                }else if(isFirst){
//                防止频繁发送消息
                    isFirst=false;
                    EventBus.getDefault().post(new ShowInterceptEvent(false));
                }
            }

            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_search_avatar);
                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_search_nickname, nickname);
                }
                // 年龄、身高
                int age = userBase.getAge();
                if (age < 18) {
                    age = 18;
                }
                StringBuilder age_height = new StringBuilder();
                age_height.append(age)
//                        .append(mContext.getString(R.string.years))
                        .append("  ");
                String heightCm = userBase.getHeightCm();
                if (!TextUtils.isEmpty(heightCm)) {
                    age_height.append(HeightUtils.getInchCmByCm(heightCm));
                }
                viewHolder.setText(R.id.item_search_age_height, age_height.toString());
                // 地区，收入
                StringBuilder area_income = new StringBuilder();
                String area = userBase.getArea().getProvinceName();
                if (!TextUtils.isEmpty(area)) {
                    area_income.append(area);
                }
                String income = ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome()));
                if (!TextUtils.isEmpty(income)) {
                    if (!TextUtils.isEmpty(area)) {
                        area_income.append("  ");
                    }
                    area_income.append(income);
                }
                viewHolder.setText(R.id.item_search_location_income, area_income.toString());
                // 标签
                LinearLayout llLabelContainer = (LinearLayout) viewHolder.getView(R.id.item_search_label);
                llLabelContainer.removeAllViews();
                List<String> labelList = bean.getListLabel();
                if (!Util.isListEmpty(labelList)) {
                    llLabelContainer.setVisibility(View.VISIBLE);
                    llLabelContainer.addView(createLabelTextView(ParamsUtils.getInterestMap().get(labelList.get(0))));
                } else {
                    llLabelContainer.setVisibility(View.GONE);
                }

                // 打招呼
                final TextView tvSayHello = (TextView) viewHolder.getView(R.id.item_search_say_hello);
                String isSayHello = bean.getIsSayHello();
                if (!TextUtils.isEmpty(isSayHello) && "1".equals(isSayHello)) {
                    tvSayHello.setSelected(true);
                    tvSayHello.setText(mContext.getString(R.string.like));
                    Utils.setDrawableLeft(tvSayHello, R.mipmap.like_small);
                } else if(!TextUtils.isEmpty(isSayHello) && "-1".equals(isSayHello)){
                    tvSayHello.setSelected(true);
                    tvSayHello.setText(mContext.getString(R.string.nope));
                    Utils.setDrawableLeft(tvSayHello, R.mipmap.error_small);
                }else{
                    tvSayHello.setSelected(false);
                    tvSayHello.setText(mContext.getString(R.string.like));
                    Utils.setDrawableLeft(tvSayHello, R.mipmap.like_small);

                }
                // 打招呼
                tvSayHello.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonRequestUtil.sayHello(mContext, userBase.getId(), CommonRequestUtil.SAY_HELLO_TYPE_SEARCH,
                                true, new CommonRequestUtil.OnSayHelloListener() {

                                    @Override
                                    public void onSuccess(SayHello sayHello) {
                                        tvSayHello.setSelected(false);
                                        bean.setIsSayHello("1");
                                        updateItem(position, bean);
//                                        EventBus.getDefault().post(new SayHelloEvent());
                                        if (sayHello != null && sayHello.getMatchUser()!=null) {
                                            // 发送事件，更新聊天列表
                                            EventBus.getDefault().post(new MessageChangedEvent());
                                            if(mContext!=null){
                                                MatchActivity.toMatchActivity(mContext, sayHello.getMatchUser());
                                            }

                                        }
                                    }

                                    @Override
                                    public void onFail() {

                                    }
                                });
                    }
                });
            }
        }
    }

    private TextView createLabelTextView(String label) {
        TextView textView = new TextView(mContext);
        int padding15 = DensityUtil.dip2px(mContext, 15);
        int padding5 = DensityUtil.dip2px(mContext, 5);
        textView.setPadding(padding15, padding5, padding15, padding5);
        textView.setText(label);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        textView.setBackgroundResource(R.drawable.shape_round_rectangle_light_yellow_bg);
        textView.setGravity(Gravity.CENTER);
        textView.setSingleLine();
        return textView;
    }

}
