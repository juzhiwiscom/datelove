package com.datelove.online.International.bean;

/**
 * Created by jzrh on 2016/7/5.
 */
public class SeeMe {
    private String time; //访问时间
    private String isSayHello;//是否打过招呼，1->已打招呼, 0->未打招呼
    private UserBase userBaseEnglish;//用户对象基础信息
    private String language;//语言
    private String country;//国家

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(String isSayHello) {
        this.isSayHello = isSayHello;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "SeeMe{" +
                "time='" + time + '\'' +
                ", isSayHello='" + isSayHello + '\'' +
                ", userBaseEnglish=" + userBaseEnglish +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

}
