package com.datelove.online.International.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.PeiDuiBean;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.TimeUtil;

/**
 * 搜索适配器
 * Created by zhangdroid on 2016/10/29.
 */
public class MutualLikeAdapter extends CommonRecyclerViewAdapter<PeiDuiBean.MatcherList> {

    public MutualLikeAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    protected void convert(final int position, RecyclerViewHolder viewHolder, final PeiDuiBean.MatcherList bean) {
        if (bean != null) {
            String time = bean.getMatchTime();
            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_search_avatar);
                TextView tvTime = (TextView) viewHolder.getView(R.id.item_visitors_time);
                tvTime.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(time)){
                    viewHolder.setText(R.id.item_visitors_time, TimeUtil.showCreatedTime(mContext, time, null));
                }

                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_search_nickname, nickname);
                }
                if(bean.getIsRead()==1){
                    viewHolder.setVisibility(R.id.watch_item_msgimage,false);
                }else{
                    viewHolder.setVisibility(R.id.watch_item_msgimage,true);
                }

                // 打招呼
                final TextView tvSayHello = (TextView) viewHolder.getView(R.id.item_search_say_hello);
                tvSayHello.setVisibility(View.GONE);
            }
        }
    }

}
