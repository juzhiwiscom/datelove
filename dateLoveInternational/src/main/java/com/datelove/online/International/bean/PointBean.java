package com.datelove.online.International.bean;

/**
 * Created by Administrator on 2017/7/11.
 */

public class PointBean {
    /**
     * 1代表请求成功
     */
    public String isSucceed;
    /**
     * 提示信息
     */
    public String remoteId;
    public String actionId;
    public String extendId;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }
    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }
    public String getExtendId() {
        return extendId;
    }

    public void setExtendId(String extendId) {
        this.extendId = extendId;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "isSucceed='" + isSucceed + '\'' +
                ", remoteId='" + remoteId + '\'' +
                ", actionId='" + actionId + '\'' +
                ", extendId='" + extendId + '\'' +
                '}';
    }

}
