package com.datelove.online.International.receiver;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baidu.android.pushservice.PushMessageReceiver;
import com.datelove.online.International.activity.ChatActivity;
import com.datelove.online.International.activity.MainActivity;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.MessageChangedEvent;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/*
 * Push消息处理receiver。请编写您需要的回调函数， 一般来说： onBind是必须的，用来处理startWork返回值；
 *onMessage用来接收透传消息； onSetTags、onDelTags、onListTags是tag相关操作的回调；
 *onNotificationClicked在通知被点击时回调； onUnbind是stopWork接口的返回值回调
 * 返回值中的errorCode，解释如下：
 *0 - Success
 *10001 - Network Problem
 *10101  Integrate Check Error
 *30600 - Internal Server Error
 *30601 - Method Not Allowed
 *30602 - Request Params Not Valid
 *30603 - Authentication Failed
 *30604 - Quota Use Up Payment Required
 *30605 -Data Required Not Found
 *30606 - Request Time Expires Timeout
 *30607 - Channel Token Timeout
 *30608 - Bind Relation Not Found
 *30609 - Bind Number Too Many
 * 当您遇到以上返回错误时，如果解释不了您的问题，请用同一请求的返回值requestId和errorCode联系我们追查问题。
 *
 */
public class MyPushMessageReceiver extends PushMessageReceiver {
    /**
     * TAG to Log
     */
    public static final String TAG = "TAG";

    /**
     * 调用PushManager.startWork后，sdk将对push
     * server发起绑定请求，这个过程是异步的。绑定请求的结果通过onBind返回。 如果您需要用单播推送，需要把这里获取的channel
     * id和user id上传到应用server中，再调用server接口用channel id和user id给单个手机或者用户推送。
     *
     * @param context   BroadcastReceiver的执行Context
     * @param errorCode 绑定接口返回值，0 - 成功
     * @param appid     应用id。errorCode非0时为null
     * @param userId    应用user id。errorCode非0时为null
     * @param channelId 应用channel id。errorCode非0时为null
     * @param requestId 向服务端发起的请求id。在追查问题时有用；
     * @return none
     */
    @Override
    public void onBind(Context context, int errorCode, String appid,
                       String userId, String channelId, String requestId) {
        String responseString = "onBind errorCode=" + errorCode + " appid="
                + appid + " userId=" + userId + " channelId=" + channelId
                + " requestId=" + requestId;
        Log.e(TAG, responseString);
        if (errorCode == 0) {// 绑定成功
            PlatformInfoXml.setPushToken(channelId + "+" + userId);
        }
    }

    /**
     * 接收透传消息的函数。
     *
     * @param context             上下文
     * @param message             推送的消息
     * @param customContentString 自定义内容,为空或者json字符串
     */
    @Override
    public void onMessage(Context context, String message,
                          String customContentString) {
        String messageString = "透传消息 onMessage=\"" + message
                + "\" customContentString=" + customContentString;
        Log.e(TAG, messageString);
//        if (!TextUtils.isEmpty(customContentString)) {
//            JSONObject jsonObject = JSON.parseObject(customContentString);
//            // 1为相互喜欢，2为发来文字消息，3为发来语音消息，4为召回信，6为招呼信（有人喜欢你），98为管理员信
//            String type = jsonObject.getString("type");
//            if (!TextUtils.isEmpty(type)) {
//                if ("1".equals(type) || "2".equals(type) || "3".equals(type) || "6".equals(type)) {
//                    // 更新聊天列表
//                    EventBus.getDefault().post(new MessageChangedEvent());
//                }
//            }
//        }
    }

    /**
     * 接收通知到达的函数。
     *
     * @param context             上下文
     * @param title               推送的通知的标题
     * @param description         推送的通知的描述
     * @param customContentString 自定义内容，为空或者json字符串
     */

    @Override
    public void onNotificationArrived(Context context, String title,
                                      String description, String customContentString) {
        String notifyString = "通知到达 onNotificationArrived  title=\"" + title
                + "\" description=\"" + description + "\" customContent=" + customContentString;
        Log.e(TAG, notifyString);
        if (!TextUtils.isEmpty(customContentString)) {
            JSONObject jsonObject = JSON.parseObject(customContentString);
            // 1为相互喜欢，2为发来文字消息，3为发来语音消息，4为召回信，6为招呼信（有人喜欢你），98为管理员信
            String type = jsonObject.getString("type");
            if (!TextUtils.isEmpty(type)) {
                if ("1".equals(type) || "2".equals(type) || "3".equals(type) || "6".equals(type)) {
                    // 更新聊天列表
                    EventBus.getDefault().post(new MessageChangedEvent());
                }
            }
        }
    }

    /**
     * 接收通知点击的函数。
     *
     * @param context             上下文
     * @param title               推送的通知的标题
     * @param description         推送的通知的描述
     * @param customContentString 自定义内容，为空或者json字符串
     */
    @Override
    public void onNotificationClicked(Context context, String title, String description, String customContentString) {
        String notifyString = "通知点击 onNotificationClicked title=\"" + title + "\" description=\"" + description + "\" customContent=" + customContentString;
        Log.e(TAG, notifyString);
        if (!TextUtils.isEmpty(customContentString)) {
            JSONObject jsonObject = JSON.parseObject(customContentString);
            // 1为相互喜欢，2为发来文字消息，3为发来语音消息，4为召回信，6为招呼信（有人喜欢你），98为管理员信
            String type = jsonObject.getString("type");
            if (!TextUtils.isEmpty(type)) {
                if ("4".equals(type)) {// 召回信
                    // 召回统计
                    login();
                    toMainActivity(context);
                } else if ("6".equals(type)) {// 招呼信
                    toMainActivity(context);
                } else if ("1".equals(type) || "2".equals(type) || "3".equals(type)) {// 聊天消息
                    User user = jsonObject.getObject("user", User.class);
                    if (user != null) {
                        UserBase userBase = user.getUserBaseEnglish();
                        if (userBase != null) {
                            ChatActivity.toChatActivity(context, userBase.getId(), userBase.getNickName(), userBase.getImage().getThumbnailUrl());
                        }
                    }
                    try {
                        Thread.sleep(1000);
                        // 延时刷新聊天列表
                        EventBus.getDefault().post(new MessageChangedEvent());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                toMainActivity(context);
            }
        } else {
            toMainActivity(context);
        }
    }

    private void toMainActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 統計召回使用
     */
    public void login() {
        if (UserInfoXml.getUsername() != null && UserInfoXml.getPassword() != null) {
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_LOGIN)
                    .addParams("account", UserInfoXml.getUsername())
                    .addParams("password", UserInfoXml.getPassword())
                    .addParams("loginType", "1")
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .build()
                    .execute(null);
        }
    }

    /**
     * setTags() 的回调函数。
     *
     * @param context     上下文
     * @param errorCode   错误码。0表示某些tag已经设置成功；非0表示所有tag的设置均失败。
     * @param successTags 设置成功的tag
     * @param failTags    设置失败的tag
     * @param requestId   分配给对云推送的请求的id
     */
    @Override
    public void onSetTags(Context context, int errorCode,
                          List<String> successTags, List<String> failTags, String requestId) {
        String responseString = "onSetTags errorCode=" + errorCode
                + " successTags=" + successTags + " failTags=" + failTags
                + " requestId=" + requestId;
        Log.e(TAG, responseString);
    }

    /**
     * delTags() 的回调函数。
     *
     * @param context     上下文
     * @param errorCode   错误码。0表示某些tag已经删除成功；非0表示所有tag均删除失败。
     * @param successTags 成功删除的tag
     * @param failTags    删除失败的tag
     * @param requestId   分配给对云推送的请求的id
     */
    @Override
    public void onDelTags(Context context, int errorCode,
                          List<String> successTags, List<String> failTags, String requestId) {
        String responseString = "onDelTags errorCode=" + errorCode
                + " successTags=" + successTags + " failTags=" + failTags
                + " requestId=" + requestId;
        Log.d(TAG, responseString);
    }

    /**
     * listTags() 的回调函数。
     *
     * @param context   上下文
     * @param errorCode 错误码。0表示列举tag成功；非0表示失败。
     * @param tags      当前应用设置的所有tag。
     * @param requestId 分配给对云推送的请求的id
     */
    @Override
    public void onListTags(Context context, int errorCode, List<String> tags,
                           String requestId) {
        String responseString = "onListTags errorCode=" + errorCode + " tags="
                + tags;
        Log.e(TAG, responseString);
    }

    /**
     * PushManager.stopWork() 的回调函数。
     *
     * @param context   上下文
     * @param errorCode 错误码。0表示从云推送解绑定成功；非0表示失败。
     * @param requestId 分配给对云推送的请求的id
     */
    @Override
    public void onUnbind(Context context, int errorCode, String requestId) {
        String responseString = "onUnbind errorCode=" + errorCode
                + " requestId = " + requestId;
        Log.e(TAG, responseString);

        if (errorCode == 0) {
            // 解绑定成功
            Log.d(TAG, "解绑成功");
        }
    }

}
