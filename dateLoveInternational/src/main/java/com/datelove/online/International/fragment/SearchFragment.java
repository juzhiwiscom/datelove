package com.datelove.online.International.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.activity.BuyServiceActivity;
import com.datelove.online.International.activity.MatchInfoActivity;
import com.datelove.online.International.activity.UserInfoDetailActivity;
import com.datelove.online.International.adapter.BannerAdapter;
import com.datelove.online.International.adapter.SearchAdapter;
import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.base.BaseTitleFragment;
import com.datelove.online.International.bean.MatcherInfo;
import com.datelove.online.International.bean.Search;
import com.datelove.online.International.bean.SearchUser;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.MatchInfoChangeEvent;
import com.datelove.online.International.event.SayHelloEvent;
import com.datelove.online.International.event.ShowInterceptEvent;
import com.datelove.online.International.event.UnlikeEvent;
import com.datelove.online.International.haoping.ActionSheetDialog;
import com.datelove.online.International.haoping.OnOperItemClickL;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.library.widgets.RefreshRecyclerView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 搜索
 */
public class SearchFragment extends BaseTitleFragment {
    @BindView(R.id.search_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    @BindView(R.id.search_fail)
    TextView mTvSearchFail;
    @BindView(R.id.iv_loading_heart)
    ImageView ivLoadingHeart;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    @BindView(R.id.tv_loading)
    TextView tvLoading;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    @BindView(R.id.tv_bannertext)
    TextView mTextView;
    @BindView(R.id.points)
    LinearLayout mLinearLayout;
    @BindView(R.id.rl_foot)
    RelativeLayout rlFoot;
    Unbinder unbinder;
    private int i = 0;

    // 声明控件
    private List<ImageView> mlist;

    // 广告图素材
    private int[] bannerImages = { R.drawable.blue, R.drawable.green, R.drawable.orange, R.drawable.purple };
    // 广告语
    private String[] bannerTexts = {BaseApplication.getGlobalContext().getString(R.string.singles), BaseApplication.getGlobalContext().getString(R.string.Filter),
            BaseApplication.getGlobalContext().getString(R.string.complete), BaseApplication.getGlobalContext().getString(R.string.visited) };

    // ViewPager适配器与监听器
    private BannerAdapter mAdapter;
    private BannerListener bannerListener;

    // 圆圈标志位
    private int pointIndex = 0;
    // 线程标志
    private boolean isStop = false;
    private boolean isFirst=true;

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("wwwwwwwwSearchFragment");
    }

    private SearchAdapter mSearchAdapter;
    private int pageNum = 1;
    private final String pageSize = "10";
    private String country=PlatformInfoXml.getPlatformInfo().getCountry().trim();
    /**
     * 搜索随机因子，避免搜索重复
     */
    private String randomNum = "0";
    /**
     * 记录当前查看的用户处于列表中的索引
     */
    private int mCurrentPosition = -1;


    @Override
    protected int getLayoutResId() {
        LayoutInflater from = LayoutInflater.from(getActivity());
        View view = from.inflate(R.layout.fragment_search, null);
//         mRefreshRecyclerView = (RefreshRecyclerView) view.findViewById(R.id.search_recyclerview);
        return R.layout.fragment_search;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_search));
        mIvRight.setImageResource(R.drawable.selector_search);

        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.lucency_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.alpha(0));
        mRefreshRecyclerView.setRefreshing(false);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(getActivity()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        /**加载读信拦截对话框**/
         boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
        mSearchAdapter = new SearchAdapter(getActivity(), R.layout.item_fragment_search,isIntercept);
        mRefreshRecyclerView.setAdapter(mSearchAdapter);


    }

    @Override
    protected void addListeners() {

        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(getActivity(), MatchInfoActivity.class, false);
            }
        });
        mTvSearchFail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(getActivity(), MatchInfoActivity.class, false);
            }
        });
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                i++;
                if (i == 1) {
                    refresh();
                    flLoading.setVisibility(View.INVISIBLE);
                    Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                    ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                } else {
                    flLoading.setVisibility(View.VISIBLE);
                    tvLoading.setVisibility(View.VISIBLE);
                    Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                    ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                }
                /**加载读信拦截对话框**/
                if(!(country.equals(BaseApplication.getGlobalContext().getString(R.string.united_states).trim()) || country.equals("America"))){
                    boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                    if(isIntercept && UserInfoXml.isMale()){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                flLoading.setVisibility(View.GONE);
                                tvLoading.setVisibility(View.GONE);
                            }
                        }, 1000);
                    }else{

                        refresh();
                    }
                }else{
                    refresh();
                }



            }
        });
        /**加载读信拦截对话框**/
        boolean isIntercept;
        if(!(country.equals(getString(R.string.united_states).trim()) || country.equals("America"))){
            isIntercept=false;
        }else{
             isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
        }

        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                /**加载读信拦截对话框**/
                if(!(country.equals(getString(R.string.united_states).trim()) || country.equals("America"))){
                    boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                    if (isIntercept && UserInfoXml.isMale()) {
//                   弹出拦截对话框
//                    interceptDialog();
//                    弹出会员的viewPager介绍
                        flLoading.setVisibility(View.VISIBLE);
                        tvLoading.setVisibility(View.VISIBLE);
                        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                        ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                flLoading.setVisibility(View.GONE);
                                tvLoading.setVisibility(View.GONE);
                            }
                        }, 1000);
                        rlFoot.setVisibility(View.VISIBLE);
//                    只加载一次
                        if(isFirst){
                            isFirst=false;
                            getViewPager();
                        }

                    } else {
                        rlFoot.setVisibility(View.GONE);
                        pageNum++;
                        loadSearchData();
                        flLoading.setVisibility(View.VISIBLE);
                        tvLoading.setVisibility(View.VISIBLE);
                        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                        ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                    }
                }else{
                    rlFoot.setVisibility(View.GONE);
                    pageNum++;
                    loadSearchData();
                    flLoading.setVisibility(View.VISIBLE);
                    tvLoading.setVisibility(View.VISIBLE);
                    Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_loading);
                    ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
                }

            }
        }, getActivity(), isIntercept);
        mSearchAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                mCurrentPosition = position;
                SearchUser searchUser = mSearchAdapter.getItemByPosition(position);
                if (searchUser != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), searchUser.getUserBaseEnglish().getId(),
                            null, UserInfoDetailActivity.SOURCE_FROM_SEARCH);
                }
            }
        });

    }

    private void getViewPager() {
        initData();
        initAction();
        initAnim();

        // 开启新线程，2秒一次更新Banner
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (!isStop) {
                    SystemClock.sleep(2000);
                    if(getActivity()==null){
                        return;
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                        }
                    });
                }
            }
        }).start();
    }

    private void interceptDialog() {
        final String[] stringItems = {getString(R.string.buy_vip), getString(R.string.buy_vip_service)};
        final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, mTvSearchFail);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1 || position == 0) {
//                    展示十个用户，之后跳转到付费界面
                    BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_INTERCEPT,
                            IConfigConstant.PAY_TAG_FROM_CHAT, IConfigConstant.PAY_WAY_ALL);
                }
                dialog.dismiss();
            }
        });
    }

    private void refresh() {
        pageNum = 1;
        loadSearchData();
    }

    private void loadSearchData() {
        Map<String, MatcherInfo> map = new HashMap<>();
        MatcherInfo matcherInfo = new MatcherInfo();
        String matcherInfoStr = UserInfoXml.getMatchInfo();
        if (!TextUtils.isEmpty(matcherInfoStr)) {
            matcherInfo = JSON.parseObject(matcherInfoStr, MatcherInfo.class);
        }
        map.put("matcherInfo", matcherInfo);
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SEARCH)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .addParams("randomNum", randomNum)
                .addParams("matcherInfo", JSON.toJSONString(map))
                .build()
                .execute(new Callback<Search>() {
                    @Override
                    public Search parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, Search.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                flLoading.setVisibility(View.GONE);
                                tvLoading.setVisibility(View.GONE);
                            }
                        }, 1000);
                    }

                    @Override
                    public void onResponse(final Search response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (Util.isListEmpty(response.getListSearch())) {
                                    mTvSearchFail.setVisibility(View.VISIBLE);
                                }
                                randomNum = String.valueOf(response.getRandomNum());
                                // 延时1秒显示
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSearchAdapter != null) {
                                            if (pageNum == 1) {
                                                mSearchAdapter.replaceAll(response.getListSearch());
                                            } else {
                                                mSearchAdapter.appendToList(response.getListSearch());
                                            }
                                            mTvSearchFail.setVisibility(View.GONE);
                                        }
                                    }
                                }, 1000);
                            }
                        } else {
//                            mTvSearchFail.setVisibility(View.VISIBLE);
                        }
                        stopRefresh(1);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(flLoading!=null){
                                    flLoading.setVisibility(View.GONE);
                                    tvLoading.setVisibility(View.GONE);
                                }

                            }
                        }, 1000);


                    }
                });
    }

    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
        dismissLoading();
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(MatchInfoChangeEvent event) {
        showLoading();
        refresh();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SayHelloEvent event) {
        if (mCurrentPosition >= 0 && mSearchAdapter != null) {
            SearchUser searchUser = mSearchAdapter.getItemByPosition(mCurrentPosition);
            if (searchUser != null) {
                searchUser.setIsSayHello("1");
                mSearchAdapter.updateItem(mCurrentPosition, searchUser);
                Log.v("setIsSayHello","Search===");
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UnlikeEvent event) {
        if (mCurrentPosition >= 0 && mSearchAdapter != null) {
            SearchUser searchUser = mSearchAdapter.getItemByPosition(mCurrentPosition);
            if (searchUser != null) {
                searchUser.setIsSayHello("-1");
                mSearchAdapter.updateItem(mCurrentPosition, searchUser);
                Log.v("setIsSayHello","Search===");
            }
        }
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // TODO: inflate a fragment view
//        View rootView = super.onCreateView(inflater, container, savedInstanceState);
//        unbinder = ButterKnife.bind(this, rootView);
//        return rootView;
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isStop=true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }
    /**
     * 上拉加载的viewPager初始化数据
     */
    private void initData() {

        mlist = new ArrayList<ImageView>();
        View view;
        LinearLayout.LayoutParams params;
        for (int i = 0; i < bannerImages.length; i++) {
            // 设置广告图
            ImageView imageView = new ImageView(getActivity());
            imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            imageView.setBackgroundResource(bannerImages[i]);
            mlist.add(imageView);
            // 设置圆圈点
            view = new View(getActivity());
            params = new LinearLayout.LayoutParams(5, 5);
            params.leftMargin = 10;
            view.setBackgroundResource(R.drawable.point_background);
            view.setLayoutParams(params);
            view.setEnabled(false);

            mLinearLayout.addView(view);
        }
        mAdapter = new BannerAdapter(mlist);
        mViewPager.setAdapter(mAdapter);
        //由于ViewPager 没有点击事件，可以通过对VIewPager的setOnTouchListener进行监听已达到实现点击事件的效果
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            int flage = 0 ;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                         flage=0;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float x = event.getX();
                        if(x>800){
                            flage = 1 ;
                        }
                        break ;
                    case  MotionEvent.ACTION_UP :
                        if(flage==0){
//                    展示十个用户，之后跳转到付费界面
                            BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_INTERCEPT,
                                    IConfigConstant.PAY_TAG_FROM_CHAT, IConfigConstant.PAY_WAY_ALL);
                        }

                        break ;
                }
                return false;
            }
        });

    }
    /**
     * 上拉加载的viewPager初始化事件
     */
    private void initAction() {
        bannerListener = new BannerListener();
        mViewPager.setOnPageChangeListener(bannerListener);
        //取中间数来作为起始位置
        int index = (Integer.MAX_VALUE / 2) - (Integer.MAX_VALUE / 2 % mlist.size());
        //用来出发监听器
        mViewPager.setCurrentItem(index);
        mLinearLayout.getChildAt(pointIndex).setEnabled(true);
    }
    //实现VierPager监听器接口
    class BannerListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int position) {
            int newPosition = position % bannerImages.length;
            mTextView.setText(bannerTexts[newPosition]);
            mLinearLayout.getChildAt(newPosition).setEnabled(true);
            mLinearLayout.getChildAt(pointIndex).setEnabled(false);
            // 更新标志位
            pointIndex = newPosition;

        }

    }
    private void initAnim() {
        /** LayoutAnimation */
        TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                0f, Animation.RELATIVE_TO_SELF, 6f, Animation.RELATIVE_TO_SELF, 0);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.setDuration(800);
        animation.setStartOffset(150);

        LayoutAnimationController lac = new LayoutAnimationController(animation, 0.12f);
        lac.setInterpolator(new DecelerateInterpolator());
        rlFoot.setAnimation(animation);
    }
    @Subscribe
    public void onEvent(ShowInterceptEvent event) {
        if(event.getIsIntecept()){
            rlFoot.setVisibility(View.VISIBLE);
        }else{
            rlFoot.setVisibility(View.GONE);
        }

    }


}
