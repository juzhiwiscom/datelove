package com.datelove.online.International.event;

/**
 * Created by tianzhentao on 2018/2/1.
 */

public class UpdateNextUser {
    private boolean isLike;
    public UpdateNextUser(boolean isLike) {
        this.isLike=isLike;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }
}
