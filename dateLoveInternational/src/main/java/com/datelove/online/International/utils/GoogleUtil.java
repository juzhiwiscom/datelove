package com.datelove.online.International.utils;

import android.os.Handler;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

import static com.datelove.online.International.utils.FileUtil.getExternalFilesDir;

/**
 * 谷歌地图接口 返回详细地址信息
 * 
 * @author GaoDaWei
 * @version
 */
public class GoogleUtil {
	private static InputStream is;
    private static InputStreamReader inputStreamReader;

//	private static final Log logger = LogFactory.getLog(GoogleUtil.class);
//定义Handler对象

	/**
	 * 根据精度和纬度获取Google地图位置描述
	 * 
	 * @param lat
	 *            纬度，取值 -90 ~ +90 TODO ??有疑问
	 * @param lng
	 *            经度，取值 -180 ~+180
	 * @param country
	 * @return 获取Google地图位置描述
	 */
	public static Map<String, String> getGoogleAddress(String lat, String lng,
													   String language, String country) {
//		String url2 = "http://ditu.google.cn/maps/api/geocode/json?latlng=31.543114,-97.154185&&sensor=false&&language=en_US";
//		本地
		String url = "http://ditu.google.cn/maps/api/geocode/json?latlng="+ lat + "," + lng + "&&sensor=false&&language=";
//		线上
//		String url = "http://ditu.google.com/maps/api/geocode/json?latlng="+ lat + "," + lng + "&&sensor=false&&language=";
		
		//获取国际化参数
		String localInfo = "en_US";
		url += localInfo;	//拼接url地址
		
		String charset = "UTF-8";

		String result = getAddress(url, charset);// 获取json字符串
		if (null != result) {
			JSONObject json = JSON.parseObject(result);// 转换为json数据
			JSONArray jsonArray = json.getJSONArray("results");

			Map<String, String> map = null;
			if(null!=jsonArray&&jsonArray.size()>0){// 增加验证
				JSONObject objResult = (JSONObject) jsonArray.get(0);
				JSONArray addressComponents = objResult.getJSONArray("address_components");

				//解析结果
				map = getByGoogleMap(addressComponents, objResult, language, country);

				return map;
			}
		}else{
//			logger.info("google get state error");
		}

		return null;
	}


	/*
	 * 请求服务器
	 */
	private  static String getAddress(String urlAll, String charset) {
		final String[] result = {null};
		OkHttpUtils.get()
				.url(urlAll)
				.build()
				.execute(new StringCallback() {
					@Override
					public void onError(Call call, Exception e, int id) {
						Log.v("Exception",e.toString());

					}

					@Override
					public void onResponse(String response, int id) {
						result[0] =response.toString();

					}
				});
		return result[0];


//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//				.detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
//		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().
//				detectLeakedClosableObjects().penaltyLog().penaltyDeath().build());
//		BufferedReader reader = null;
//		String result = null;
//		StringBuffer sbf = new StringBuffer();
//		try {
//			URL url = new URL(urlAll);
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//			connection.setRequestMethod("GET");// 请求方式
//			connection.setReadTimeout(5000);// 设置读取超时
//			connection.setConnectTimeout(5000);// 设置连接超时
//			connection.connect();// 连接服务器发送消息
//			 is = connection.getInputStream();
//             inputStreamReader = new InputStreamReader(is, charset);
//            reader = new BufferedReader(inputStreamReader);
//			String strRead = null;
//			while ((strRead = reader.readLine()) != null) {
//				sbf.append(strRead);
//				sbf.append("\r\n");
//			}
//			result = sbf.toString();
//		}catch(java.net.SocketTimeoutException e){
//		}catch (Exception e) {
//			e.printStackTrace();
//		}finally{
//			if(reader!=null){
//				try {
//                    inputStreamReader.close();
//					reader.close();
//					is.close();
//
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return result[0];
	}

	/*
	 * 解析定位结果
	 */
	private static Map<String, String> getByGoogleMap(JSONArray addressComponents,
			JSONObject objResult, String language, String country) {
		
		String countryResult="";
		String province="";
		String city="";
		if(addressComponents!=null&&addressComponents.size()>0){
			for(int i=0;i<addressComponents.size();i++){
				JSONObject obj = (JSONObject) addressComponents
						.get(i);
				JSONArray types = obj.getJSONArray("types");
				String type = (String)types.get(0);

				//获取目标解析单位的key
				String proviceKey = getGoogleProviceL1Key(country);
				
				if(type.equals(proviceKey)){
					province = obj.getString("long_name");// 州
				}else if(type.equals("administrative_area_level_2")){
					city = obj.getString("long_name");// 城市
				}else if (type.equals("country")){ //国家
					countryResult = obj.getString("long_name");
				}
			}
			
//			if(StringUtils.isBlank(province)){ //不是一级城市 取二级城市
			if(TextUtils.isEmpty(province)){ //不是一级城市 取二级城市
				province = city;
			}
		}
		
		String formatted_address = objResult.getString("formatted_address");

		Map<String, String> map = new HashMap<String, String>();
		map.put("city", city);
		map.put("formatted_address", formatted_address);
		map.put("province", province);
		map.put("country", countryResult);
		return map;
	}

	public static String getGoogleProviceL1Key(String country) {
		String proviceL1Key = "administrative_area_level_1";
		if("UnitedKingdom".equals(country) || "United Kingdom".equals(country)){
			proviceL1Key = "administrative_area_level_2";
		}
		return proviceL1Key;
	}


	//测试
	public static void main(String[] args) throws IOException {
		 GoogleUtil google = new GoogleUtil();
		 Map<String, String> googleAddress = google.getGoogleAddress("27.8912554","76.2916943","English","America");
		 System.out.println(googleAddress);
		 String country = googleAddress.get("country");
		 System.out.println(country);
	}
}




