package com.datelove.online.International.bean;

import java.util.List;

/**
 * 聊天列表对象
 * Created by zhangdroid on 2016/8/23.
 */
public class ChatInfo extends BaseModel {
    private int pageSize;// 该页内容数
    private int pageNum;// 页码
    private long systemTime;// 系统时间
    private int totalUnread;// 所有未读消息数
    private List<Chat> listChat;// 聊天记录集合
    private String showWriteMsgIntercept;// 1:消息拦截 2:不拦截
    private int totalUnread24H;//非会员24小时之前的消息不显示

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public long getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(long systemTime) {
        this.systemTime = systemTime;
    }

    public int getTotalUnread() {
        return totalUnread;
    }

    public void setTotalUnread(int totalUnread) {
        this.totalUnread = totalUnread;
    }

    public List<Chat> getListChat() {
        return listChat;
    }

    public void setListChat(List<Chat> listChat) {
        this.listChat = listChat;
    }

    public String getShowWriteMsgIntercept() {
        return showWriteMsgIntercept;
    }

    public void setShowWriteMsgIntercept(String showWriteMsgIntercept) {
        this.showWriteMsgIntercept = showWriteMsgIntercept;
    }
    public int getTotalUnread24H() {
        return totalUnread24H;
    }

    public void setTotalUnread24H(int totalUnread24H) {
        this.totalUnread24H = totalUnread24H;
    }

    @Override
    public String toString() {
        return "ChatInfo{" +
                "pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", systemTime=" + systemTime +
                ", totalUnread=" + totalUnread +
                ", listChat=" + listChat +
                ", showWriteMsgIntercept='" + showWriteMsgIntercept + '\'' +
                '}';
    }

}
