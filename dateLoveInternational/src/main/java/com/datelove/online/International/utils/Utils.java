package com.datelove.online.International.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.library.utils.DensityUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by zhangdroid on 2016/8/9.
 */
public class Utils {

    private static AlertDialog dialog;
    private static RelativeLayout relativeLayout;
    private static ImageView imageView;

    /**
     * @return Application Context
     */
    public static Context getContext() {
        return BaseApplication.getGlobalContext();
    }

    public static boolean isHttpUrl(String url) {
        if (!TextUtils.isEmpty(url) && url.startsWith("http")) {
            return true;
        }
        return false;
    }

    /**
     * 显示公用加载对话框
     *
     * @param context      上下文对象
     * @param isCancelable 是否可以取消
     * @return dialog对象，用来隐藏
     */
    public static Dialog showLoadingDialog(Context context, boolean isCancelable) {

//        Dialog dialog= new Dialog(context);
//        LayoutInflater li = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
//        View v=li.inflate(R.layout.dialog_loading_layout, null);
//        dialog.setContentView(v);
//        ImageView ivHeart = (ImageView)v.findViewById(R.id.iv_loading_heart);
//        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(context, R.anim.anim_show_loading);
//        ivHeart.startAnimation(hyperspaceJumpAnimation);


         relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        relativeLayout.setGravity(Gravity.CENTER);
         imageView = new ImageView(context);
        int size = DensityUtil.getScreenWidth(context) / 5;
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        imageView.setImageResource(R.drawable.heart);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(context, R.anim.anim_show_loading);
        imageView.startAnimation(hyperspaceJumpAnimation);


//        AnimationDrawable animationDrawable = Util.getFrameAnim(getLoadingDrawableList(context), true, 50);
//        animationDrawable.start();
//        imageView.setImageDrawable(animationDrawable);
//        TextView tvloading = new TextView(context);
//        tvloading.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
//        tvloading.setText(R.string.loading);
//        tvloading.setTextColor(context.getResources().getColor(R.color.main_color));
//        tvloading.setMa(10,300,10,10);
        relativeLayout.addView(imageView);
        relativeLayout.setVisibility(View.GONE);
//        relativeLayout.addView(tvloading);
         dialog = new AlertDialog.Builder(context, android.R.style.Theme_Panel)
                .setView(relativeLayout)
                .setCancelable(isCancelable)
                .show();
        return dialog;
    }

    /**
     * 列表加载更多
     */
    public static View getLoadingView(Context context) {
//        Dialog dialog= new Dialog(context);
//        LayoutInflater li = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
//        View v=li.inflate(R.layout.dialog_loading_layout, null);
//        dialog.setContentView(v);
//        ImageView ivHeart = (ImageView)v.findViewById(R.id.iv_loading_heart);
//        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(context, R.anim.anim_show_loading);
//        ivHeart.startAnimation(hyperspaceJumpAnimation);
//        return v;
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        relativeLayout.setGravity(Gravity.CENTER);
        int padding = DensityUtil.dip2px(context, 15);
        relativeLayout.setPadding(0, padding, 0, padding);
        AnimationDrawable animationDrawable = Util.getFrameAnim(getLoadingDrawableList(context), true, 75);
        animationDrawable.start();
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imageView.setImageDrawable(animationDrawable);
        relativeLayout.addView(imageView);
        return relativeLayout;
    }

    public static List<Drawable> getLoadingDrawableList(Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(getDrawable(context, R.drawable.loading_01));
        drawableList.add(getDrawable(context, R.drawable.loading_02));
        drawableList.add(getDrawable(context, R.drawable.loading_03));
        drawableList.add(getDrawable(context, R.drawable.loading_04));
        drawableList.add(getDrawable(context, R.drawable.loading_05));
        drawableList.add(getDrawable(context, R.drawable.loading_06));
        drawableList.add(getDrawable(context, R.drawable.loading_07));
        drawableList.add(getDrawable(context, R.drawable.loading_08));
        drawableList.add(getDrawable(context, R.drawable.loading_09));
        drawableList.add(getDrawable(context, R.drawable.loading_10));
        drawableList.add(getDrawable(context, R.drawable.loading_11));
        drawableList.add(getDrawable(context, R.drawable.loading_12));
        return drawableList;
    }

    private static Drawable getDrawable(Context context, int resId) {
        return context.getResources().getDrawable(resId);
    }

    public static void setDrawableLeft(TextView textView, int leftDrawableResId) {
        Drawable leftDrawable = getDrawable(textView.getContext(), leftDrawableResId);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
        textView.setCompoundDrawables(leftDrawable, null, null, null);
    }

    /**
     * 获得AnimationDrawable，图片动画
     *
     * @param list         动画需要播放的图片集合
     * @param isRepeatable 是否可以重复
     * @param duration     帧间隔（毫秒）
     * @return
     */
    public static AnimationDrawable getFrameAnim(List<Drawable> list, boolean isRepeatable, int duration) {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(!isRepeatable);
        if (!Util.isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                animationDrawable.addFrame(list.get(i), duration);
            }
        }
        return animationDrawable;
    }

    public static void dismissLoadingDialog() {
//        if(dialog!=null && dialog.isShowing()){
            imageView.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.GONE);
//            dialog.dismiss();
//
//        }
    }
    public static void display(Context context, ImageView imageView, String url) {
        if(imageView == null) {
            throw new IllegalArgumentException("argument error");
        }
        Glide.with(BaseApplication.getGlobalContext()).load(url).placeholder(R.drawable.upload_photo)
                .error(R.drawable.upload_photo).crossFade().into(imageView);
    }

}
