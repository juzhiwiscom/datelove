package com.datelove.online.International.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bm.library.PhotoView;
import com.datelove.online.International.R;
import com.datelove.online.International.anim.DepthPageTransformer;
import com.datelove.online.International.base.BaseFragmentActivity;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.constant.IConfigConstant;
//import com.datelove.online.International.utils.BlurUtil;
import com.datelove.online.International.utils.FastBlurUtil;
import com.datelove.online.International.utils.Utils;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.wonderkiln.blurkit.BlurLayout;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 图片浏览器
 * Created by zhangdroid on 2016/11/5.
 */
public class PictureBrowseActivity extends BaseTitleActivity {

    private static int getDimNum=0;
    private static int getBitmapNum=0;
    private static int getUrl=0;
    @BindView(R.id.pic_viewpager)
    ViewPager mViewPager;

    private static final String INTENT_KEY_INDEX = "intent_key_INDEX";
    private static final String INTENT_KEY_IMG_URL_LIST = "intent_key_IMG_URL_LIST";
    private static final String INTENT_KEY_IMG_URL_MAP = "intent_key_IMG_URL_MAP";
    private static final String NO_DIM_BITMAP = "no_dim_bitmao";
    @BindView(R.id.iv_lock)
    ImageView ivLock;
    @BindView(R.id.blurLayout)
    BlurLayout blurLayout;

    private int totalCount;
    private String imageUrl;
    private List<Image> list;
    static List<PhotoView> photoViewList;
    private static int pohotoNum=0;
    PhotoView photoView;
    int num=0;
    int currIndex;
    /**
     * 进度加载对话框
     */
    private Dialog mLoadingDialog;
    private List<Bitmap> getBitmapMap;
    List<PhotoView> imageViewList=new ArrayList<>();
    //从个人资料过来的照片不需要毛玻璃
    private static boolean noDimBitmap=false;
//    private static BlurUtil blurUtil;

    /**
     * 跳转图片浏览器
     *
     * @param context    上下文对象
     * @param currIndex  当前显示的图片的索引，从0开始
     * @param imgUrlList 所有图片url集合
     */
    public static void toPictureBrowseActivity(Context context, int currIndex, List<Image> imgUrlList) {
        Intent intent = new Intent(context, PictureBrowseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_KEY_INDEX, currIndex);
        intent.putExtra(INTENT_KEY_IMG_URL_LIST, (Serializable) imgUrlList);
        context.startActivity(intent);
    }
    public static void toPictureBrowseActivity(Context context, int currIndex, List<Image> imgUrlList,boolean noDimBitmao) {
        Intent intent = new Intent(context, PictureBrowseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_KEY_INDEX, currIndex);
        intent.putExtra(INTENT_KEY_IMG_URL_LIST, (Serializable) imgUrlList);
        intent.putExtra(NO_DIM_BITMAP, noDimBitmao);
        context.startActivity(intent);

    }
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_picture_browse;
    }

    @Override
    protected String getCenterTitle() {
        return "0/0";
    }

    @Override
    protected void initViewsAndVariables() {
        getPhoto();
    }

    private void getPhoto() {
        Intent intent = getIntent();
        if (intent != null) {
             currIndex = intent.getIntExtra(INTENT_KEY_INDEX, 0);
            noDimBitmap = intent.getBooleanExtra(NO_DIM_BITMAP, false);
            list = (List<Image>) intent.getSerializableExtra(INTENT_KEY_IMG_URL_LIST);
            getBitmapMap = (List<Bitmap>) intent.getSerializableExtra(INTENT_KEY_IMG_URL_MAP);
            totalCount = list.size();
             photoViewList = new ArrayList<PhotoView>();
            if (!Util.isListEmpty(list)) {

//                int i=0;
//                for (Image image : list) {
                for(int n=0;n<list.size();n++){
//                    i++;
                    Image image = list.get(n);
                    if (image != null) {
                        imageUrl = image.getImageUrl();
                        String thumbnailUrl = image.getThumbnailUrl();
                        /**添加非会员的毛玻璃效果**/
                        final boolean interceptLock = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                        if(n>0 && interceptLock){
                            imageUrl=thumbnailUrl;
                        }




                        photoView = new PhotoView(PictureBrowseActivity.this);

                        photoView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        // 开启缩放功能
                        photoView.enable();
                        final int finalN = n;
                        if (Utils.isHttpUrl(imageUrl)) {

                            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                                    .url(imageUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR).imageView(photoView).build(), new ImageLoaderUtil.OnImageLoadListener() {
                                @Override
                                public void onCompleted() {
                                    if(!noDimBitmap){
                                        if (finalN > 0 && interceptLock) {

//                                            blurUtil.clickBlurImg(photoView);

                                            if(getBitmapMap!=null && getBitmapMap.size()+1==list.size()){
                                                Bitmap bitmap = getBitmapMap.get(finalN - 1);
                                                dimBitmao(photoView, bitmap);
                                            }else{
                                                returnBitmap(imageUrl, photoView);
                                            }

                                        }
                                    }

                                }

                                @Override
                                public void onFailed() {

                                }
                            });
                        } else {
                            ImageLoaderUtil.getInstance().loadLocalImage(PictureBrowseActivity.this, photoView, Uri.fromFile(new File(imageUrl)), new ImageLoaderUtil.OnImageLoadListener() {
                                @Override
                                public void onCompleted() {
                                    if(!noDimBitmap){
                                        /**添加非会员的毛玻璃效果**/
                                        boolean interceptLock = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                                        if (finalN > 1 && interceptLock) {
                                            returnBitmap(imageUrl, photoView);
//                                            blurUtil.clickBlurImg(photoView);
                                        }
                                    }

                                }

                                @Override
                                public void onFailed() {

                                }
                            });
                        }
                        photoView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
                        photoViewList.add(photoView);
//                            }
//                        },200);
                    }
                }
            }
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
                    ImagePagerAdapter imagePagerAdapter = new ImagePagerAdapter(photoViewList,list,totalCount);
                    mViewPager.setAdapter(imagePagerAdapter);
                    mViewPager.setPageTransformer(true, new DepthPageTransformer());
                    mViewPager.setOffscreenPageLimit(totalCount - 1);
                    mViewPager.setCurrentItem(currIndex);
                    setTitle((currIndex + 1) + "/" + totalCount);
//                }
//            },3000);

        }
    }

    @Override
    protected void addListeners() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTitle((position + 1) + "/" + totalCount);
                if(!noDimBitmap){
                    boolean interceptLock = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                    if(interceptLock){
                        if(position==0){
                            blurLayout.setVisibility(View.GONE);
//                        getPhoto();
                        }else{
                            blurLayout.setVisibility(View.VISIBLE);
                            blurLayout.setBlurRadius(10);
                        }
                    }else{
                        blurLayout.setVisibility(View.GONE);
                    }
                }else{
                    blurLayout.setVisibility(View.GONE);
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

//         blurUtil=new BlurUtil(PictureBrowseActivity.this);
    }

    @OnClick(R.id.iv_lock)
    public void onViewClicked() {
        BuyServiceActivity.toBuyServiceActivity(PictureBrowseActivity.this, BuyServiceActivity.INTENT_FROM_MONTH,
                IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_MONTH);
    }

    private static class ImagePagerAdapter extends PagerAdapter {
        private List<PhotoView> mPhotoViewList;
        private List<Image> urlList;
        private int totalCount;

        public ImagePagerAdapter(List<PhotoView> list, List<Image> url,int count) {
            this.mPhotoViewList = list;
            this.urlList=url;
            this.totalCount=count;
        }

        @Override
        public int getCount() {
            return mPhotoViewList == null ? 0 : mPhotoViewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final PhotoView photoView = mPhotoViewList.get(position);
//            Bitmap bitmap=bitmapList.get(position);
            if(!noDimBitmap){
                /**添加非会员的毛玻璃效果**/
                boolean interceptLock = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                if (position > 0 && interceptLock) {

//                    blurUtil.clickBlurImg(photoView);

//                   改
                    final String imageUrl = urlList.get(position).getImageUrl();
                    returnBitmap(imageUrl, photoView);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            returnBitmap(imageUrl, photoView);
                        }
                    },200);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            returnBitmap(imageUrl, photoView);
                        }
                    },1000);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            returnBitmap(imageUrl, photoView);
                        }
                    },1500);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            returnBitmap(imageUrl, photoView);
                        }

                    },2000);

                }
            }
            container.addView(photoView, photoView.getLayoutParams());
            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }
/**毛玻璃效果**/
    public static void dimBitmao(PhotoView imageView, Bitmap originBitmap) {
        int scaleRatio = 7;
        int blurRadius = 8;
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(originBitmap,
                originBitmap.getWidth() / scaleRatio,
                originBitmap.getHeight() / scaleRatio,
                false);
        Bitmap blurBitmap = FastBlurUtil.doBlur(scaledBitmap, blurRadius, true);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageBitmap(blurBitmap);
    }

    /**
     * 根据图片的url路径获得Bitmap对象
     * @param url
     * @return
     */
    public static void returnBitmap(final String url, final PhotoView photoView) {

        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                URL fileUrl = null;
                Bitmap bitmap = null;
                try {
                    fileUrl = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                try {
                    HttpURLConnection conn = (HttpURLConnection) fileUrl
                            .openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    bitmap = BitmapFactory.decodeStream(is);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bitmap;

            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                Bitmap bitmap = (Bitmap) o;
                if(bitmap!=null){
                    pohotoNum++;
                    dimBitmao(photoView, bitmap);


                }


            }
        }.execute();
    }
    /**
     *Bitmap加载监听器
     */
    public interface OnBitmapListener {
        /**
         * 加载完成
         */
        void onCompleted();

        /**
         * 加载失败
         */
        void onFailed();
    }

//    /**
//     * 显示加载对话框
//     */
//    protected void showLoading() {
//        mLoadingDialog = Utils.showLoadingDialog(this, true);
//    }
//
//    /**
//     * 隐藏加载对话框
//     */
//    protected void dismissLoading() {
//        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
//            mLoadingDialog.dismiss();
//        }
//    }
}
