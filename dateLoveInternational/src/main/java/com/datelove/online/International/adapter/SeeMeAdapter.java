package com.datelove.online.International.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.datelove.online.International.R;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.SeeMe;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;

import java.util.List;

public class SeeMeAdapter extends CommonRecyclerViewAdapter<SeeMe> {

    public SeeMeAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public SeeMeAdapter(Context context, int layoutResId, List<SeeMe> list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, SeeMe bean) {
        if (bean != null) {
            UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                Image image = userBase.getImage();
                if (image != null) {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(image.getThumbnailUrl()).imageView((ImageView) viewHolder.getView(R.id.item_seeme_iv))
                            .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR).build());
                }
            }
        }
    }

}
