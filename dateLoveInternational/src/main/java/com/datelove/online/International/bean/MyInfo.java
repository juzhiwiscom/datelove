package com.datelove.online.International.bean;

/**
 * Created by jzrh on 2016/7/2.
 */
public class MyInfo {
    public User userPandora;
    public String isSucceed;
    public String msg;

    public User getUserEnglish() {
        return userPandora;
    }

    public void setUserEnglish(User userPandora) {
        this.userPandora = userPandora;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MyInfo{" +
                "userEnglish=" + userPandora +
                ", isSucceed='" + isSucceed + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
