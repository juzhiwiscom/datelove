package com.datelove.online.International.behavior;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

import com.library.utils.LogUtil;

/**
 * common bottom bar behavior
 * Created by zhangdroid on 2016/9/2.
 */
public class BottomBarBehavior extends CoordinatorLayout.Behavior<View> {

    public BottomBarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        float translationY = Math.abs(dependency.getTop());// 获取更随布局的顶部位置
        LogUtil.v("translationY","translationY=="+translationY);
        child.setTranslationY(translationY/6);// 在Y轴上平移
        return true;
    }

}
