package com.datelove.online.International.utils;

import android.os.Environment;
import android.text.TextUtils;

import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.event.SetAvatarEvent;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.TimeUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;

/**
 * File I/O
 * Created by zhangdroid on 2016/7/21.
 */
public class FileUtil {

    /**
     * 录音文件存放的文件夹路径（根目录/DateLove/record）
     */
    public static final String RECORD_DIRECTORY_PATH = FileUtil.getExternalStorageDirectory() + File.separator + "record";

    /**
     * 图片文件存放的文件夹路径（根目录/DateLove/picture）
     */
    public static final String PICTURE_DIRECTORY_PATH = FileUtil.getExternalStorageDirectory() + File.separator + "picture";

    /**
     * 判断是否有SD卡
     *
     * @return
     */
    public static boolean isSDCardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 获得SD卡目录路径，应用内所有数据均存放在该目录下
     *
     * @return 根目录下Amor文件夹
     */
    public static String getExternalStorageDirectory() {
        if (isSDCardExist()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "DateLove";
        }
        return null;
    }

    /**
     * 获得应用文件存放路径（不需要SD卡读写权限）
     *
     * @param type {@link android.content.Context#getExternalFilesDir(String)}
     * @return
     * @see android.content.Context#getExternalFilesDir(String)
     */
    public static String getExternalFilesDir(String type) {
        return BaseApplication.getGlobalContext().getExternalFilesDir(type).getAbsolutePath();
    }

    /**
     * 根据当前时间点生成文件名（不重复）
     *
     * @return 生成文件名格式为：20160721-180810
     */
    public static String createFileNameByTime() {
        StringBuilder stringBuilder = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        stringBuilder.append(calendar.get(Calendar.YEAR))
                .append(TimeUtil.pad(calendar.get(Calendar.MONTH) + 1))
                .append(TimeUtil.pad(calendar.get(Calendar.DAY_OF_MONTH)))
                .append("-")
                .append(TimeUtil.pad(calendar.get(Calendar.HOUR_OF_DAY)))
                .append(TimeUtil.pad(calendar.get(Calendar.MINUTE)))
                .append(TimeUtil.pad(calendar.get(Calendar.SECOND)));
        return stringBuilder.toString();
    }

    /**
     * 根据日期生成文件名
     *
     * @return 生成文件名格式为：20160721-180810
     */
    public static String createFileNameByDate(String date) {
        if (TextUtils.isEmpty(date)) {
            return null;
        }
        Date d = null;
        try {
            d = TimeUtil.timeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(d.getYear() + 1900)
                .append(TimeUtil.pad(d.getMonth() + 1))
                .append(TimeUtil.pad(d.getDate()))
                .append("-")
                .append(TimeUtil.pad(d.getHours()))
                .append(TimeUtil.pad(d.getMinutes()))
                .append(TimeUtil.pad(d.getSeconds()));
        return stringBuilder.toString();
    }

    /**
     * 下载文件并保存到SD卡
     *
     * @param url          目标文件url
     * @param destFileDir  目标文件存放的文件夹
     * @param destFileName 目标文件保存的名称
     */
    public static void downloadFile(String url, final String destFileDir, final String destFileName) {
        // 若目标文件已存在则不下载
        File file = new File(new File(destFileDir), destFileName);
        if (file.exists()) {
            return;
        }
        OkHttpUtils.get().url(url).build().execute(new FileCallBack(destFileDir, destFileName) {

            @Override
            public void onError(Call call, Exception e, int id) {
                String s = e.toString();
            }

            @Override
            public void onResponse(File response, int id) {
                boolean exists = response.exists();
            }
        });
    }
    public static void downWallFile(String url, final String destFileDir, final String destFileName, final boolean fromUploadPhontoActivity) {
        // 若目标文件已存在则不下载
        File file = new File(new File(destFileDir), destFileName);
        if (file.exists()) {
            file.delete();
        }
        OkHttpUtils.get().url(url).build().execute(new FileCallBack(destFileDir, destFileName) {

            @Override
            public void onError(Call call, Exception e, int id) {
                String s = e.toString();
            }

            @Override
            public void onResponse(File response, int id) {
                boolean exists = response.exists();
                if(fromUploadPhontoActivity){
                    EventBus.getDefault().post(new SetAvatarEvent());
                }

            }
        });
    }
    /**
     * 删除文件
     *
     * @param path 需要删除的文件路径
     */
    public static void deleteFile(String path) {
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        }
    }

}
