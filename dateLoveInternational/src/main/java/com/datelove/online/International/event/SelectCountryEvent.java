package com.datelove.online.International.event;

/**
 * Created by Administrator on 2017/9/29.
 */

public class SelectCountryEvent {
    public String mCountry;
    public SelectCountryEvent() {
    }

    public SelectCountryEvent(String contry) {
        this.mCountry = contry;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String contry) {
        this.mCountry = contry;
    }

}
