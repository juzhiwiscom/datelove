package com.datelove.online.International.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.datelove.online.International.R;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.constant.IConfigConstant;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;

import java.util.List;

public class MyPhotoAdapter extends CommonRecyclerViewAdapter<Image> {

    public MyPhotoAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public MyPhotoAdapter(Context context, int layoutResId, List<Image> list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, Image bean) {
        if (bean != null) {
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(bean.getThumbnailUrlM()).imageView((ImageView) viewHolder.getView(R.id.item_photo_iv))
                    .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR).build());
        }
    }

}
