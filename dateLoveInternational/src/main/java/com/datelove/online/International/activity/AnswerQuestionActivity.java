package com.datelove.online.International.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.fragment.AnswerQuestionFragment;
import com.library.widgets.ScrollControlViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 注册QA信
 */
public class AnswerQuestionActivity extends BaseTitleActivity {
    @BindView(R.id.answer_question_viewpager)
    ScrollControlViewPager mAnswerQuestionViewPager;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_answer_question;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_question);
    }

    @Override
    protected void initViewsAndVariables() {
        mIvLeft.setVisibility(View.GONE);
        List<Fragment> questionList = new ArrayList<>();
        questionList.add(AnswerQuestionFragment.newInstance(1));
        questionList.add(AnswerQuestionFragment.newInstance(2));
        questionList.add(AnswerQuestionFragment.newInstance(3));
        questionList.add(AnswerQuestionFragment.newInstance(4));
        questionList.add(AnswerQuestionFragment.newInstance(5));
        mAnswerQuestionViewPager.setAdapter(new AnswerQuestionAdapter(getSupportFragmentManager(), questionList));
        mAnswerQuestionViewPager.setCanScroll(false);
        mAnswerQuestionViewPager.setOffscreenPageLimit(questionList.size() - 1);
        mAnswerQuestionViewPager.setCurrentItem(0);
    }

    @Override
    protected void addListeners() {

    }

    private static class AnswerQuestionAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragmentList;

        public AnswerQuestionAdapter(FragmentManager fm, List<Fragment> list) {
            super(fm);
            mFragmentList = list;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList == null ? null : mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList == null ? 0 : mFragmentList.size();
        }
    }

}
