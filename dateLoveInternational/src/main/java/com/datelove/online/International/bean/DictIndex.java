package com.datelove.online.International.bean;

/**
 * Created by jzrh on 2016/7/5.
 */
public class DictIndex {
    public String bodyType;
    public String constellationId;
    public String drink;
    public String education;
    public String ethnicity;
    public String exerciseHabits;
    public String eyes;
    public String faith;
    public String hair;
    public String haveKids;
    public String howManyKids;
    public String income;
    public String interest;
    public String languages;
    public String marriage;
    public String needBabe;
    public String pets;
    public String politicalViews;
    public String smoke;
    public String sport;
    public String work;

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getConstellationId() {
        return constellationId;
    }

    public void setConstellationId(String constellationId) {
        this.constellationId = constellationId;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getExerciseHabits() {
        return exerciseHabits;
    }

    public void setExerciseHabits(String exerciseHabits) {
        this.exerciseHabits = exerciseHabits;
    }

    public String getEyes() {
        return eyes;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public String getFaith() {
        return faith;
    }

    public void setFaith(String faith) {
        this.faith = faith;
    }

    public String getHaveKids() {
        return haveKids;
    }

    public void setHaveKids(String haveKids) {
        this.haveKids = haveKids;
    }

    public String getHowManyKids() {
        return howManyKids;
    }

    public void setHowManyKids(String howManyKids) {
        this.howManyKids = howManyKids;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getNeedBabe() {
        return needBabe;
    }

    public void setNeedBabe(String needBabe) {
        this.needBabe = needBabe;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getPoliticalViews() {
        return politicalViews;
    }

    public void setPoliticalViews(String politicalViews) {
        this.politicalViews = politicalViews;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return "DictIndex{" +
                "bodyType='" + bodyType + '\'' +
                ", constellationId='" + constellationId + '\'' +
                ", drink='" + drink + '\'' +
                ", education='" + education + '\'' +
                ", ethnicity='" + ethnicity + '\'' +
                ", exerciseHabits='" + exerciseHabits + '\'' +
                ", eyes='" + eyes + '\'' +
                ", faith='" + faith + '\'' +
                ", hair='" + hair + '\'' +
                ", haveKids='" + haveKids + '\'' +
                ", howManyKids='" + howManyKids + '\'' +
                ", income='" + income + '\'' +
                ", interest='" + interest + '\'' +
                ", languages='" + languages + '\'' +
                ", marriage='" + marriage + '\'' +
                ", needBabe='" + needBabe + '\'' +
                ", pets='" + pets + '\'' +
                ", politicalViews='" + politicalViews + '\'' +
                ", smoke='" + smoke + '\'' +
                ", sport='" + sport + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}
