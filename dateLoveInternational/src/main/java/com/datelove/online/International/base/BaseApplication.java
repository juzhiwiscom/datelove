package com.datelove.online.International.base;

import android.app.Application;
import android.content.Context;

import com.kochava.base.Tracker;
import com.library.utils.CrashHandler;
import com.wonderkiln.blurkit.BlurKit;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by zhangdroid on 2016/5/25.
 */
public class BaseApplication extends Application {
    private static Context sApplicationContext;

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化异常收集
        CrashHandler.getInstance().init(this);
        // 初始化OkHttpClient
        initOkhttpClient();

//        kochava初始化
        Tracker.configure(new Tracker.Configuration(getApplicationContext())
                .setAppGuid("koflirt-eemy3mu")
                .setLogLevel(Tracker.LOG_LEVEL_INFO)
        );

//        高斯模糊初始化
        BlurKit.init(this);
    }

    /**
     * 配置网络请求相关参数
     */
    private void initOkhttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
