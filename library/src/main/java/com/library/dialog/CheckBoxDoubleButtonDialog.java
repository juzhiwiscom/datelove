package com.library.dialog;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.library.R;
import com.library.app.BaseDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CheckBoxDoubleButtonDialog extends BaseDialogFragment {
    private OnCheckBoxDoubleDialogClickListener mOnDialogClickListener;
    private List<String> mDataList;
    private List<String> selectList = new ArrayList<String>();

    public static CheckBoxDoubleButtonDialog newInstance(String title, String positive, String negative, boolean isCancelable, List<String> list, OnCheckBoxDoubleDialogClickListener listener) {
        CheckBoxDoubleButtonDialog doubleButtonDialog = new CheckBoxDoubleButtonDialog();
        doubleButtonDialog.mOnDialogClickListener = listener;
        doubleButtonDialog.mDataList = list;
        doubleButtonDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return doubleButtonDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_checkbox_double_btn;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_two_btn_title);
        View dividerView = view.findViewById(R.id.dialog_two_btn_divider);// 标题栏下分割线
        ListView tvMessage = (ListView) view.findViewById(R.id.dialog_two_btn_content);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_two_btn_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_two_btn_cancel);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        tvMessage.setAdapter(new MessageAdapter());

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onPositiveClick(v, selectList);
                }
                dismiss();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onNegativeClick(v);
                }
                dismiss();
            }
        });

    }

    // 用来控制CheckBox的选中状况
    private HashMap<Integer, Boolean> isSelected;

    public class MessageAdapter extends BaseAdapter {

        class ViewHolder {
            TextView tvName;
            CheckBox cb;
        }

        public MessageAdapter() {
            isSelected = new HashMap<Integer, Boolean>();
            // 初始化数据
            initDate();
        }

        // 初始化isSelected的数据
        private void initDate() {
            for (int i = 0; i < mDataList.size(); i++) {
                getIsSelected().put(i, false);
            }
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public Object getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            final String bean = mDataList.get(position);
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_check_box, null);
                holder = new ViewHolder();
                holder.cb = (CheckBox) convertView.findViewById(R.id.item_checkBox);
                holder.tvName = (TextView) convertView
                        .findViewById(R.id.item_tv_content);
                convertView.setTag(holder);
            } else {
                // 取出holder
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tvName.setText(bean);
            // 监听checkBox并根据原来的状态来设置新的状态
            holder.cb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    if (isSelected.get(position)) {
                        isSelected.put(position, false);
                        setIsSelected(isSelected);
                        selectList.remove(mDataList.get(position));
                    } else {
                        isSelected.put(position, true);
                        setIsSelected(isSelected);
                        selectList.add(mDataList.get(position));
                    }

                }
            });

            // 根据isSelected来设置checkbox的选中状况
            holder.cb.setChecked(getIsSelected().get(position));
            return convertView;
        }

        public HashMap<Integer, Boolean> getIsSelected() {
            return isSelected;
        }

        public void setIsSelected(HashMap<Integer, Boolean> isSelect) {
            isSelected = isSelect;
        }
    }
}
