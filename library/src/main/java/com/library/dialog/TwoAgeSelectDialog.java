package com.library.dialog;


import com.library.constant.CommonData;
import com.library.widgets.WheelView;

/**
 * 双列年龄选择对话框
 * Created by zhangdroid on 2016/6/25.
 */
public class TwoAgeSelectDialog extends TwoWheelDialog {
    private OnTwoWheelDialogClickListener mOnDialogClickListener;
    public int min;
    private int max;

    public static TwoAgeSelectDialog newInstance(int min, int max, String title, String positive, String negative, boolean isCancelable, OnTwoWheelDialogClickListener listener) {
        TwoAgeSelectDialog twoAgeSelectDialog = new TwoAgeSelectDialog();
        twoAgeSelectDialog.mOnDialogClickListener = listener;
        twoAgeSelectDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        twoAgeSelectDialog.min=min;
        twoAgeSelectDialog.max=max;

        return twoAgeSelectDialog;
    }

    @Override
    protected void setWheelView1(WheelView wheelView) {
        wheelView.setData(CommonData.getAgeList());
        min=min-18;
//        wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
        if(min>0){
            wheelView.setDefaultIndex(min);// 设置默认年龄为24岁
        }else{
         wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
        }

    }

    @Override
    protected void setWheelView2(WheelView wheelView) {
        wheelView.setData(CommonData.getAgeList());
        max=max-18;
        if(max>0){
            wheelView.setDefaultIndex(max);// 设置默认年龄为24岁
        }else{
            wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
        }
    }

    @Override
    protected OnTwoWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }

}
