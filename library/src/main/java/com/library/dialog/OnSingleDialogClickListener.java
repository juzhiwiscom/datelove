package com.library.dialog;

import android.view.View;

/**
 * Single button dialog click listener
 * Created by zhangdroid on 2016/6/25.
 */
public interface OnSingleDialogClickListener {

    /**
     * Called when the dialog positive button has been clicked.
     *
     * @param view The View that was clicked.
     */
    void onPositiveClick(View view);

}
