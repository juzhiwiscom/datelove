package com.library.dialog;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.library.R;
import com.library.app.BaseDialogFragment;

import java.util.List;


public class ListDoubleButtonDialog extends BaseDialogFragment {
    private OnListDoubleDialogClickListener mOnDialogClickListener;
    private List<String> list;

    public static ListDoubleButtonDialog newInstance(String title, String positive, String negative, boolean isCancelable, List<String> dataList, OnListDoubleDialogClickListener listener) {
        ListDoubleButtonDialog doubleButtonDialog = new ListDoubleButtonDialog();
        doubleButtonDialog.mOnDialogClickListener = listener;
        doubleButtonDialog.list = dataList;
        doubleButtonDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return doubleButtonDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_list_double_btn;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_two_btn_title);
        View dividerView = view.findViewById(R.id.dialog_two_btn_divider);// 标题栏下分割线
        ListView listViewMessage = (ListView) view.findViewById(R.id.dialog_two_btn_content);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_two_btn_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_two_btn_cancel);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        listViewMessage.setAdapter(new MessageAdapter());

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onPositiveClick(v);
                }
                dismiss();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onNegativeClick(v);
                }
                dismiss();
            }
        });
        listViewMessage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onItemClick(view, list.get(i));
                }
                dismiss();
            }
        });
    }

    public class MessageAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = View.inflate(getActivity(), R.layout.item_text, null);
                viewHolder.item_text = (TextView) convertView.findViewById(R.id.item_text);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.item_text.setText(list.get(position));
            return convertView;
        }

        public class ViewHolder {
            public TextView item_text;
        }
    }

}
