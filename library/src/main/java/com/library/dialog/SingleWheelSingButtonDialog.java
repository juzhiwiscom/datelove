package com.library.dialog;


import com.library.widgets.WheelView;

import java.util.List;

/**
 * 年龄选择对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class SingleWheelSingButtonDialog extends OneWheelSingleButtonDialog {
    private OnOneWheelDialogClickListener mOnDialogClickListener;
    private static List<String> list;

    public static SingleWheelSingButtonDialog newInstance(List<String> stringList, String title, String positive, String negative, boolean isCancelable, OnOneWheelDialogClickListener listener) {
        SingleWheelSingButtonDialog singleWheelDialog = new SingleWheelSingButtonDialog();
        singleWheelDialog.mOnDialogClickListener = listener;
        list=stringList;
        singleWheelDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return singleWheelDialog;
    }

    @Override
    protected void setWheelView(WheelView wheelView) {
        wheelView.setData(list);
        wheelView.setDefaultIndex(1);// 设置默认年龄为24岁
    }

    @Override
    protected OnOneWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }

}
