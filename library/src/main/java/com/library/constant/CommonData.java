package com.library.constant;


import com.library.utils.TimeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>公用数据，用于滚轮选择器数据源，格式统一为List<String>.</p>
 * <p/>
 * <p>1、年龄选择列表</p>
 * <p>2、日期选择列表</p>
 * Created by zhangdroid on 2016/6/24.
 */
public class CommonData {

    /**
     * @return 获得年龄列表
     */
    public static List<String> getAgeList() {
        List<String> ageList = new ArrayList<String>();
        for (int i = 0; i < 45; i++) {
            ageList.add(String.valueOf(i + 18));
        }
        return ageList;
    }

    /**
     * @return 获得年份列表
     */
    public static List<String> getYearList() {
        List<String> yearList = new ArrayList<String>();
        for (int i = 1997; i >= 1950; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    /**
     * @return 获得月份列表
     */
    public static List<String> getMonthList() {
        List<String> monthList = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            monthList.add(TimeUtil.pad(i));
        }
        return monthList;
    }

    /**
     * 获得天数列表
     *
     * @param maxDays 该月的最大天数
     */
    public static List<String> getDayList(int maxDays) {
        List<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= maxDays; i++) {
            dayList.add(TimeUtil.pad(i));
        }
        return dayList;
    }

}
