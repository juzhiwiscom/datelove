package com.library.utils;
import java.io.File;
import java.io.FileInputStream;
import java.security.DigestInputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class MessageDigestUtil {
	
	/**
	 * 功能描述：MD5加密算法 对字节数组进行加密 返回加密后的字符串（16进制字符串）
	 *
	 * @param data
	 * @return
	 * 
	 * @author lxl
	 * @since 2016-10-28
	 * @update:[变更日期YYYY-MM-DD][更改人姓名][变更描述]
	 */
	public static String encryptMD5(byte[] data) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md5.update(data);
		byte[] resultBytes = md5.digest();
		
		String resultString = BytesToHex.fromBytesToHex(resultBytes);
		return resultString;
	}
	
	
	public static String getMD5OfFile(String path) throws Exception{
		FileInputStream fis = new FileInputStream(new File(path));
		DigestInputStream dis = new DigestInputStream(fis, MessageDigest.getInstance("MD5"));
		byte[] buffer = new byte[1024];
		int read = dis.read(buffer, 0, 1024);
		while (read != -1){
			read = dis.read(buffer, 0, 1024);
		}
		
		MessageDigest md = dis.getMessageDigest();
		byte[] resultBytes = md.digest();
		String resultString = BytesToHex.fromBytesToHex(resultBytes);
		fis.close();
		dis.close();
		return resultString;
	}
	
	public static String encryptSHA(byte[] data) throws NoSuchAlgorithmException{
		MessageDigest sha = MessageDigest.getInstance("SHA-512");
		sha.update(data);
		byte[] resultBytes = sha.digest();
		
		String resultString = BytesToHex.fromBytesToHex(resultBytes);
		return resultString;
	}
	
	public static byte[] initHmacKey() throws Exception{
		KeyGenerator keyGen = KeyGenerator.getInstance("HmacSHA512");
		SecretKey secretKey = keyGen.generateKey();
		return secretKey.getEncoded();
	}
	
	public static String encryptHmac(byte[] data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException{
		SecretKey secretKey = new SecretKeySpec(key, "HmacSHA512");
		Mac mac = Mac.getInstance("HmacSHA512");
		mac.init(secretKey);
		byte[] resultBytes = mac.doFinal(data);
		
		String resultString = BytesToHex.fromBytesToHex(resultBytes);
		return resultString;
	}

	//内部工具类
	static class BytesToHex {
		
		//字节数组转为16进制字符串
		public static String fromBytesToHex(byte[] resultBytes) {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < resultBytes.length; i++) {
				if (Integer.toHexString(0xFF & resultBytes[i]).length() == 1) {
					builder.append("0").append(
							Integer.toHexString(0xFF & resultBytes[i]));
				} else {
					builder.append(Integer.toHexString(0xFF & resultBytes[i]));
				}
			}
			return builder.toString();
		}
		
	}
	
}
