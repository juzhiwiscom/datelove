package com.yueai.fragment;

import android.graphics.Color;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.utils.Util;
import com.library.widgets.RefreshRecyclerView;
import com.yueai.R;
import com.yueai.activity.MatchInfoActivity;
import com.yueai.activity.UserInfoDetailActivity;
import com.yueai.adapter.SearchAdapter;
import com.yueai.base.BaseTitleFragment;
import com.yueai.bean.MatcherInfo;
import com.yueai.bean.Search;
import com.yueai.bean.SearchUser;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.MatchInfoChangeEvent;
import com.yueai.event.SayHelloEvent;
import com.yueai.utils.Utils;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 搜索
 */
public class SearchFragment extends BaseTitleFragment {
    @BindView(R.id.search_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    @BindView(R.id.search_fail)
    TextView mTvSearchFail;

    private SearchAdapter mSearchAdapter;
    private int pageNum = 1;
    private final String pageSize = "30";
    /**
     * 搜索随机因子，避免搜索重复
     */
    private String randomNum = "0";
    /**
     * 记录当前查看的用户处于列表中的索引
     */
    private int mCurrentPosition = -1;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_search));
        mIvRight.setImageResource(R.drawable.selector_search);

        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(getActivity()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        mSearchAdapter = new SearchAdapter(getActivity(), R.layout.item_fragment_search);
        mRefreshRecyclerView.setAdapter(mSearchAdapter);
    }

    @Override
    protected void addListeners() {
        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(getActivity(), MatchInfoActivity.class, false);
            }
        });
        mTvSearchFail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(getActivity(), MatchInfoActivity.class, false);
            }
        });
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                pageNum++;
                loadSearchData();
            }
        });
        mSearchAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                mCurrentPosition = position;
                SearchUser searchUser = mSearchAdapter.getItemByPosition(position);
                if (searchUser != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), searchUser.getUserBaseEnglish().getId(),
                            null, UserInfoDetailActivity.SOURCE_FROM_SEARCH);
                }
            }
        });
    }

    private void refresh() {
        pageNum = 1;
        loadSearchData();
    }

    private void loadSearchData() {
        Map<String, MatcherInfo> map = new HashMap<>();
        MatcherInfo matcherInfo = new MatcherInfo();
        String matcherInfoStr = UserInfoXml.getMatchInfo();
        if (!TextUtils.isEmpty(matcherInfoStr)) {
            matcherInfo = JSON.parseObject(matcherInfoStr, MatcherInfo.class);
        }
        map.put("matcherInfo", matcherInfo);
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SEARCH)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .addParams("randomNum", randomNum)
                .addParams("matcherInfo", JSON.toJSONString(map))
                .build()
                .execute(new Callback<Search>() {
                    @Override
                    public Search parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, Search.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(final Search response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (Util.isListEmpty(response.getListSearch())) {
                                    mTvSearchFail.setVisibility(View.VISIBLE);
                                }
                                randomNum = String.valueOf(response.getRandomNum());
                                // 延时1秒显示
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSearchAdapter != null) {
                                            if (pageNum == 1) {
                                                mSearchAdapter.replaceAll(response.getListSearch());
                                            } else {
                                                mSearchAdapter.appendToList(response.getListSearch());
                                            }
                                        }
                                    }
                                }, 1000);
                            }
                        } else {
                            mTvSearchFail.setVisibility(View.VISIBLE);
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
        dismissLoading();
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(MatchInfoChangeEvent event) {
        showLoading();
        refresh();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SayHelloEvent event) {
        if (mCurrentPosition >= 0 && mSearchAdapter != null) {
            SearchUser searchUser = mSearchAdapter.getItemByPosition(mCurrentPosition);
            if (searchUser != null) {
                searchUser.setIsSayHello("1");
                mSearchAdapter.updateItem(mCurrentPosition, searchUser);
            }
        }
    }

}
