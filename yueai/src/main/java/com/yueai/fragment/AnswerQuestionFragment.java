package com.yueai.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.utils.Util;
import com.library.widgets.ScrollControlViewPager;
import com.yueai.R;
import com.yueai.activity.UploadPhotoActivity;
import com.yueai.base.BaseFragment;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.ParamsUtils;
import com.yueai.xml.UserInfoXml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

/**
 * QA页
 * Created by zhangdroid on 2016/12/29.
 */
public class AnswerQuestionFragment extends BaseFragment {
    @BindView(R.id.iv_system_user_avatar)
    ImageView mIvSystemUserAvatar;
    @BindView(R.id.tv_question)
    TextView mTvQuestion;
    @BindView(R.id.answer_recyclerview)
    RecyclerView mAnswerRecyclerview;

    // 本地头像
    private int[] aq_images_female = new int[]{R.drawable.nv1, R.drawable.nv2,
            R.drawable.nv3, R.drawable.nv4, R.drawable.nv5};
    private int[] aq_images_male = new int[]{R.drawable.nan1, R.drawable.nan2,
            R.drawable.nan3, R.drawable.nan4, R.drawable.nan5};

    private static final String ARG_QUESTION_INDEX = "arg_QUESTION_INDEX";
    private boolean isInto=true;

    public static AnswerQuestionFragment newInstance(int index) {
        AnswerQuestionFragment answerQuestionFragment = new AnswerQuestionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_QUESTION_INDEX, index);
        answerQuestionFragment.setArguments(bundle);
        return answerQuestionFragment;
    }

    // qa index
    private int index;
    private AnswerAdapter mAnswerAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_answer_question;
    }

    @Override
    protected void initViewsAndVariables() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            index = bundle.getInt(ARG_QUESTION_INDEX);
            mIvSystemUserAvatar.setImageResource(UserInfoXml.isMale() ? aq_images_female[index - 1] : aq_images_male[index - 1]);
            String question = "";
            List<String> answerList = new ArrayList<>();

//            String[] answerList = new String[0];
            switch (index) {
                case 1:// 婚姻状况
                    question ="你希望你的另一半有更多的性經驗嗎？";
                    String[] answerList1 = new String[]{"不希望", "當然，越多越好", "無所謂喔"};
                    answerList= Arrays.asList(answerList1);
                    break;
                case 2:// 教育
                    question = "你認為婚前性行為重要嗎？";
                    String[] answerList2= new String[]{"一定要婚前試愛", "內心是拒絕的", "無所謂啦"};
                    answerList= Arrays.asList(answerList2);
                    break;
                case 3:// 是否想要孩子
                    question = "你希望和Ta有一個什麼樣的first date？";
                    String[] answerList3 = new String[]{"驚喜浪漫噠", "簡單純真", "直接奔放","只要是你，其他不在意"};
                    answerList= Arrays.asList(answerList3);
                    break;
                case 5:// 兴趣爱好
                    question = "你傾向於一見鍾情還是日久生情？";
                    String[] answerList4 = new String[]{"一見鍾情", "日久生情", "一見鍾情然後日久生情","不確定"};
                    answerList= Arrays.asList(answerList4);
                    break;
                case 4:// 职业
                    question = "你能接受一夜情嗎？";
                    String[] answerList5 = new String[]{"完全不能", "可以的哦", "看跟誰哦","沒有戀人時可以，有戀人時絕對不會","一夜情是什麼？好吃嘛？","保密"};
                    answerList= Arrays.asList(answerList5);
                    break;




//                case 1:// 婚姻状况
//                    question = getString(R.string.question1);
//                    answerList = ParamsUtils.getMarriageMapValue();
//                    break;
//                case 2:// 教育
//                    question = getString(R.string.question2);
//                    answerList = ParamsUtils.getEducationMapValue();
//                    break;
//                case 3:// 是否想要孩子
//                    question = getString(R.string.question3);
//                    answerList = ParamsUtils.getWantBabyMapValue();
//                    break;
//                case 5:// 兴趣爱好
//                    question = getString(R.string.question4);
//                    answerList = ParamsUtils.getInterestMapValue();
//                    break;
//                case 4:// 职业
//                    question = getString(R.string.question5);
//                    answerList = ParamsUtils.getWorkMapValue();
//                    break;

            }
            mTvQuestion.setText(question);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mAnswerRecyclerview.setLayoutManager(linearLayoutManager);
            mAnswerAdapter = new AnswerAdapter(getActivity(), R.layout.item_answer_list, answerList);
            mAnswerRecyclerview.setAdapter(mAnswerAdapter);
        }
    }

    @Override
    protected void addListeners() {
        mAnswerAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                String selectedItem = mAnswerAdapter.getItemByPosition(position);
                switch (index) {
                    case 1:// 婚姻状况
                        UserInfoXml.setMarriage(ParamsUtils.getMarriageMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 2:// 学历
                        UserInfoXml.setEducation(ParamsUtils.getEducationMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 3:// 是否想要孩子
                        UserInfoXml.setWantBaby(ParamsUtils.getWantBabyMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 4:// 兴趣爱好
                        UserInfoXml.setInterest(ParamsUtils.getInterestMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 5:// 职业
                        UserInfoXml.setWork(ParamsUtils.getWorkMapKeyByValue(selectedItem));
                        showLoading();
                        CommonRequestUtil.uploadUserInfo(false, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
                                dismissLoading();
//                                Util.gotoActivity(getActivity(), UploadPhotoActivity.class, true);
                                if(isInto){
                                    isInto=false;
                                    Intent intent=new Intent(getActivity(),UploadPhotoActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.login_anim_in,R.anim.login_anim_out);
                                }

                            }

                            @Override
                            public void onFail() {
                                if(isInto) {
                                    isInto = false;
                                    dismissLoading();
                                    Util.gotoActivity(getActivity(), UploadPhotoActivity.class, true);
                                }
                            }
                        });
                        break;
                }
            }
        });
    }

    private void next() {
        ScrollControlViewPager viewPager = (ScrollControlViewPager) getActivity().findViewById(R.id.answer_question_viewpager);
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, false);
    }

    private static class AnswerAdapter extends CommonRecyclerViewAdapter<String> {
//
        public AnswerAdapter(Context context, int layoutResId) {
            super(context, layoutResId);
        }

        public AnswerAdapter(Context context, int layoutResId, List<String> list) {
            super(context, layoutResId, list);
        }

        @Override
        protected void convert(int position, RecyclerViewHolder viewHolder, String bean) {
            if (!TextUtils.isEmpty(bean)) {
                viewHolder.setText(R.id.item_answer, bean);
            }
        }
    }

}
