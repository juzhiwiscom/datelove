package com.yueai.xml;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.library.utils.SharedPreferenceUtil;
import com.yueai.bean.PlatformInfo;
import com.yueai.utils.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Platfrom信息
 * Created by zhangdroid on 2016/6/17.
 */
public class PlatformInfoXml {
    public static final String XML_NAME = "platform_info";
    public static final String KEY_PID = "pid";//手机串号
    public static final String KEY_IMSI = "imsi";//sim卡imsi号
    private static final String KEY_WIDTH = "width";//分辨率宽
    private static final String KEY_HEIGHT = "height";//分辨率高
    private static final String KEY_VERSION = "version";//版本号
    private static final String KEY_PHONE_TYPE = "phonetype";//手机型号
    private static final String KEY_FID = "fid";//渠道号
    private static final String KEY_PRODUCT_ID = "productId"; //产品号
    private static final String KEY_NET_TYPE = "netType";//联网类型(0->无网络,2->wifi,3->cmwap,4->cmnet,5->ctnet,6->ctwap,7->3gwap,8->3gnet,9->uniwap,10->uninet)
    private static final String KEY_MOBILE_IP = "mobileIP";//手机ip
    private static final String KEY_RELEASE_TIME = "releaseTime";//发版时间，例如：201501170945
    private static final String KEY_PLATFORM = "platform";//平台(1->IOS,2->Android)
    private static final String KEY_SYSTEM_VERSION = "systemVersion";//系统版本
    private static final String KEY_LANGUAGE = "language";// 语言
    private static final String KEY_COUNTRY = "country";//国家

    private static final String KEY_TOKEN = "token";// token值
    private static final String KEY_PUSH_TOKEN = "push_token";// 百度推送token值


    private static void saveIfNotEmpty(String key, String value) {
        if (!TextUtils.isEmpty(value) && !"".equals(value)) {
            SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, key, value);
        }
    }

    /**
     * 获得PlatformInfo
     *
     * @return
     */
    public static PlatformInfo getPlatformInfo() {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setPid(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PID, null));
        platformInfo.setImsi(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_IMSI, null));
        platformInfo.setW(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WIDTH, null));
        platformInfo.setH(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_HEIGHT, null));
        platformInfo.setVersion(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_VERSION, null));
        platformInfo.setPhonetype(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PHONE_TYPE, null));
        platformInfo.setFid(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_FID, null));
        platformInfo.setProduct(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PRODUCT_ID, null));
        platformInfo.setNetType(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_NET_TYPE, null));
        platformInfo.setMobileIP(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MOBILE_IP, null));
        platformInfo.setRelease(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_RELEASE_TIME, null));
        platformInfo.setPlatform(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PLATFORM, null));
        platformInfo.setSystemVersion(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SYSTEM_VERSION, null));
        platformInfo.setLanguage(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LANGUAGE, null));
        platformInfo.setCountry(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_COUNTRY, null));
        return platformInfo;
    }

    /**
     * 保存platform信息
     *
     * @param platformInfo
     */
    public static void setPlatfromInfo(PlatformInfo platformInfo) {
        if (platformInfo != null) {
            saveIfNotEmpty(KEY_PID, platformInfo.getPid());
            saveIfNotEmpty(KEY_IMSI, platformInfo.getImsi());
            saveIfNotEmpty(KEY_WIDTH, platformInfo.getW());
            saveIfNotEmpty(KEY_HEIGHT, platformInfo.getH());
            saveIfNotEmpty(KEY_VERSION, platformInfo.getVersion());
            saveIfNotEmpty(KEY_PHONE_TYPE, platformInfo.getPhonetype());
            saveIfNotEmpty(KEY_PRODUCT_ID, platformInfo.getProduct());
            saveIfNotEmpty(KEY_NET_TYPE, platformInfo.getNetType());
            saveIfNotEmpty(KEY_MOBILE_IP, platformInfo.getMobileIP());
            saveIfNotEmpty(KEY_RELEASE_TIME, platformInfo.getRelease());
            saveIfNotEmpty(KEY_PLATFORM, platformInfo.getPlatform());
            saveIfNotEmpty(KEY_SYSTEM_VERSION, platformInfo.getSystemVersion());
            saveIfNotEmpty(KEY_LANGUAGE, platformInfo.getLanguage());
            saveIfNotEmpty(KEY_COUNTRY, platformInfo.getCountry());
            saveIfNotEmpty(KEY_FID, platformInfo.getFid());
        }
    }

    /**
     * 获得Platform JSON字符串
     */
    public static String getPlatformJsonString() {
        // 创建以platformInfo为Key的JSON字符串
        Map<String, PlatformInfo> map = new HashMap<>();
        map.put("platformInfo", getPlatformInfo());
        return JSON.toJSONString(map);
    }

    /**
     * 获得服务器返回的token值
     */
    public static String getToken() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_TOKEN, null);
    }

    public static void setToken(String token) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_TOKEN, token);
    }

    /**
     * 获得百度推送token值
     */
    public static String getPushToken() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PUSH_TOKEN, null);
    }

    public static void setPushToken(String token) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_PUSH_TOKEN, token);
    }

    public static String getCountry() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_COUNTRY, null);
    }

    public static void setCountry(String value) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_COUNTRY, value);
    }

    public static String getLanguage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LANGUAGE, null);
    }

    public static void setLanguage(String value) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_LANGUAGE, value);
    }

}
