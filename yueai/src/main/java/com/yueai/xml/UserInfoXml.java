package com.yueai.xml;

import android.text.TextUtils;

import com.library.utils.SharedPreferenceUtil;
import com.yueai.bean.Area;
import com.yueai.bean.Image;
import com.yueai.bean.User;
import com.yueai.bean.UserBase;
import com.yueai.utils.Utils;

/**
 * 登陆用户信息
 * Created by zhangdroid on 2016/6/24.
 */
public class UserInfoXml {
    public static final String XML_NAME = "user_info";
    // 基本信息
    private static final String KEY_USER_NAME = "username";// 账号
    private static final String KEY_PASSWORD = "password";// 密码
    private static final String KEY_UID = "uid";// id
    private static final String KEY_NICKNAME = "nickname";// 昵称
    private static final String KEY_GENDER = "gender";// 性别
    private static final String KEY_AGE = "age";// 年龄
    private static final String KEY_BIRTHDAY = "birthday";// 生日
    private static final String KEY_IS_BEAN_USER = "isBeanUser";//是否豆币用户
    private static final String KEY_IS_MONTHLY = "isMonthly";// 是否包月用户
    private static final String KEY_BEAN_COUNT = "beanCount";// 豆币数量
    // 头像相关
    private static final String KEY_AVATAR_URL = "avatarUrl";// 头像原图url
    private static final String KEY_AVATAR_URL_C = "avatarUrlC";// 头像缩略url
    private static final String KEY_AVATAR_STATUS = "avatarStatus";// 头像审核状态
    // 详细资料项
    private static final String KEY_COUNTRY = "country";// 国家
    private static final String KEY_LANGUAGE = "language";// 国家对应的语言
    private static final String KEY_PROVINCE_NAME = "provinceName";// 地区
    private static final String KEY_MONOLOGUE = "monologue";// 内心独白
    private static final String KEY_CONSTELLATION = "constellation";// 星座
    private static final String KEY_WORK = "work";// 工作
    private static final String KEY_INCOME = "income";// 收入
    private static final String KEY_HEIGHT = "height";// 身高
    private static final String KEY_WEIGHT = "weight";// 体重
    private static final String KEY_EDUCATION = "education";// 学历
    private static final String KEY_MARRIAGE = "marriage";// 婚姻状况
    private static final String KEY_WANT_BABY = "wantBaby";// 是否要小孩
    private static final String KEY_INTEREST = "interest";// 兴趣
    private static final String KEY_PERSONAL = "personal";// 个性特征
    private static final String KEY_SPORT = "sport";// 喜欢的运动
    private static final String KEY_BOOK_CARTOON = "book_cartoon";// 喜欢的书和漫画
    private static final String KEY_FOOD = "food";// 喜欢的食物
    private static final String KEY_TRAVEL = "travel";// 旅行
    private static final String KEY_MUSIC = "music";// 喜欢的音乐
    private static final String KEY_MOVIES = "movies";// 喜欢的电影
    private static final String KEY_BLOOD = "blood";// 血型
    private static final String KEY_CHARM = "charm";// 魅力部位
    private static final String KEY_DIFFAREA = "diffAreaLove";// 异地恋
    private static final String KEY_HOUSE = "house";// 住房状况
    private static final String KEY_LIVE_WITH_PARENT = "liveWithParent";// 与父母同住
    private static final String KEY_LOVETYPE = "loveType";// 喜欢的类型
    private static final String KEY_SEXBEF_MARRIED = "sexBefMarried";// 亲密行为

    private static final String KEY_MATCH_INFO = "matchInfo";// 征友条件
    private static final String KEY_IS_CLIENT_ACTIVATE = "is_client_activate";// 是否激活
    private static final String KEY_PARAMS_INIT = "paramsInit";// 初始化数据
    private static final String KEY_SHOW_HEAD_MSG = "headMsg";// 是否显示页眉
    private static final String KEY_SHOW_HEAD_MSG_NEW = "headMsgNew";// 是否显示新页眉
    private static final String KEY_SHOW_RECOMMEND_USER = "recommendUser";// 是否显示推荐用户

    private static void saveIfExists(String key, String value) {
        if (!TextUtils.isEmpty(value) && !"".equals(value)) {
            SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, key, value);
        }
    }

    /**
     * 保存用户信息
     */
    public static void setUserInfo(User user) {
        if (user != null) {
            setBeanCount(user.getBeanCurrencyCount());
            setUsername(user.getAccount());
            setPassword(user.getPassword());
            setBirthday(user.getBirthday());
            setWeight(String.valueOf(user.getWeight()));
            setWantBaby(String.valueOf(user.getWantsKids()));
            setSport(user.getSports());
            setBookCartoon(user.getBooks());
            setTravel(user.getTravel());
            setInterest(user.getListHobby());
            setPersonal(user.getCharacteristics());
            UserBase userBase = user.getUserBaseEnglish();
            if (userBase != null) {
                Image image = userBase.getImage();
                if (image != null) {
                    // 保存头像原图
                    setAvatarUrl(image.getImageUrl());
                    // 保存头像缩略图
                    setAvatarThumbnailUrl(image.getThumbnailUrl());
                    // 保存头像审核状态
                    setAvatarStatus(image.getStatus());
                }
                setUID(userBase.getId());
                setGender(String.valueOf(userBase.getSex()));
                setAge(String.valueOf(userBase.getAge()));
                setIsMonthly(String.valueOf(userBase.getIsMonthly()));
                setIsBeanUser(String.valueOf(userBase.getIsBeanUser()));
                setMonologue(userBase.getMonologue());
                setNickName(userBase.getNickName());
                setConstellation(String.valueOf(userBase.getSign()));
                setCountry(userBase.getCountry());
                setLanguage(userBase.getLanguage());
                Area area = userBase.getArea();
                if (area != null) {
                    setProvinceName(area.getProvinceName());
                }
                setIncome(String.valueOf(userBase.getIncome()));
                setHeight(userBase.getHeightCm());
                setWork(String.valueOf(userBase.getOccupation()));
                setEducation(String.valueOf(userBase.getEducation()));
                setMarriage(String.valueOf(userBase.getMaritalStatus()));
            }
        }
    }

    // ***********************************************  公用方法，获取用户基本信息  ***********************************************

    /**
     * @return 是否包月用户
     */

    public static boolean isMonthly() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_IS_MONTHLY, "0"));
    }

    /**
     * 存储是否包月用户
     */
    public static void setIsMonthly(String value) {
        saveIfExists(KEY_IS_MONTHLY, value);
    }

    /**
     * @return 是否豆币用户
     */
    public static boolean isBeanUser() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_IS_BEAN_USER, "0"));
    }

    /**
     * 存储是否豆币用户
     */
    public static void setIsBeanUser(String value) {
        saveIfExists(KEY_IS_BEAN_USER, value);
    }

    /**
     * @return 豆币数量
     */
    public static int getBeanCount() {
        return SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_BEAN_COUNT, 0);
    }

    /**
     * 存储豆币值
     */
    public static void setBeanCount(int value) {
        SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_BEAN_COUNT, value);
    }

    /**
     * @return ture is male, else famale
     */
    public static boolean isMale() {
        return "0".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_GENDER, "0"));
    }

    /**
     * 存储用户名
     */
    public static void setUsername(String username) {
        saveIfExists(KEY_USER_NAME, username);
    }

    /**
     * @return 用户名
     */
    public static String getUsername() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_USER_NAME, null);
    }

    /**
     * 存储密码
     */
    public static void setPassword(String password) {
        saveIfExists(KEY_PASSWORD, password);
    }

    /**
     * @return 密码
     */
    public static String getPassword() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PASSWORD, null);
    }

    /**
     * @return 用户ID
     */
    public static String getUID() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_UID, null);
    }

    /**
     * 存储用户ID
     */
    public static void setUID(String value) {
        saveIfExists(KEY_UID, value);
    }

    /**
     * @return 用户性别 "0"：男性，"1"：女性
     */
    public static String getGender() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_GENDER, "0");
    }

    /**
     * 存储性别
     */
    public static void setGender(String value) {
        saveIfExists(KEY_GENDER, value);
    }

    /**
     * @return 用户年龄
     */
    public static String getAge() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AGE, null);
    }

    /**
     * 存储年龄
     */
    public static void setAge(String value) {
        saveIfExists(KEY_AGE, value);
    }

    /**
     * @return 内心独白
     */
    public static String getMonologue() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MONOLOGUE, null);
    }

    /**
     * 存储内心独白
     */
    public static void setMonologue(String value) {
        saveIfExists(KEY_MONOLOGUE, value);
    }

    /**
     * @return 昵称
     */
    public static String getNickName() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_NICKNAME, null);
    }

    /**
     * 设置昵称
     */
    public static void setNickName(String value) {
        saveIfExists(KEY_NICKNAME, value);
    }

    /**
     * @return 生日
     */
    public static String getBirthday() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_BIRTHDAY, null);
    }

    /**
     * 存储生日
     */
    public static void setBirthday(String value) {
        saveIfExists(KEY_BIRTHDAY, value);
    }


    // ***********************************************  公用方法，获取用户图片相关信息  ***********************************************


    /**
     * @return 头像缩略图Url
     */
    public static String getAvatarThumbnailUrl() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_URL_C, null);
    }

    /**
     * 存储头像缩略图
     */
    public static void setAvatarThumbnailUrl(String value) {
        saveIfExists(KEY_AVATAR_URL_C, value);
    }

    /**
     * @return 头像原图Url
     */
    public static String getAvatarUrl() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_URL, null);
    }

    /**
     * 存储头像原图
     */
    public static void setAvatarUrl(String value) {
        saveIfExists(KEY_AVATAR_URL, value);
    }

    /**
     * @return 头像审核状况
     */
    public static String getAvatarStatus() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_STATUS, null);
    }

    /**
     * 存储头像审核状态
     */
    public static void setAvatarStatus(String value) {
        saveIfExists(KEY_AVATAR_STATUS, value);
    }


    // ***********************************************  公用方法，用户资料项信息  ***********************************************


    /**
     * @return 国家
     */
    public static String getCountry() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_COUNTRY, null);
    }

    /**
     * 存储国家
     */
    public static void setCountry(String value) {
        saveIfExists(KEY_COUNTRY, value);
    }

    /**
     * @return 语言
     */
    public static String getLanguage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LANGUAGE, null);
    }

    /**
     * 存储语言
     */
    public static void setLanguage(String value) {
        saveIfExists(KEY_LANGUAGE, value);
    }

    /**
     * @return 地区
     */
    public static String getProvinceName() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PROVINCE_NAME, null);
    }

    /**
     * 存储地区
     */
    public static void setProvinceName(String value) {
        saveIfExists(KEY_PROVINCE_NAME, value);
    }

    /**
     * @return 星座
     */
    public static String getConstellation() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_CONSTELLATION, null);
    }

    /**
     * 存储星座
     */
    public static void setConstellation(String value) {
        saveIfExists(KEY_CONSTELLATION, value);
    }

    /**
     * @return 收入
     */
    public static String getIncome() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_INCOME, null);
    }

    /**
     * 存储收入
     */
    public static void setIncome(String value) {
        saveIfExists(KEY_INCOME, value);
    }

    /**
     * @return 身高
     */
    public static String getHeight() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_HEIGHT, null);
    }

    /**
     * 存储身高
     */
    public static void setHeight(String value) {
        saveIfExists(KEY_HEIGHT, value);
    }

    /**
     * @return 体重
     */
    public static String getWeight() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WEIGHT, null);
    }

    /**
     * 存储体重
     */
    public static void setWeight(String value) {
        if (!"0".equals(value)) {
            saveIfExists(KEY_WEIGHT, value);
        }
    }

    /**
     * @return 职业
     */
    public static String getWork() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WORK, null);
    }

    /**
     * 存储职业
     */
    public static void setWork(String value) {
        saveIfExists(KEY_WORK, value);
    }

    /**
     * @return 学历
     */
    public static String getEducation() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_EDUCATION, null);
    }

    /**
     * 存储学历
     */
    public static void setEducation(String value) {
        saveIfExists(KEY_EDUCATION, value);
    }

    /**
     * @return 婚姻状况
     */
    public static String getMarriage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MARRIAGE, null);
    }

    /**
     * 存储婚姻状况
     */
    public static void setMarriage(String value) {
        saveIfExists(KEY_MARRIAGE, value);
    }

    /**
     * @return 是否想要小孩
     */
    public static String getWantBaby() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WANT_BABY, null);
    }

    /**
     * 存储是否想要小孩
     */
    public static void setWantBaby(String value) {
        saveIfExists(KEY_WANT_BABY, value);
    }

    /**
     * @return 喜欢的运动
     */
    public static String getSport() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SPORT, null);
    }

    /**
     * 存储喜欢的运动
     */
    public static void setSport(String value) {
        saveIfExists(KEY_SPORT, value);
    }

    /**
     * @return 个性
     */
    public static String getPersonal() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PERSONAL, null);
    }

    /**
     * 存储个性
     */
    public static void setPersonal(String value) {
        saveIfExists(KEY_PERSONAL, value);
    }

    /**
     * @return 兴趣爱好
     */
    public static String getInterest() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_INTEREST, null);
    }

    /**
     * 存储兴趣爱好
     */
    public static void setInterest(String value) {
        saveIfExists(KEY_INTEREST, value);
    }

    /**
     * @return 喜欢的书、漫画
     */
    public static String getBookCartoon() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_BOOK_CARTOON, null);
    }

    /**
     * 存储喜欢的书、漫画
     */
    public static void setBookCartoon(String value) {
        saveIfExists(KEY_BOOK_CARTOON, value);
    }

    /**
     * @return 旅行
     */
    public static String getTravel() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_TRAVEL, null);
    }

    /**
     * 存储旅行
     */
    public static void setTravel(String value) {
        saveIfExists(KEY_TRAVEL, value);
    }

    /**
     * @return 喜欢的食物
     */
    public static String getFood() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_FOOD, null);
    }

    /**
     * 存储喜欢的食物
     */
    public static void setFood(String value) {
        saveIfExists(KEY_FOOD, value);
    }

    /**
     * @return 喜欢的音乐
     */
    public static String getMusic() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MUSIC, null);
    }

    /**
     * 存储喜欢的音乐
     */
    public static void setMusic(String value) {
        saveIfExists(KEY_MUSIC, value);
    }

    /**
     * @return 喜欢的电影
     */
    public static String getMovies() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MOVIES, null);
    }

    /**
     * 存储喜欢的电影
     */
    public static void setMovies(String value) {
        saveIfExists(KEY_MOVIES, value);
    }

    /**
     * @return 血型
     */
    public static String getBlood() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_BLOOD, null);
    }

    /**
     * 存储血型
     */
    public static void setBlood(String value) {
        saveIfExists(KEY_BLOOD, value);
    }

    /**
     * @return 魅力部位
     */
    public static String getCharm() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_CHARM, null);
    }

    /**
     * 存储魅力部位
     */
    public static void setCharm(String value) {
        saveIfExists(KEY_CHARM, value);
    }

    /**
     * @return 异地恋
     */
    public static String getDiffAreaLove() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_DIFFAREA, null);
    }

    /**
     * 存储异地恋
     */
    public static void setDiffAreaLove(String value) {
        saveIfExists(KEY_DIFFAREA, value);
    }

    /**
     * @return 住房状况
     */
    public static String getHouse() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_HOUSE, null);
    }

    /**
     * 存储住房状况
     */
    public static void setHouse(String value) {
        saveIfExists(KEY_HOUSE, value);
    }

    /**
     * @return 与父母同住
     */
    public static String getLiveWithParent() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LIVE_WITH_PARENT, null);
    }

    /**
     * 存储与父母同住
     */
    public static void setLiveWithParent(String value) {
        saveIfExists(KEY_LIVE_WITH_PARENT, value);
    }

    /**
     * @return 喜欢的类型
     */
    public static String getLoveType() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LOVETYPE, null);
    }

    /**
     * 存储喜欢的类型
     */
    public static void setLoveType(String value) {
        saveIfExists(KEY_LOVETYPE, value);
    }

    /**
     * @return 亲密行为
     */
    public static String getSexBefMarried() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SEXBEF_MARRIED, null);
    }

    /**
     * 存储亲密行为
     */
    public static void setSexBefMarried(String value) {
        saveIfExists(KEY_SEXBEF_MARRIED, value);
    }


    // ***********************************************  公用方法，保存其它用户相关信息  ***********************************************


    /**
     * @return 搜索條件
     */
    public static String getMatchInfo() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MATCH_INFO, null);
    }

    /**
     * @return 搜索條件
     */
    public static void setMatchInfo(String value) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_MATCH_INFO, value);
    }

    /**
     * 获得是否激活
     */
    public static boolean getIsClient() {
        return SharedPreferenceUtil.getBooleanValue(Utils.getContext(), XML_NAME, KEY_IS_CLIENT_ACTIVATE, false);
    }

    public static void setIsClient() {
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), XML_NAME, KEY_IS_CLIENT_ACTIVATE, true);
    }

    /**
     * 获得初始化数据
     */
    public static String getParamsInit() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PARAMS_INIT, null);
    }

    public static void setParamsInit(String paramsInit) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_PARAMS_INIT, paramsInit);
    }

    public static void setIsShowHeadMsg(String isShowHeadMsg) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_SHOW_HEAD_MSG, isShowHeadMsg);
    }

    public static boolean isShowHeadMsg() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SHOW_HEAD_MSG, "0"));
    }

    public static void setIsShowHeadMsgNew(String isShowHeadMsgNew) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_SHOW_HEAD_MSG_NEW, isShowHeadMsgNew);
    }

    public static boolean isShowHeadMsgNew() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SHOW_HEAD_MSG_NEW, "0"));
    }

    public static void setIsShowRecommendUser(String isShowRecommendUser) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_SHOW_RECOMMEND_USER, isShowRecommendUser);
    }

    public static boolean isShowRecommendUser() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SHOW_RECOMMEND_USER, "0"));
    }

}
