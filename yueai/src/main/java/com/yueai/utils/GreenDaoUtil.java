package com.yueai.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.yueai.online.DaoMaster;
import com.yueai.online.DaoSession;

/**
 * An util for greenDao
 * Created by zhangdroid on 2017/5/4.
 */
public class GreenDaoUtil {
    private static DaoMaster.DevOpenHelper mHelper;
    private static SQLiteDatabase mSqLiteDatabase;
    private static DaoSession mDaoSession;

    private GreenDaoUtil() {
    }

    /**
     * 初始化greenDao，这个操作建议在Application初始化时进行
     *
     * @param context 上下文对象
     * @param name    数据库名称
     */
    public static void initDatabase(Context context, String name) {
        mHelper = new DaoMaster.DevOpenHelper(context, name, null);
        mSqLiteDatabase = mHelper.getWritableDatabase();
        // 注意：该数据库连接属于 DaoMaster，所以多个 Session 指的是相同的数据库连接。
        mDaoSession = new DaoMaster(mSqLiteDatabase).newSession();
    }

    public static DaoSession getDaoSession() {
        return mDaoSession;
    }

    public static SQLiteDatabase getDatabase() {
        return mSqLiteDatabase;
    }

}
