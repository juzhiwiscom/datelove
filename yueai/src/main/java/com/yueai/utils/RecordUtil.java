package com.yueai.utils;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.text.TextUtils;

import java.io.File;

/**
 * 录音工具类
 * Created by zhangdroid on 2016/7/20.
 */
public class RecordUtil {
    private static volatile RecordUtil sDefault;
    // 录音器
    private MediaRecorder mRecorder;
    // 播放器
    private MediaPlayer mPlayer;

    private RecordUtil() {
    }

    public static RecordUtil getInstance() {
        if (sDefault == null) {
            synchronized (RecordUtil.class) {
                if (sDefault == null) {
                    sDefault = new RecordUtil();
                }
            }
        }
        return sDefault;
    }
    /**开始播放**/
    public void startPaly(final OnPlayerListener listener){
        if(!isPlaying()){
            mPlayer.start();
        }
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mPlayer.release();
                mPlayer = null;
                if (listener != null) {
                    listener.onCompleted();
                }
            }
        });
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mPlayer.release();
                mPlayer = null;
                if (listener != null) {
                    listener.onCompleted();
                }
                return true;
            }
        });

    }

    /**暂停录音**/
    public void pause(){
        if(isPlaying()){
            mPlayer.pause();
        }

    }
    /**判断mplayer是否为空**/
    public boolean mPlayerIsNull(){
        return mPlayer==null;
    }

    /**
     * 开始录音
     *
     * @param path 录音后输出的文件路径，绝对路径
     */
    public void startRecord(String path) {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        File file = new File(path);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        // 初始化MediaRecorder
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
        }

        try {
            // 设置录音源为麦克风
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            // 设置录音文件格式(顺序不能换)
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            // 设置录音文件存放路径
            mRecorder.setOutputFile(path);

            // 开始录音
            mRecorder.prepare();
            mRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停止录音
     */
    public void stopRecord() {
        try {
            if (mRecorder != null) {
                mRecorder.stop();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放录音器资源
     */
    public void releaseRecord() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }
    }

    /**
     * * 开始播放录音
     *
     * @param path     录音文件路径（绝对路径）
     * @param listener 监听器，控制动画的播放
     */
    public void play(String path, final OnPlayerListener listener) {
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
        }
        try {
            mPlayer.reset();
            mPlayer.setDataSource(path);
            mPlayer.prepare();
            mPlayer.start();

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                    }
                }
            });
            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                    }
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停止播放录音
     */
    public void stop() {
        if (mPlayer != null) {
            mPlayer.stop();
        }
    }

    /**
     * 释放播放器资源
     */
    public void release() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    /**
     * 是否正在播放
     *
     * @return
     */
    public boolean isPlaying() {
        return !(mPlayer == null || !mPlayer.isPlaying());
    }

    /**
     * 切换播放/暂停状态
     *
     * @param path     录音文件路径（绝对路径）
     * @param listener 监听器，控制动画的播放
     */
    public void toggle(String path, OnPlayerListener listener) {
        try {
            if (isPlaying()) {
                // 暂停
                mPlayer.pause();
                if (listener != null) {
                    listener.onPaused();
                }
            } else {
                // 播放
                play(path, listener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放器状态监听器
     */
    public interface OnPlayerListener {

        /**
         * 播放完成/异常
         */
        void onCompleted();

        /**
         * 暂停
         */
        void onPaused();
    }

}

