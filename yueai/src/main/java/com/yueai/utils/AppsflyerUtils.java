package com.yueai.utils;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.yueai.base.BaseApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * Appsflyer辅助类
 * Created by zhangdroid on 2017/3/17.
 */
public class AppsflyerUtils {

    private static void trackEvent(String eventName, Map<String, Object> eventValues) {
        AppsFlyerLib.getInstance().trackEvent(BaseApplication.getGlobalContext(), eventName, eventValues);
    }

    /**
     * 激活
     */
    public static void activate() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("activate", 1);
        trackEvent("Activation", eventValues);
    }

    /**
     * 注册
     */
    public static void register() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventType.COMPLETE_REGISTRATION, 1);
        trackEvent("Register", eventValues);
    }

    /**
     * 登陆
     */
    public static void login() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventType.LOGIN, 1);
        trackEvent("Login", eventValues);
    }

    /**
     * 点击一个月包月服务
     */
    public static void click1Month() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("oneMonth", 1);
        trackEvent("Click one month service", eventValues);
    }

    /**
     * 点击三个月包月服务
     */
    public static void click3Month() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("threeMonth", 1);
        trackEvent("Click three month service", eventValues);
    }

    /**
     * 点击一年包月服务
     */
    public static void clickOneYear() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("oneYear", 1);
        trackEvent("Click one year service", eventValues);
    }

    /**
     * 点击300豆币
     */
    public static void click300Bean() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("300Bean", 1);
        trackEvent("Click 300 Bean", eventValues);
    }

    /**
     * 点击600豆币
     */
    public static void click600Bean() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("600Bean", 1);
        trackEvent("Click 600 Bean", eventValues);
    }

    /**
     * 点击1500豆币
     */
    public static void click1500Bean() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("1500Bean", 1);
        trackEvent("Click 1500 Bean", eventValues);
    }

    /**
     * 成功购买一个月包月服务
     */
    public static void purchase1Month() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventParameterName.REVENUE, 500);// 收益
        eventValues.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        eventValues.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        eventValues.put(AFInAppEventParameterName.CURRENCY, "TWD");// 币种
        trackEvent("One month service", eventValues);
    }

    /**
     * 成功购买三个月包月服务
     */
    public static void purchase3Month() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventParameterName.REVENUE, 1000);// 收益
        eventValues.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        eventValues.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        eventValues.put(AFInAppEventParameterName.CURRENCY, "TWD");// 币种
        trackEvent("Three month service", eventValues);
    }

    /**
     * 成功购买一年包月服务
     */
    public static void purchaseOneYear() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventParameterName.REVENUE, 2500);// 收益
        eventValues.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        eventValues.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        eventValues.put(AFInAppEventParameterName.CURRENCY, "TWD");// 币种
        trackEvent("One year service", eventValues);
    }

    /**
     * 成功购买300豆币
     */
    public static void purchase300Bean() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventParameterName.REVENUE, 300);// 收益
        eventValues.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        eventValues.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        eventValues.put(AFInAppEventParameterName.CURRENCY, "TWD");// 币种
        trackEvent("300 Bean revenue", eventValues);
    }

    /**
     * 成功购买600豆币
     */
    public static void purchase600Bean() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventParameterName.REVENUE, 600);// 收益
        eventValues.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        eventValues.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        eventValues.put(AFInAppEventParameterName.CURRENCY, "TWD");// 币种
        trackEvent("600 Bean revenue", eventValues);
    }

    /**
     * 成功购买1500豆币
     */
    public static void purchase1500Bean() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put(AFInAppEventParameterName.REVENUE, 1500);// 收益
        eventValues.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        eventValues.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        eventValues.put(AFInAppEventParameterName.CURRENCY, "TWD");// 币种
        trackEvent("1500 Bean revenue", eventValues);
    }

}
