package com.yueai.utils;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.library.utils.ToastUtil;
import com.yueai.R;
import com.yueai.bean.Area;
import com.yueai.bean.AreaBean;
import com.yueai.bean.BaseModel;
import com.yueai.bean.HeadMsgNotice;
import com.yueai.bean.Image;
import com.yueai.bean.MyInfo;
import com.yueai.bean.NewHeadMsgNotice;
import com.yueai.bean.RecommendUser;
import com.yueai.bean.SayHello;
import com.yueai.bean.UpImage;
import com.yueai.bean.User;
import com.yueai.constant.IUrlConstant;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 公用接口请求工具类
 * 1、初始化数据字典
 * 2、打招呼
 * 3、拉黑
 * 4、取消拉黑
 * 5、删除聊天记录
 * 6、关注
 * 7、取消关注
 * 8、上传/加载用户资料项信息
 * // 图片相关
 * 9、上传图片
 * 10、删除图片
 * 11、设置为头像
 * 12、获取页眉
 * 13、获取新页眉
 * 14、获取推荐用户
 * Created by zhangdroid on 2016/6/30.
 */
public class CommonRequestUtil {

    /**
     * 缘分打招呼
     */
    public static final String SAY_HELLO_TYPE_FATE = "1";
    /**
     * 搜索打招呼
     */
    public static final String SAY_HELLO_TYPE_SEARCH = "2";
    /**
     * 用户详情页打招呼
     */
    public static final String SAY_HELLO_TYPE_USER_INFO = "3";
    /**
     * 附近的人打招呼
     */
    public static final String SAY_HELLO_TYPE_NEARBY = "9";
    /**
     * 最近访客打招呼
     */
    public static final String SAY_HELLO_TYPE_GUEST = "10";

    /**
     * 初始化数据字典(带有监听回调)
     *
     * @param onCommonListener 请求结果回调
     */
    public static void initParamsDict(final OnCommonListener onCommonListener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_INIT)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new StringCallback() {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (onCommonListener != null) {
                            onCommonListener.onFail();
                        }
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        UserInfoXml.setParamsInit(response);
                        if (onCommonListener != null) {
                            onCommonListener.onSuccess();
                        }
                    }
                });
    }

    /**
     * 打招呼
     *
     * @param context      上下文对象
     * @param userId       用户id
     * @param sayHelloType 打招呼类型
     * @param showToast    是否显示toast
     * @param listener     请求结果回调
     */
    public static void sayHello(final Context context, String userId, String sayHelloType, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SAY_HELLO)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", userId)
                .addParams("sayHelloType", sayHelloType)
                .build()
                .execute(new Callback<SayHello>() {

                    @Override
                    public SayHello parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, SayHello.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(context, context.getString(R.string.sayHello_fail));
                        }
                    }

                    @Override
                    public void onResponse(SayHello response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(context, message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 取消关注
     *
     * @param context   上下文对象
     * @param userId    用户id
     * @param showToast 是否显示toast
     * @param listener  请求结果回调
     */
    public static void cancelFollow(final Context context, String userId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_CANCEL_FOLLOW)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", userId)
                .build()
                .execute(new Callback<BaseModel>() {

                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(context, context.getString(R.string.cancel_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(context, message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 拉黑
     *
     * @param userId    用户id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void pull2Black(String userId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_PULL_TO_BLACK)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", userId)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.pull_to_black_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(Utils.getContext(), message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 取消拉黑
     *
     * @param userId    用户id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void cancelPull2Black(String userId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_CANCEL_BLACK)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", userId)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.cancel_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(Utils.getContext(), message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 删除聊天记录
     *
     * @param userId    用户id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void deleteChatHistory(String userId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_DEL_CHAT_HISOTRY)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("remoteUserIds", userId)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.del_chat_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(Utils.getContext(), message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 关注
     *
     * @param userId    用户id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void follow(String userId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_FOLLOW)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", userId)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.follow_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(Utils.getContext(), message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 取消关注
     *
     * @param userId    用户id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void cancelFollow(String userId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_CANCEL_FOLLOW)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", userId)
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.cancel_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String message = response.getMsg();
                            if (showToast && !TextUtils.isEmpty(message)) {
                                ToastUtil.showShortToast(Utils.getContext(), message);
                            }
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        }
                    }
                });
    }

    /**
     * 上传用户个人资料项
     *
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void uploadUserInfo(final boolean showToast, final OnCommonListener listener) {
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("platformInfo", PlatformInfoXml.getPlatformJsonString());
        String nickname = UserInfoXml.getNickName();
        if (!TextUtils.isEmpty(nickname)) {
            mapParams.put("nickName", nickname);
        }
        String monologue = UserInfoXml.getMonologue();
        if (!TextUtils.isEmpty(monologue)) {
            mapParams.put("monologue", monologue);
        }
        String birthday = UserInfoXml.getBirthday();
        if (!TextUtils.isEmpty(birthday)) {
            mapParams.put("birthday", birthday);
        }
        String constellation = UserInfoXml.getConstellation();
        if (!TextUtils.isEmpty(constellation)) {
            mapParams.put("constellationId", constellation);
        }
        String provinceName = UserInfoXml.getProvinceName();
        if (!TextUtils.isEmpty(provinceName)) {
            Area area = new Area();
            area.setProvinceName(provinceName);
            AreaBean areaBean = new AreaBean();
            areaBean.setArea(area);
            mapParams.put("area", JSON.toJSONString(areaBean));
        }
        String income = UserInfoXml.getIncome();
        if (!TextUtils.isEmpty(income)) {
            mapParams.put("income", income);
        }
        String height = UserInfoXml.getHeight();
        if (!TextUtils.isEmpty(height)) {
            mapParams.put("height", height);
        }
        String occupation = UserInfoXml.getWork();
        if (!TextUtils.isEmpty(occupation)) {
            mapParams.put("occupation", occupation);
        }
        String education = UserInfoXml.getEducation();
        if (!TextUtils.isEmpty(education)) {
            mapParams.put("education", education);
        }
        String marriage = UserInfoXml.getMarriage();
        if (!TextUtils.isEmpty(marriage)) {
            mapParams.put("maritalStatus", marriage);
        }
        String wantBaby = UserInfoXml.getWantBaby();
        if (!TextUtils.isEmpty(wantBaby)) {
            mapParams.put("childStatus", wantBaby);
        }
        String sport = UserInfoXml.getSport();
        if (!TextUtils.isEmpty(sport)) {
            mapParams.put("sports", sport);
        }
        String personal = UserInfoXml.getPersonal();
        if (!TextUtils.isEmpty(personal)) {
            mapParams.put("characteristics", personal);
        }
        String travel = UserInfoXml.getTravel();
        if (!TextUtils.isEmpty(travel)) {
            mapParams.put("travel", travel);
        }
        String bookCartoon = UserInfoXml.getBookCartoon();
        if (!TextUtils.isEmpty(bookCartoon)) {
            mapParams.put("books", bookCartoon);
        }
        String interest = UserInfoXml.getInterest();
        if (!TextUtils.isEmpty(interest)) {
            mapParams.put("listHobby", interest);
        }
        OkHttpUtils.post()
                .url(IUrlConstant.URL_UPLOAD_USER_INFO)
                .params(mapParams)
                .build()
                .execute(new Callback<MyInfo>() {
                    @Override
                    public MyInfo parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, MyInfo.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                    }

                    @Override
                    public void onResponse(MyInfo response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                // 上传成功后更新本地信息
                                UserInfoXml.setUserInfo(response.getUserEnglish());
                                if (showToast) {
                                    ToastUtil.showShortToast(Utils.getContext(), response.getMsg());
                                }
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 加载用户个人信息
     *
     * @param listener 请求结果回调
     */
    public static void loadUserInfo(final OnLoadUserInfoListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_USER_INFO)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new Callback<MyInfo>() {

                    @Override
                    public MyInfo parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, MyInfo.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                    }

                    @Override
                    public void onResponse(MyInfo response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                User user = response.getUserEnglish();
                                if (user != null) {
                                    UserInfoXml.setUserInfo(user);
                                }
                                if (listener != null) {
                                    listener.onSuccess(user);
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 上传图片
     *
     * @param isMain    是否上传头像（“1”表示上传头像，“2”表示普通图片）
     * @param file      图片File
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void uploadImage(boolean isMain, File file, final boolean showToast, final OnUploadImageListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_UPLOAD_PHOTO)
                .addHeader("isMain", isMain ? "1" : "2")
                .addHeader("token", PlatformInfoXml.getToken())
                .addHeader("productId", PlatformInfoXml.getPlatformInfo().getProduct())
                .addHeader("fid", PlatformInfoXml.getPlatformInfo().getFid())
                .addHeader("pid", PlatformInfoXml.getPlatformInfo().getPid())
                .addHeader("country", PlatformInfoXml.getPlatformInfo().getCountry())
                .addHeader("language", PlatformInfoXml.getPlatformInfo().getLanguage())
                .addHeader("version", PlatformInfoXml.getPlatformInfo().getVersion())
                .addHeader("platform", PlatformInfoXml.getPlatformInfo().getPlatform())
                .addFile("file", "image", file)
                .build()
                .connTimeOut(60 * 1000)
                .readTimeOut(60 * 1000)
                .writeTimeOut(60 * 1000)
                .execute(new Callback<UpImage>() {
                    @Override
                    public UpImage parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, UpImage.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.upload_img_fail));
                        }
                    }

                    @Override
                    public void onResponse(UpImage response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                String message = response.getMsg();
                                if (!TextUtils.isEmpty(message)) {
                                    ToastUtil.showShortToast(Utils.getContext(), message);
                                }
                                if (listener != null) {
                                    listener.onSuccess(response.getImage());
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 删除图片
     *
     * @param imageId   图片id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void deleteImage(String imageId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_DEL_PHOTO)
                .addParams("imgId", imageId)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.del_image_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                String message = response.getMsg();
                                if (!TextUtils.isEmpty(message)) {
                                    ToastUtil.showShortToast(Utils.getContext(), message);
                                }
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 设置图片作为头像
     *
     * @param imageId   图片id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void setAvatar(String imageId, final boolean showToast, final OnCommonListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_SET_HEAD_ICON)
                .addParams("imgId", imageId)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                        if (showToast) {
                            ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.set_to_avatar_fail));
                        }
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                String message = response.getMsg();
                                if (!TextUtils.isEmpty(message)) {
                                    ToastUtil.showShortToast(Utils.getContext(), message);
                                }
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 获取页眉
     *
     * @param listener 请求结果回调
     */
    public static void getHeadMsg(final OnGetHeadMsgListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_HEAD_NOTIFICATION)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("token", PlatformInfoXml.getToken())
                .build()
                .execute(new Callback<HeadMsgNotice>() {

                    @Override
                    public HeadMsgNotice parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, HeadMsgNotice.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                    }

                    @Override
                    public void onResponse(HeadMsgNotice response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess(response);
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 获取新页眉
     *
     * @param lastMsgTime 最后一条未读消息时间毫秒数
     * @param listener    请求结果回调
     */
    public static void getNewHeadMsg(long lastMsgTime, final OnGetNewHeadMsgListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_HEAD_NOTIFICATION2)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("token", PlatformInfoXml.getToken())
                .addParams("lastMsgTime", String.valueOf(lastMsgTime))
                .build()
                .execute(new Callback<NewHeadMsgNotice>() {

                    @Override
                    public NewHeadMsgNotice parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, NewHeadMsgNotice.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                    }

                    @Override
                    public void onResponse(NewHeadMsgNotice response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess(response);
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 获取推荐用户
     *
     * @param cycle    轮询周期（秒）
     * @param listener 请求结果回调
     */
    public static void getRecommendUser(int cycle, final OnGetRecommendUserListener listener) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_RECOMMEND_USER)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("token", PlatformInfoXml.getToken())
                .addParams("type", "0")
                .addParams("cycle", cycle < 20 ? "20" : String.valueOf(cycle))
                .build()
                .execute(new Callback<RecommendUser>() {

                    @Override
                    public RecommendUser parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, RecommendUser.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (listener != null) {
                            listener.onFail();
                        }
                    }

                    @Override
                    public void onResponse(RecommendUser response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (listener != null) {
                                    listener.onSuccess(response);
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }
                    }
                });
    }

    /**
     * 通用监听器
     */
    public interface OnCommonListener {

        /**
         * 请求成功
         */
        void onSuccess();

        /**
         * 请求失败
         */
        void onFail();
    }

    /**
     * 上传用户个人资料监听器
     */
    public interface OnLoadUserInfoListener {

        /**
         * 请求成功
         *
         * @param user 用户信息对象
         */
        void onSuccess(User user);

        /**
         * 请求失败
         */
        void onFail();
    }

    /**
     * 上传图片监听器
     */
    public interface OnUploadImageListener {

        /**
         * 请求成功
         *
         * @param image 上传后图片对象
         */
        void onSuccess(Image image);

        /**
         * 请求失败
         */
        void onFail();
    }

    /**
     * 获取页眉监听器
     */
    public interface OnGetHeadMsgListener {

        /**
         * 请求成功
         *
         * @param headMsgNotice 页眉对象
         */
        void onSuccess(HeadMsgNotice headMsgNotice);

        /**
         * 请求失败
         */
        void onFail();
    }

    /**
     * 获取新页眉监听器
     */
    public interface OnGetNewHeadMsgListener {

        /**
         * 请求成功
         *
         * @param newHeadMsgNotice 新页眉对象
         */
        void onSuccess(NewHeadMsgNotice newHeadMsgNotice);

        /**
         * 请求失败
         */
        void onFail();
    }

    /**
     * 获取推荐用户监听器
     */
    public interface OnGetRecommendUserListener {

        /**
         * 请求成功
         *
         * @param recommendUser 推荐用户对象
         */
        void onSuccess(RecommendUser recommendUser);

        /**
         * 请求失败
         */
        void onFail();
    }

}