package com.yueai.activity;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseTitleActivity;
import com.yueai.constant.IConfigConstant;
import com.yueai.event.CloseMainActivityEvent;
import com.yueai.utils.FileUtil;
import com.yueai.xml.UserInfoXml;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * 设置
 * Created by Administrator on 2016/10/22.
 */
public class SettingActivity extends BaseTitleActivity {
    @BindView(R.id.uid)
    TextView mTvUid;
    @BindView(R.id.setting_email)
    TextView mTvEmail;
    @BindView(R.id.change_pwd)
    LinearLayout mLlChangePwd;
    @BindView(R.id.log_out)
    Button mBtnLogOut;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_set);
    }

    @Override
    protected void initViewsAndVariables() {
        mTvUid.setText(UserInfoXml.getUID());
        mTvEmail.setText(IConfigConstant.E_MAIL);
    }

    @Override
    protected void addListeners() {
        mBtnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 删除用户本地头像
                FileUtil.deleteFile(IConfigConstant.LOCAL_AVATAR_PATH);
                String username = UserInfoXml.getUsername();
                String password = UserInfoXml.getPassword();
                SharedPreferenceUtil.clearXml(SettingActivity.this, UserInfoXml.XML_NAME);
                UserInfoXml.setUsername(username);
                UserInfoXml.setPassword(password);
                // 关闭MainActivity事件
                EventBus.getDefault().post(new CloseMainActivityEvent());
                Util.gotoActivity(SettingActivity.this, LoginActivity.class, true);
            }
        });
        mLlChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(SettingActivity.this, ChangePwdActivity.class, false);
            }
        });
    }

}
