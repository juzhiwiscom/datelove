package com.yueai.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yueai.R;
import com.yueai.base.BaseActivity;

import java.io.File;

import butterknife.BindView;

/**
 * 拍照/从相册选择图片
 * Created by zhangdroid on 2017/03/01.
 */
public class GetPhotoActivity extends BaseActivity {
    @BindView(R.id.photo_root)
    LinearLayout mLlRoot;
    @BindView(R.id.choose_from_album)
    TextView mTvChooseFromAlbum;
    @BindView(R.id.take_photo)
    TextView mTvTakePhoto;

    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    private static final int REQUEST_CODE_TAKE_PHOTO = 0;
    private static final int REQUEST_CODE_CHOOSE_FROM_ALBUM = 1;
    private static final int REQUEST_CODE_CROP = 2;
    private static OnGetPhotoListener mOnGetPhotoListener;

    public interface OnGetPhotoListener {

        /**
         * 返回选择后的图片文件
         *
         * @param file 包含图片的File对象
         */
        void getSelectedPhoto(File file);
    }

    private static final String INTENT_KEY_TEMP_PATH = "intent_key_TEMP_PATH";

    /**
     * 获取本地图片（拍照/从相册选取）
     *
     * @param from     上下文
     * @param listener 图片监听器
     */
    public static void toGetPhotoActivity(Activity from, String tempPath, OnGetPhotoListener listener) {
        mOnGetPhotoListener = listener;
        Intent intent = new Intent(from, GetPhotoActivity.class);
        intent.putExtra(INTENT_KEY_TEMP_PATH, tempPath);
        from.startActivity(intent);
    }

    // 图片临时保存路径
    private String mTempImgPath;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_get_photo;
    }

    @Override
    protected void initViewsAndVariables() {
        Intent intent = getIntent();
        if (intent != null) {
            mTempImgPath = intent.getStringExtra(INTENT_KEY_TEMP_PATH);
        }
    }

    @Override
    protected void addListeners() {
        mTvChooseFromAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏对话框
                mLlRoot.setVisibility(View.GONE);
                // 从相册获取
                Intent chooseIntent = new Intent(Intent.ACTION_PICK);
                chooseIntent.setType("image/*");
                startActivityForResult(chooseIntent, REQUEST_CODE_CHOOSE_FROM_ALBUM);
            }
        });
        mTvTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏对话框
                mLlRoot.setVisibility(View.GONE);
                // 7.0及以上系统需要请求android.permission.READ_EXTERNAL_STORAGE权限，用于转换拍照后路径
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (ContextCompat.checkSelfPermission(GetPhotoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        // 请求android.permission.READ_EXTERNAL_STORAGE权限
                        ActivityCompat.requestPermissions(GetPhotoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE_READ_EXTERNAL_STORAGE);
                        return;
                    }
                }
                // 拍照
                takePhoto();
            }
        });
    }

    private void takePhoto() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // 表示对目标应用临时授权该Uri所代表的文件，7.0及以上
            takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        //设置照片的临时保存路径
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(GetPhotoActivity.this, new File(mTempImgPath)));
        startActivityForResult(takePhotoIntent, REQUEST_CODE_TAKE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_TAKE_PHOTO:// 拍照后裁剪图片
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            cropImage(uri);
                        } else {
                            // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                cropImage(convertImageFile2ContentUri(GetPhotoActivity.this, new File(mTempImgPath)));
                            } else {
                                cropImage(Uri.fromFile(new File(mTempImgPath)));
                            }
                        }
                    } else {
                        // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            cropImage(convertImageFile2ContentUri(GetPhotoActivity.this, new File(mTempImgPath)));
                        } else {
                            cropImage(Uri.fromFile(new File(mTempImgPath)));
                        }
                    }
                    break;

                case REQUEST_CODE_CHOOSE_FROM_ALBUM:// 从相册获取裁剪后的图片
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            cropImage(uri);
                        } else {
                            cropImage(Uri.fromFile(new File(mTempImgPath)));
                        }
                    } else {
                        cropImage(Uri.fromFile(new File(mTempImgPath)));
                    }
                    break;

                case REQUEST_CODE_CROP:// 裁剪后的图片
                    File cropFile = new File(mTempImgPath);
                    if (cropFile != null && cropFile.exists() && mOnGetPhotoListener != null) {
                        mOnGetPhotoListener.getSelectedPhoto(cropFile);
                    }
                    finish();
                    break;
            }
        } else {
            finish();
        }
    }

    /**
     * 裁剪拍照后得到的图片
     */
    private void cropImage(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 设置裁剪框宽高比
        intent.putExtra("aspectX", 3);
        intent.putExtra("aspectY", 4);
        intent.putExtra("scale", true);
        // 不返回data
        intent.putExtra("return-data", false);
        // 设置裁剪后的图片路径
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(mTempImgPath)));
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, REQUEST_CODE_CROP);
    }

    /**
     * 根据file获得Uri（适配7.0及以上）
     *
     * @param file
     */
    private Uri getUriFromFile(Context context, File file) {
        Uri imageUri = null;
        if (context != null && file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // 7.0及以上因为安全问题，需要通过Content Provider封装Uri对象
                imageUri = FileProvider.getUriForFile(this, context.getApplicationContext().getPackageName().concat(".fileprovider"), file);
            } else {
                imageUri = Uri.fromFile(file);
            }
        }
        return imageUri;
    }

    /**
     * <p>将图片路径由file://转换成content://</p>
     * 需要申请android.permission.READ_EXTERNAL_STORAGE权限
     *
     * @param context
     * @param imageFile
     * @return Uri
     */
    private Uri convertImageFile2ContentUri(Context context, File imageFile) {
        if (context != null && imageFile != null) {
            String filePath = imageFile.getAbsolutePath();
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Images.Media._ID},
                    MediaStore.Images.Media.DATA + "=? ",
                    new String[]{filePath}, null);

            if (cursor != null && cursor.moveToFirst()) {
                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
                Uri baseUri = Uri.parse("content://media/external/images/media");
                return Uri.withAppendedPath(baseUri, "" + id);
            } else {
                if (imageFile.exists()) {
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.DATA, filePath);
                    return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                }
            }
        }
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhoto();
            }
        }
    }
}
