package com.yueai.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bm.library.PhotoView;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.anim.DepthPageTransformer;
import com.yueai.base.BaseTitleActivity;
import com.yueai.bean.Image;
import com.yueai.constant.IConfigConstant;
import com.yueai.utils.Utils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 图片浏览器
 * Created by zhangdroid on 2016/11/5.
 */
public class PictureBrowseActivity extends BaseTitleActivity {
    @BindView(R.id.pic_viewpager)
    ViewPager mViewPager;

    private static final String INTENT_KEY_INDEX = "intent_key_INDEX";
    private static final String INTENT_KEY_IMG_URL_LIST = "intent_key_IMG_URL_LIST";

    private int totalCount;

    /**
     * 跳转图片浏览器
     *
     * @param context    上下文对象
     * @param currIndex  当前显示的图片的索引，从0开始
     * @param imgUrlList 所有图片url集合
     */
    public static void toPictureBrowseActivity(Context context, int currIndex, List<Image> imgUrlList) {
        Intent intent = new Intent(context, PictureBrowseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_KEY_INDEX, currIndex);
        intent.putExtra(INTENT_KEY_IMG_URL_LIST, (Serializable) imgUrlList);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_picture_browse;
    }

    @Override
    protected String getCenterTitle() {
        return "0/0";
    }

    @Override
    protected void initViewsAndVariables() {
        Intent intent = getIntent();
        if (intent != null) {
            int currIndex = intent.getIntExtra(INTENT_KEY_INDEX, 0);
            List<Image> list = (List<Image>) intent.getSerializableExtra(INTENT_KEY_IMG_URL_LIST);
            totalCount = list.size();
            List<PhotoView> photoViewList = new ArrayList<PhotoView>();
            if (!Util.isListEmpty(list)) {
                for (Image image : list) {
                    if (image != null) {
                        PhotoView photoView = new PhotoView(PictureBrowseActivity.this);
                        photoView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        // 开启缩放功能
                        photoView.enable();
                        String imageUrl = image.getImageUrl();
                        if (Utils.isHttpUrl(imageUrl)) {
                            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                                    .url(imageUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR).imageView(photoView).build());
                        } else {
                            ImageLoaderUtil.getInstance().loadLocalImage(PictureBrowseActivity.this, photoView, Uri.fromFile(new File(imageUrl)));
                        }
                        photoView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
                        photoViewList.add(photoView);
                    }
                }
            }
            ImagePagerAdapter imagePagerAdapter = new ImagePagerAdapter(photoViewList);
            mViewPager.setAdapter(imagePagerAdapter);
            mViewPager.setPageTransformer(true, new DepthPageTransformer());
            mViewPager.setOffscreenPageLimit(totalCount - 1);
            mViewPager.setCurrentItem(currIndex);
            setTitle((currIndex + 1) + "/" + totalCount);
        }
    }

    @Override
    protected void addListeners() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTitle((position + 1) + "/" + totalCount);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private static class ImagePagerAdapter extends PagerAdapter {
        private List<PhotoView> mPhotoViewList;

        public ImagePagerAdapter(List<PhotoView> list) {
            this.mPhotoViewList = list;
        }

        @Override
        public int getCount() {
            return mPhotoViewList == null ? 0 : mPhotoViewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = mPhotoViewList.get(position);
            container.addView(photoView, photoView.getLayoutParams());
            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

}
