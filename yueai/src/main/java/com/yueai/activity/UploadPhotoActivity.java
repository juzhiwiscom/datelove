package com.yueai.activity;

import android.view.View;
import android.widget.TextView;

import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseTitleActivity;
import com.yueai.bean.Image;
import com.yueai.constant.IConfigConstant;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.xml.UserInfoXml;

import java.io.File;

import butterknife.BindView;

/**
 * Created by Administrator on 2016/12/28.
 */
public class UploadPhotoActivity extends BaseTitleActivity {
    @BindView(R.id.tv_up_photo)
    TextView tv_up_photo;
    @BindView(R.id.skip)
    TextView skip;

    @Override
    protected String getCenterTitle() {
        return getString(R.string.up_headicon);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_upphoto;
    }

    @Override
    protected void initViewsAndVariables() {
        mIvLeft.setVisibility(View.GONE);
    }

    @Override
    protected void addListeners() {
        tv_up_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPhotoActivity.toGetPhotoActivity(UploadPhotoActivity.this, IConfigConstant.LOCAL_AVATAR_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        if (file != null) {
                            showLoading();
                            CommonRequestUtil.uploadImage(true, file, true, new CommonRequestUtil.OnUploadImageListener() {
                                @Override
                                public void onSuccess(Image image) {
                                    dismissLoading();
                                    gotoMainOrUploadAudio();
                                }

                                @Override
                                public void onFail() {
                                    dismissLoading();
                                }
                            });
                        }
                    }
                });
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMainOrUploadAudio();
            }
        });
    }

    private void gotoMainOrUploadAudio() {
        // 女性用户注册
        if (!UserInfoXml.isMale()) {
            Util.gotoActivity(UploadPhotoActivity.this, UploadAudioActivity.class, true);
        } else {
            Util.gotoActivity(UploadPhotoActivity.this, MainActivity.class, true);
        }
    }

}
