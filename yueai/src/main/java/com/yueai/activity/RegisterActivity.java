package com.yueai.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OnSingleDialogClickListener;
import com.library.dialog.OneWheelDialog;
import com.library.utils.DialogUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseFragmentActivity;
import com.yueai.bean.Register;
import com.yueai.constant.IUrlConstant;
import com.yueai.dialog.AgeSelectDialog;
import com.yueai.event.FinishEvent;
import com.yueai.utils.AppsflyerUtils;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.WheelView;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 注册页面
 * Midified by zhangdroid on 2017/03/09.
 */
public class RegisterActivity extends BaseFragmentActivity implements View.OnClickListener {

    @BindView(R.id.register_male)
    ImageView register_male;
    @BindView(R.id.register_female)
    ImageView register_female;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.agreement)
    TextView mTvAgreement;
    @BindView(R.id.tv_sex)
    TextView tvSex;
    @BindView(R.id.tv_age)
    TextView tvAge;
    @BindView(R.id.register)
    TextView register;
    private List<String> mAgeList = new ArrayList<>();
    private String mSelectedText;
    private AgeSelectDialog.OnAgeSelectListener mOnAgeSelectListener;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initViewsAndVariables() {
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        tvAge.setText("24");
        tvSex.setText("男");
    }

    @Override
    protected void addListeners() {
        register_male.setOnClickListener(this);
        register_female.setOnClickListener(this);
        login.setOnClickListener(this);
        mTvAgreement.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register_male:
                DialogUtil.showAgeSelectDialog(getSupportFragmentManager(), getString(R.string.select_age), getString(R.string.register), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        if (selectedText != null) {
                            regist("0", selectedText);
                        }
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.register_female:
                DialogUtil.showAgeSelectDialog(getSupportFragmentManager(), getString(R.string.select_age), getString(R.string.register), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        if (selectedText != null) {
                            regist("1", selectedText);
                        }
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.login:
                Util.gotoActivity(RegisterActivity.this, LoginActivity.class, false);
                break;

            case R.id.agreement:
                DialogUtil.showXiYiSingleBtnDialog(getSupportFragmentManager(), getString(R.string.register_xieyi), getString(R.string.register_xieyi_content), getString(R.string.sure), true, new OnSingleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view) {

                    }
                });
                break;
        }
    }


    public void regist(String gender, String age) {
        showLoading();
        Callback<Register> callback = new Callback<Register>() {
            @Override
            public Register parseNetworkResponse(Response response, int id) throws Exception {
                String resultJson = response.body().string();
                if (!TextUtils.isEmpty(resultJson)) {
                    return JSON.parseObject(resultJson, Register.class);
                }
                return null;
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                dismissLoading();
                ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
            }

            @Override
            public void onResponse(Register response, int id) {
                if (response != null) {
                    String isSucceed = response.getIsSucceed();
                    if (!TextUtils.isEmpty(response.getMsg())) {
                        ToastUtil.showShortToast(RegisterActivity.this, response.getMsg());
                    }
                    if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                        UserInfoXml.setUserInfo(response.getUserPandora());
                        PlatformInfoXml.setToken(response.getToken());
                        // 设置页眉和互推开关
                        UserInfoXml.setIsShowHeadMsg(response.getIsShowHeadNotice());
                        UserInfoXml.setIsShowHeadMsgNew(response.getIsShowHeadNotice2());
                        UserInfoXml.setIsShowRecommendUser(response.getIsShowCommendUser());
                        // appsflyer注册
                        AppsflyerUtils.register();
                        // 初始化数据字典
                        CommonRequestUtil.initParamsDict(new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
                                gotoQA();
                            }

                            @Override
                            public void onFail() {
                                gotoQA();
                            }
                        });
                    } else {
                        dismissLoading();
                        ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
                    }
                } else {
                    dismissLoading();
                    ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
                }
            }
        };
        OkHttpUtils.post()
                .url(IUrlConstant.URL_REGISTER)
                .addParams("gender", gender)
                .addParams("pushToken", PlatformInfoXml.getPushToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("age", age)
                .build()
                .execute(callback);
    }

    private void gotoQA() {
        // 发送事件，finish其它Activity
        EventBus.getDefault().post(new FinishEvent());
        dismissLoading();
        Util.gotoActivity(RegisterActivity.this, AnswerQuestionActivity.class, true);
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tv_sex, R.id.tv_age, R.id.register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_sex:
                showSexDialog();
                break;
            case R.id.tv_age:
                showAgeDialog();
                break;
            case R.id.register:
                    String sex= (String) tvSex.getText();
                    String age=(String) tvAge.getText();
                    if(!(sex.length()>2 || age.length()>2)){
                        if(sex.equals("女")){
                            regist("1",age);
                        }else{
                            regist("0",age);
                        }
                    }else{
                        ToastUtil.showShortToast(this,"请选择性别或者年龄");
                    }
                break;


        }
    }

    private void showAgeDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_select_age,
                null);
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();


        final WheelView wheelView = (WheelView) view.findViewById(R.id.dialog_age_wheelview);
        for (int i = 18; i < 66; i++) {
            mAgeList.add(String.valueOf(i));
        }
        wheelView.setData(mAgeList);
        if (!TextUtils.isEmpty(tvAge.getText().toString())) {
            wheelView.setDefaultIndex(mAgeList.indexOf(tvAge.getText().toString()));
        }
        Button btnSure = (Button) view.findViewById(R.id.dialog_age_sure);
        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              tvAge.setText(wheelView.getSelectedText());
               dialog.dismiss();
            }
        });


        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void showSexDialog() {
        View view = getLayoutInflater().inflate(R.layout.sex_dialog,
                null);
        TextView tvMan = (TextView) view.findViewById(R.id.tv_man);
        TextView tvWoman = (TextView) view.findViewById(R.id.tv_woman);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();
        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        tvMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvSex.setText("男");
                dialog.dismiss();
            }
        });
        tvWoman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvSex.setText("女");
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

}
