package com.yueai.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.utils.DialogUtil;
import com.library.utils.PayUtils;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.library.utils.VersionInfoUtil;
import com.yueai.R;
import com.yueai.base.BaseTitleActivity;
import com.yueai.bean.PayWay;
import com.yueai.bean.User;
import com.yueai.constant.IConfigConstant;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.UpdatePayInfoEvent;
import com.yueai.payUtil.IabHelper;
import com.yueai.payUtil.IabResult;
import com.yueai.payUtil.Purchase;
import com.yueai.utils.AppsflyerUtils;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 支付页面
 * Modified by zhangdroid on 2017/03/09.
 */
public class BuyServiceActivity extends BaseTitleActivity implements View.OnClickListener {
    @BindView(R.id.ll_month)
    LinearLayout ll_month;
    @BindView(R.id.ll_bean)
    LinearLayout ll_bean;
    @BindView(R.id.month1)
    Button month1;
    @BindView(R.id.month3)
    Button month3;
    @BindView(R.id.month12)
    Button month12;
    @BindView(R.id.month1_payall)
    TextView month1_payall;
    @BindView(R.id.month3_payall)
    TextView month3_payall;
    @BindView(R.id.month12_payall)
    TextView month12_payall;
    @BindView(R.id.bean1)
    Button bean1;
    @BindView(R.id.bean2)
    Button bean2;
    @BindView(R.id.bean3)
    Button bean3;
    @BindView(R.id.beans)
    TextView beans;// 豆币数

    private String[] skus = {IConfigConstant.SKU_BEAN300, IConfigConstant.SKU_BEAN600, IConfigConstant.SKU_BEAN1500,
            IConfigConstant.SKU_MONTH1, IConfigConstant.SKU_MONTH3, IConfigConstant.SKU_MONTH12};

    private static final String INTENT_KEY_TYPE = "intent_key_type";
    private static final String INTENT_KEY_FROM_TAG = "intent_key_from_tag";
    private static final String INTENT_KEY_SERVICE_WAY = "intent_key_service_way";
    public static final String INTENT_FROM_INTERCEPT = "intent_key_intercept";
    public static final String INTENT_FROM_BEAN = "intent_key_bean";
    public static final String INTENT_FROM_MONTH = "intent_key_month";

    private IabHelper mHelper;
    private static final int RC_REQUEST = 10001;
    private boolean iap_is_ok = false;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_buy_service;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.recharge);
    }

    /**
     * 跳转支付页面
     *
     * @param from       Activity对象
     * @param sourceTag  支付入口
     * @param serviceWay 支付类型
     */
    public static void toBuyServiceActivity(Activity from, String type, String sourceTag, String serviceWay) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(INTENT_KEY_TYPE, type);
        map.put(INTENT_KEY_FROM_TAG, sourceTag);
        map.put(INTENT_KEY_SERVICE_WAY, serviceWay);
        Util.gotoActivity(from, BuyServiceActivity.class, false, map);
    }

    @Override
    protected void initViewsAndVariables() {
        String type = INTENT_FROM_INTERCEPT;
        String fromTag = "";
        String serviceWay = "-1";
        Intent intent = getIntent();
        if (intent != null) {
            type = intent.getStringExtra(INTENT_KEY_TYPE);
            fromTag = intent.getStringExtra(INTENT_KEY_FROM_TAG);
            serviceWay = intent.getStringExtra(INTENT_KEY_SERVICE_WAY);
        }
        switch (type) {
            case INTENT_FROM_INTERCEPT:// 支付拦截
                ll_bean.setVisibility(View.VISIBLE);
                ll_month.setVisibility(View.VISIBLE);
                beans.setVisibility(View.GONE);
                break;

            case INTENT_FROM_BEAN:// 豆币
                ll_bean.setVisibility(View.VISIBLE);
                ll_month.setVisibility(View.GONE);
                beans.setVisibility(View.VISIBLE);
                beans.setText(getString(R.string.beancounts, String.valueOf(UserInfoXml.getBeanCount())));
                break;

            case INTENT_FROM_MONTH:// 包月
                ll_bean.setVisibility(View.GONE);
                ll_month.setVisibility(View.VISIBLE);
                beans.setVisibility(View.GONE);
                break;
        }

        // 支付统计和支付方式
        getPayWay(fromTag, serviceWay);

        mHelper = new IabHelper(BuyServiceActivity.this, IConfigConstant.GOOGLE_APP_KEY);
        mHelper.enableDebugLogging(false);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    return;
                }
                iap_is_ok = true;
            }
        });
    }

    private void getPayWay(String fromTag, String serviceWay) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_PAY_WAY)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("fromTag", fromTag)
                .addParams("serviceWay", serviceWay)
                .build()
                .execute(new Callback<PayWay>() {
                    @Override
                    public PayWay parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, PayWay.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                    }

                    @Override
                    public void onResponse(PayWay response, int id) {
                    }
                });
    }

    @Override
    protected void addListeners() {
        month1.setOnClickListener(this);
        month3.setOnClickListener(this);
        month12.setOnClickListener(this);
        bean1.setOnClickListener(this);
        bean2.setOnClickListener(this);
        bean3.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bean1:
//                payTest(PlatformInfoXml.getPlatformInfo().getFid() + "101", "1");
                AppsflyerUtils.click300Bean();
                iapHandler.sendEmptyMessage(0);
                break;
            case R.id.bean2:
                AppsflyerUtils.click600Bean();
                iapHandler.sendEmptyMessage(1);
                break;
            case R.id.bean3:
                AppsflyerUtils.click1500Bean();
                iapHandler.sendEmptyMessage(2);
                break;
            case R.id.month1:
                AppsflyerUtils.click1Month();
                iapHandler.sendEmptyMessage(3);
                break;
            case R.id.month3:
                AppsflyerUtils.click3Month();
                iapHandler.sendEmptyMessage(4);
                break;
            case R.id.month12:
                AppsflyerUtils.clickOneYear();
                iapHandler.sendEmptyMessage(5);
                break;
        }
    }

    /**
     * 根据SKU购买商品
     *
     * @param sku 商品对应的SKU
     */
    private void purchase(String sku) {
        if (VersionInfoUtil.checkAPKExist(BuyServiceActivity.this, "com.android.vending") && mHelper != null && iap_is_ok) {
            mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
//            mHelper.launchPurchaseFlow(BuyServiceActivity.this, sku, RC_REQUEST, mPurchaseFinishedListener);
            mHelper.launchSubscriptionPurchaseFlow(BuyServiceActivity.this, sku, RC_REQUEST, mPurchaseFinishedListener);
        } else {
            showMessage(getString(R.string.pay_tip_message));
        }
    }

    Handler iapHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0:
                    purchase(skus[0]);
                    break;
                case 1:
                    purchase(skus[1]);
                    break;
                case 2:
                    purchase(skus[2]);
                    break;
                case 3:
                    purchase(skus[3]);
                    break;
                case 4:
                    purchase(skus[4]);
                    break;
                case 5:
                    purchase(skus[5]);
                    break;
            }
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                return;
            }
            if (purchase.getSku().equals(skus[0])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "101", "2");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[1])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "102", "2");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[2])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "103", "2");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[3])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "002", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[4])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "001", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[5])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "003", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                updateUserInfo();
                if (purchase.getSku().equals(skus[0])) {
                    showMessage(getString(R.string.pay_bean300_success));
                    AppsflyerUtils.purchase300Bean();
                } else if (purchase.getSku().equals(skus[1])) {
                    showMessage(getString(R.string.pay_bean600_success));
                    AppsflyerUtils.purchase600Bean();
                } else if (purchase.getSku().equals(skus[2])) {
                    showMessage(getString(R.string.pay_bean1500_success));
                    AppsflyerUtils.purchase1500Bean();
                } else if (purchase.getSku().equals(skus[3])) {
                    showMessage(getString(R.string.pay_month1_success));
                    AppsflyerUtils.purchase1Month();
                } else if (purchase.getSku().equals(skus[4])) {
                    showMessage(getString(R.string.pay_month3_success));
                    AppsflyerUtils.purchase3Month();
                } else if (purchase.getSku().equals(skus[5])) {
                    showMessage(getString(R.string.pay_month12_success));
                    AppsflyerUtils.purchaseOneYear();
                }
            }
        }
    };

    private void updateUserInfo() {
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                // 更新用户支付信息
                EventBus.getDefault().post(new UpdatePayInfoEvent());
            }

            @Override
            public void onFail() {
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    private void showMessage(String message) {
        DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), getString(R.string.pay_tip), message, getString(R.string.sure), true, null);
    }

    private void pay(final Purchase purchase, final String serviceid, final String payType) {
        OkHttpUtils.post()
                .url(IUrlConstant.GOOGLE_PAY)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("token", PlatformInfoXml.getToken())
                .addParams("transactionId", purchase.getOrderId())
                .addParams("payTime", purchase.getPurchaseTime() + "")
                .addParams("clientTime", TimeUtil.getCurrentTime())
                .addParams("userGoogleId", " ")
                .addParams("payType", payType)
                .addParams("googleId", purchase.getPackageName())
                .addParams("serviceId", serviceid)
                .addParams("userId", UserInfoXml.getUID())
                .addParams("transactionToken", PayUtils.encryptPay(PlatformInfoXml.getPlatformInfo().getPid(), purchase.getOrderId()))
                //后台验证支付和续费使用 具体key 跟后台协商
                .addParams("purchaseToken", purchase.getToken())
                .addParams("packageName", purchase.getPackageName())
                .addParams("productId", purchase.getSku())
                .build()
                .execute(new PayCallback());
    }

   /* private void payTest(final String serviceid, final String payType) {
        OkHttpUtils.post()
                .url(IUrlConstant.GOOGLE_PAY)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("token", PlatformInfoXml.getToken())
                .addParams("transactionId", "GPA.1234-5678-9012-34567")
                .addParams("payTime", System.currentTimeMillis() + "")
                .addParams("clientTime", TimeUtil.getCurrentTime())
                .addParams("userGoogleId", "123")
                .addParams("googleId", "123")
                .addParams("payType", payType)
                .addParams("serviceId", serviceid)
                .addParams("userId", UserInfoXml.getUID())
                .addParams("transactionToken", PayUtils.encryptPay(PlatformInfoXml.getPlatformInfo().getPid(), "GPA.1234-5678-9012-34567"))
                .build()
                .execute(new PayCallback());
    }*/

    public class PayCallback extends StringCallback {

        @Override
        public void onError(Call call, Exception e, int id) {
            ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_fail) + e.toString());
        }

        @Override
        public void onResponse(String response, int id) {
            if (!TextUtils.isEmpty(response) && "success".equals(response)) {
                ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_success));
            } else {
                ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_fail));
            }
        }
    }

}
