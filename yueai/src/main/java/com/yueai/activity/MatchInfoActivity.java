package com.yueai.activity;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OneWheelDialog;
import com.library.dialog.TwoWheelDialog;
import com.library.utils.DialogUtil;
import com.library.utils.HeightUtils;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseTitleActivity;
import com.yueai.bean.MatcherInfo;
import com.yueai.constant.CommonData;
import com.yueai.event.MatchInfoChangeEvent;
import com.yueai.utils.ParamsUtils;
import com.yueai.xml.UserInfoXml;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * 配对条件
 * Created by Administrator on 2016/10/22.
 */
public class MatchInfoActivity extends BaseTitleActivity implements View.OnClickListener {
    @BindView(R.id.meHisAddress)
    LinearLayout meHisAddress;
    @BindView(R.id.meHisAddressT)
    TextView meHisAddressT;
    @BindView(R.id.meHisAge)
    LinearLayout meHisAge;
    @BindView(R.id.meHisAgeT)
    TextView meHisAgeT;
    @BindView(R.id.meHisHeight)
    LinearLayout meHisHeight;
    @BindView(R.id.meHisHeightT)
    TextView meHisHeightT;
    @BindView(R.id.meHisEducation)
    LinearLayout meHisEducation;
    @BindView(R.id.meHisEducationT)
    TextView meHisEducationT;
    @BindView(R.id.meHisIncome)
    LinearLayout meHisIncome;
    @BindView(R.id.meHisIncomeT)
    TextView meHisIncomeT;
    @BindView(R.id.save)
    Button save;

    private MatcherInfo matcherInfo;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_match_info;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_match);
    }

    @Override
    protected void initViewsAndVariables() {
        if (UserInfoXml.getMatchInfo() != null) {
            matcherInfo = JSON.parseObject(UserInfoXml.getMatchInfo(), MatcherInfo.class);
        } else {
            matcherInfo = new MatcherInfo();
        }
        if (matcherInfo != null) {
            if (matcherInfo.getArea() != null && matcherInfo.getArea().getProvinceName() != null) {
                meHisAddressT.setText(Util.convertText(matcherInfo.getArea().getProvinceName()));
            }
            if (matcherInfo.getMinAge() != 0 && matcherInfo.getMaxAge() != 0) {
                meHisAgeT.setText(matcherInfo.getMinAge() + getString(R.string.age) + " - " + matcherInfo.getMaxAge() + getString(R.string.age));
            }
            if (matcherInfo.getMinHeightCm() != 0 && matcherInfo.getMaxHeightCm() != 0) {
                meHisHeightT.setText(HeightUtils.getInchCmByCm(String.valueOf(matcherInfo.getMinHeightCm())) + " - " + HeightUtils.getInchCmByCm(String.valueOf(matcherInfo.getMaxHeightCm())));
            }
            if (matcherInfo.getMinimumEducation() != 0) {
                meHisEducationT.setText(ParamsUtils.getEducationMap().get(String.valueOf(matcherInfo.getMinimumEducation())));
            }
            if (matcherInfo.getIncome() != 0) {
                meHisIncomeT.setText("NT$" + ParamsUtils.getIncomeMap().get(String.valueOf(matcherInfo.getIncome())));
            }
        }
    }

    @Override
    protected void addListeners() {
        meHisAddress.setOnClickListener(this);
        meHisAge.setOnClickListener(this);
        meHisHeight.setOnClickListener(this);
        meHisEducation.setOnClickListener(this);
        meHisIncome.setOnClickListener(this);
        save.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.meHisAddress:
                String address = meHisAddressT.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), CommonData.getProvinceList(), TextUtils.isEmpty(address) ? 0 : CommonData.getProvinceList().indexOf(address),
                        getString(R.string.matchAddress), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (selectedText != null) {
                                    meHisAddressT.setText(selectedText);
                                    matcherInfo.getArea().setProvinceName(selectedText);
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.meHisAge:
//                DialogUtil.showTwoAgeSelectDialog(getSupportFragmentManager(), getString(R.string.matchAge), getString(R.string.sure),
//                        getString(R.string.cancel), false, new TwoWheelDialog.OnTwoWheelDialogClickListener() {
//                            @Override
                int minAge = matcherInfo.getMinAge();
                int maxAge = matcherInfo.getMaxAge();
                DialogUtil.showTwoAgeSelectDialog(minAge,maxAge,getSupportFragmentManager(), getString(R.string.matchAge), getString(R.string.positive),
                        getString(R.string.negative), false, new TwoWheelDialog.OnTwoWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText1, String selectedText2) {
                                if (!TextUtils.isEmpty(selectedText1) && !TextUtils.isEmpty(selectedText2)) {
                                    int minAge = Integer.parseInt(selectedText1);
                                    int maxAge = Integer.parseInt(selectedText2);
                                    if (minAge > maxAge) {
                                        matcherInfo.setMinAge(maxAge);
                                        matcherInfo.setMaxAge(minAge);
                                        meHisAgeT.setText(selectedText2 + getString(R.string.age) + " - " + selectedText1 + getString(R.string.age));
                                    } else {
                                        matcherInfo.setMinAge(minAge);
                                        matcherInfo.setMaxAge(maxAge);
                                        meHisAgeT.setText(selectedText1 + getString(R.string.age) + " - " + selectedText2 + getString(R.string.age));
                                    }
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.meHisHeight:
//                DialogUtil.showTwoHeightSelectDialog(getSupportFragmentManager(), getString(R.string.matchHeight), getString(R.string.sure),
//                        getString(R.string.cancel), false, new TwoWheelDialog.OnTwoWheelDialogClickListener() {
//                            @Override
                String minHeight =String.valueOf(matcherInfo.getMinHeightCm());
                String maxHeight = String.valueOf(matcherInfo.getMaxHeightCm());

                DialogUtil.showTwoHeightSelectDialog(minHeight,maxHeight,getSupportFragmentManager(), getString(R.string.matchHeight), getString(R.string.positive),
                        getString(R.string.negative), false, new TwoWheelDialog.OnTwoWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText1, String selectedText2) {
                                meHisHeightT.setText(HeightUtils.getCmByInchCm(selectedText1));
                                if (Integer.parseInt(HeightUtils.getCmByInchCm(selectedText1)) > Integer.parseInt(HeightUtils.getCmByInchCm(selectedText2))) {
                                    meHisHeightT.setText(selectedText2 + " - " + selectedText1);
                                    matcherInfo.setMinHeightCm(Integer.parseInt(HeightUtils.getCmByInchCm(selectedText2)));
                                    matcherInfo.setMaxHeightCm(Integer.parseInt(HeightUtils.getCmByInchCm(selectedText1)));
                                } else {
                                    meHisHeightT.setText(selectedText1 + " - " + selectedText2);
                                    matcherInfo.setMinHeightCm(Integer.parseInt(HeightUtils.getCmByInchCm(selectedText1)));
                                    matcherInfo.setMaxHeightCm(Integer.parseInt(HeightUtils.getCmByInchCm(selectedText2)));

                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.meHisEducation:
                String education = meHisEducationT.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getEducationMapValue(), TextUtils.isEmpty(education) ? 0 : ParamsUtils.getEducationMapValue().indexOf(education),
                        getString(R.string.matchEducation), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (selectedText != null) {
                                    meHisEducationT.setText(selectedText);
                                    matcherInfo.setMinimumEducation(Integer.parseInt(ParamsUtils.getEducationMapKeyByValue(selectedText)));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.meHisIncome:
                String income = meHisIncomeT.getText().toString();
                if (!TextUtils.isEmpty(income) && income.contains("NT$")) {
                    income = income.replace("NT$", "");
                }
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getIncomeMapValue(), TextUtils.isEmpty(income) ? 0 : ParamsUtils.getIncomeMapValue().indexOf(income),
                        getString(R.string.matchIncome), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (selectedText != null) {
                                    meHisIncomeT.setText("NT$" + selectedText);
                                    matcherInfo.setIncome(Integer.parseInt(ParamsUtils.getIncomeMapKeyByValue(selectedText)));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.save:
                UserInfoXml.setMatchInfo(JSON.toJSONString(matcherInfo));
                finish();
                // 延时发送事件
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new MatchInfoChangeEvent());
                    }
                }, 200);
                break;
        }
    }

}
