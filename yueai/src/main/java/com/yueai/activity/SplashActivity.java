package com.yueai.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.appsflyer.AppsFlyerLib;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.library.dialog.OnSingleDialogClickListener;
import com.library.utils.DensityUtil;
import com.library.utils.DialogUtil;
import com.library.utils.LogUtil;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.library.utils.VersionInfoUtil;
import com.yueai.R;
import com.yueai.base.BaseFragmentActivity;
import com.yueai.bean.BaseModel;
import com.yueai.bean.CheckRefresh;
import com.yueai.bean.LocationInfo;
import com.yueai.bean.LoginEntity;
import com.yueai.bean.PlatformInfo;
import com.yueai.constant.IConfigConstant;
import com.yueai.constant.IUrlConstant;
import com.yueai.utils.AppsflyerUtils;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.GoogleUtil;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/18.
 */
public class SplashActivity extends BaseFragmentActivity {
    @BindView(R.id.iv_yueai)
    ImageView ivYueai;
    private String UPDATE_SERVERAPK = "YueAi";
    private LocationManager mLocationManager;
    private static final int PERMISSION_CODE_ACCESS_FINE_LOCATION = 0;
    private boolean isFirstInto=true;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initViewsAndVariables() {
        // 百度推送service初始化及绑定
        PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY, IConfigConstant.BAIDU_PUSH_API_KEY);
        // AppsFlyer初始化
        AppsFlyerLib.getInstance().setAndroidIdData(getAndroidId());
        AppsFlyerLib.getInstance().startTracking(this.getApplication(), IConfigConstant.APPSFLYER_DEV_KEY);
        // 获得platform信息
        getPlatformInfo();
//        // 获取位置信息并上传
//        requestLocation();
        init();
//        anim();
    }

    private void anim() {
        //准备动画
        AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
        alphaAnimation.setDuration(2000);   //2秒钟内，透明的0-1

        ScaleAnimation scaleAnimation = new ScaleAnimation(0, 1, 0, 1,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(2000);	//2秒钟内，由没有到有

        RotateAnimation rotateAnimation = new RotateAnimation
                (0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(2000);	// 2秒钟内旋转结束

        //动画集合，让上诉三个动画一起执行shareInterpolator		true 共享插值器 数学曲线		false  不共享插值器，不共享数学曲线
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);

        ivYueai.startAnimation(animationSet);

        //监听动画结束后，跳转到后一个界面（GuideActivity（提示用户项目有什么功能界面，应用程序的主界面））
        animationSet.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                //动画开始时候调用方法
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //动画重复时候调用方法

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //动画结束时候调用方法
                //数据的存储（文件存储（sp  数据库	 File（sd卡  files文件夹   cache文件））
                //内存存储（List（变量，集合））
                //网络存储（数据在服务器端））

//
//                //SplashActivity.this		创建对话框的时候,需要上下环境,一定要用activity.this
//                //getApplicationContext()
//                Boolean isFirst = SharePreUtil.getBoolean(getApplicationContext(), ConstantValue.ISFIRST, true);
//                if(isFirst){
//                    //进入包含了viewpager那个导航界面
//                    Intent intent = new Intent(getApplicationContext(), GuideActivity.class);
//                    startActivity(intent);
//                    //将isFirst改为false,并且在本地持久化
//                    SharePreUtil.saveBoolean(getApplicationContext(), ConstantValue.ISFIRST, false);
//                }else{
//                    //进入应用程序主界面
//                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                    startActivity(intent);
//                }
//                finish();
                if (!UserInfoXml.getIsClient()) {
                    activation();
                }
                if (UserInfoXml.getUsername() != null && UserInfoXml.getPassword() != null) {
                    checkRefresh();
                } else {
                    Util.gotoActivity(SplashActivity.this, RegisterActivity.class, true);
                }
            }
        });
    }

    @Override
    protected void addListeners() {
    }

    protected void init() {
            if (!UserInfoXml.getIsClient()) {
                activation();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (UserInfoXml.getUsername() != null && UserInfoXml.getPassword() != null) {
                        checkRefresh();
                    } else {
                        Util.gotoActivity(SplashActivity.this, RegisterActivity.class, true);
                    }
                }
            }, 2000);
    }

    public void checkRefresh() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_CHECK_UPDATE)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new RefreshCallBack());
    }

    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    public class RefreshCallBack extends Callback<CheckRefresh> {
        @Override
        public CheckRefresh parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, CheckRefresh.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            login();
        }

        @Override
        public void onResponse(CheckRefresh response, int id) {
            if (response != null) {
                if (response.isUpdate == null || response.isUpdate.equals("0")) {
                    login();
                } else {
                    DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), "更新", response.updateInfo, getString(R.string.sure), false, new OnSingleDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view) {
                            pd = new ProgressDialog(SplashActivity.this);
                            pd.setTitle("正在下載");
                            pd.setCanceledOnTouchOutside(false);
                            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            downFile("http://iospan.ilove.ren:8092/apk/" + getString(R.string.app_name) + ".apk");
                        }
                    });
                }
            }
        }
    }

    /**
     * 下载apk
     */
    public void downFile(final String url) {
        pd.show();
        OkHttpUtils.get()
                .url(url)
                .build()
                .execute(new FileCallBack(getExternalFilesDir(null).getPath() + File.separator, UPDATE_SERVERAPK) {
                    @Override
                    public void inProgress(float progress, long total, int id) {
                        super.inProgress(progress, total, id);
                        pd.setProgress((int) (progress * 100));
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        pd.cancel();
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        pd.cancel();
                        update();
                    }
                });
    }

    /**
     * 安装应用
     */
    public void update() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(getExternalFilesDir(null).getPath() + File.separator, UPDATE_SERVERAPK)),
                "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void login() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_LOGIN)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pushToken", PlatformInfoXml.getPushToken())
                .addParams("account", UserInfoXml.getUsername())
                .addParams("password", UserInfoXml.getPassword())
                .build()
                .execute(new LoginCallBack());
    }

    public class LoginCallBack extends Callback<LoginEntity> {
        @Override
        public LoginEntity parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, LoginEntity.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            Util.gotoActivity(SplashActivity.this, LoginActivity.class, true);
        }

        @Override
        public void onResponse(final LoginEntity response, int id) {
            if (response != null) {
                if (!TextUtils.isEmpty(response.getMsg())) {
                    ToastUtil.showShortToast(SplashActivity.this, response.getMsg());
                }
                String isSucceed = response.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                    PlatformInfoXml.setToken(response.getToken());
                    UserInfoXml.setUserInfo(response.getUserEnglish());
                    // 设置页眉和互推开关
                    UserInfoXml.setIsShowHeadMsg(response.getIsShowHeadNotice());
                    UserInfoXml.setIsShowHeadMsgNew(response.getIsShowHeadNotice2());
                    UserInfoXml.setIsShowRecommendUser(response.getIsShowCommendUser());
                    // appsflyer登录
                    AppsflyerUtils.login();
                    CommonRequestUtil.initParamsDict(new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {
                            if(isFirstInto){
                                isFirstInto=false;
                                if (response.getIsShowUploadImg() != null && response.getIsShowUploadImg().equals("1")) {
                                    Util.gotoActivity(SplashActivity.this, UploadPhotoActivity.class, true);
                                } else {
                                    Util.gotoActivity(SplashActivity.this, MainActivity.class, true);
                                }
                            }

                        }

                        @Override
                        public void onFail() {
                            if(isFirstInto) {
                                isFirstInto = false;
                                if (response.getIsShowUploadImg() != null && response.getIsShowUploadImg().equals("1")) {
                                    Util.gotoActivity(SplashActivity.this, UploadPhotoActivity.class, true);
                                } else {
                                    Util.gotoActivity(SplashActivity.this, MainActivity.class, true);
                                }
                            }

                        }
                    });
                } else {
                    if(isFirstInto) {
                        isFirstInto = false;
                        Util.gotoActivity(SplashActivity.this, LoginActivity.class, true);
                    }
                }
            } else {
                if(isFirstInto) {
                    isFirstInto = false;
                    Util.gotoActivity(SplashActivity.this, LoginActivity.class, true);
                }
            }
        }
    }

    protected void activation() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_CLENT_ACTIVATION)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                UserInfoXml.setIsClient();
                                // appsflyer激活
                                AppsflyerUtils.activate();
                            }
                        }
                    }
                });
    }

    private void getPlatformInfo() {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setW(String.valueOf(DensityUtil.getScreenWidth(SplashActivity.this)));
        platformInfo.setH(String.valueOf(DensityUtil.getScreenHeight(SplashActivity.this)));
        platformInfo.setVersion(String.valueOf(VersionInfoUtil.getVersionCode(getApplicationContext())));
        platformInfo.setPhonetype(Build.MODEL);
        platformInfo.setPid(getAndroidId());
        platformInfo.setImsi(getAndroidId());
        platformInfo.setProduct(IConfigConstant.PRODUCT_ID);// 产品号
        platformInfo.setFid(IConfigConstant.F_ID);// 渠道号
        platformInfo.setCountry("Taiwan");
        platformInfo.setLanguage("Traditional");
        platformInfo.setNetType(getNetType());
        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(TimeUtil.getCurrentTime());
        platformInfo.setPlatform("2");
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
        // 保存platform信息
        PlatformInfoXml.setPlatfromInfo(platformInfo);
    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getNetType() {
        String type = "";
        ConnectivityManager cm = (ConnectivityManager) SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }

    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

//    /**
//     * 请求定位信息
//     */
//    private void requestLocation() {
//        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (ContextCompat.checkSelfPermission(SplashActivity.this,
//                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
//            // 请求android.permission.ACCESS_FINE_LOCATION
//            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
//        } else {
//            if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
//                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
//            } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
//                uploadLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
//            }
//        }
//    }
//
//    private LocationListener mLocationListener = new LocationListener() {
//
//        @Override
//        public void onLocationChanged(Location location) {
//            uploadLocation(location);
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//        }
//    };
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == PERMISSION_CODE_ACCESS_FINE_LOCATION) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {// 申请权限成功
//                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
//                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
//                            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
//                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                        // TODO: Consider calling
//                        //    ActivityCompat#requestPermissions
//                        // here to request the missing permissions, and then overriding
//                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                        //                                          int[] grantResults)
//                        // to handle the case where the user grants the permission. See the documentation
//                        // for ActivityCompat#requestPermissions for more details.
//                        return;
//                    }
//                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
//                } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
//                    uploadLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
//                }else{
//                    init();
//                }
//            }else{
//                init();
//            }
//        }else{
//            init();
//        }
//    }
//
//    /**
//     * 上传位置信息
//     *
//     * @param location 定位成功后的信息
//     */
//    private void uploadLocation(Location location) {
//        if (location != null) {
//            String getLatitude = String.valueOf(location.getLatitude());
//            String getLongitude = String.valueOf(location.getLongitude());
//            String country = UserInfoXml.getCountry();
//            Map<String, String> googleAddress = GoogleUtil.getGoogleAddress(getLatitude, getLongitude, "zh-TW", "Taiwan");
//            String province = googleAddress.get("province");
//            if(!TextUtils.isEmpty(province)){
//                if (!province.equals(UserInfoXml.getProvinceName())) {
//                    UserInfoXml.setProvinceName(province);
//                }
//            }
//            LocationInfo locationInfo = new LocationInfo();
//            locationInfo.setAddrStr("");
//            locationInfo.setProvince(province);
//            locationInfo.setCity("");
//            locationInfo.setCityCode("");
//            locationInfo.setDistrict("");
//            locationInfo.setStreet("");
//            locationInfo.setStreetNumber("");
//            locationInfo.setLatitude(getLatitude);
//            locationInfo.setLongitude(getLongitude);
//            Map<String, LocationInfo> map = new HashMap<String, LocationInfo>();
//            map.put("locationInfo", locationInfo);
//            OkHttpUtils.post()
//                    .url(IUrlConstant.URL_UPLOAD_LOCATION)
////                    .addHeader("token", PlatformInfoXml.getToken())
//                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                    .addParams("locationInfo", JSON.toJSONString(map))
//                    .build()
//                    .execute(new Callback<BaseModel>() {
//                        @Override
//                        public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
//                            String resultJson = response.body().string();
//                            if (!TextUtils.isEmpty(resultJson)) {
//                                return JSON.parseObject(resultJson, BaseModel.class);
//                            }
//                            return null;
//                        }
//
//                        @Override
//                        public void onError(Call call, Exception e, int id) {
//                            init();
//
//                        }
//
//                        @Override
//                        public void onResponse(BaseModel response, int id) {
//                            if (response != null) {
//                                String isSucceed = response.getIsSucceed();
//                                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {// 上传成功
//                                    LogUtil.i(LogUtil.TAG_ZL, "上传位置信息成功");
//                                    init();
//                                }
//                            }
//                        }
//                    });
//        }else{
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    init();
//                }
//            },5000);
//        }
//    }

}
