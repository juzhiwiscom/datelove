package com.yueai.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.yueai.utils.Utils;

import butterknife.ButterKnife;

/**
 * Fragment基类
 * Created by zhangdroid on 2016/5/25.
 */
public abstract class BaseFragment extends Fragment {
    private View mRootView;
    /**
     * 进度加载对话框
     */
    private Dialog mLoadingDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doRegister();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            if (getLayoutResId() > 0) {
                mRootView = inflater.inflate(getLayoutResId(), container, false);
            } else {
                return null;
            }
        }
        ButterKnife.bind(this, mRootView);
        initViewsAndVariables();
        addListeners();
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregister();
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    protected abstract void initViewsAndVariables();

    protected abstract void addListeners();

    /**
     * 注册/初始化receiver或第三方库
     */
    protected void doRegister() {
    }

    /**
     * 注销/释放相关资源
     */
    protected void unregister() {
    }


    //********************************************** 公用方法 **********************************************//

    protected void showNoCancelLoading() {
        mLoadingDialog = Utils.showLoadingDialog(getActivity(), false);
    }

    /**
     * 显示加载对话框
     */
    protected void showLoading() {
        mLoadingDialog = Utils.showLoadingDialog(getActivity(), true);
    }

    /**
     * 隐藏加载对话框
     */
    protected void dismissLoading() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }

}
