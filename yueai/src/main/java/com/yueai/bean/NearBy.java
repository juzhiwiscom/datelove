package com.yueai.bean;

import java.util.List;

/**
 * 附近的人列表对象
 * Created by zhangdroid on 2016/6/29.
 */
public class NearBy extends BaseModel {
    private int pageNum;// 当前页码
    private List<NearByUser> listUser;// 附近的人用户对象

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public List<NearByUser> getListUser() {
        return listUser;
    }

    public void setListUser(List<NearByUser> listUser) {
        this.listUser = listUser;
    }

    @Override
    public String toString() {
        return "NearBy{" +
                "pageNum=" + pageNum +
                ", listUser=" + listUser +
                '}';
    }

}
