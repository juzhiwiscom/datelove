package com.yueai.bean;

/**
 * Created by Administrator on 2017/1/10.
 */

public class MarkPhoto {
    public String id;
    public String imageUrl;
    public String score;
    public String add_time;
    public String update_time;
    public String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MarkPhoto{" +
                "id='" + id + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", score='" + score + '\'' +
                ", add_time='" + add_time + '\'' +
                ", update_time='" + update_time + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
