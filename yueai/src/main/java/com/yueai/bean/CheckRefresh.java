package com.yueai.bean;

/**
 * Created by jzrh on 2016/7/15.
 */
public class CheckRefresh {
    public String isUpdate;
    public String type;
    public String updateInfo;
    public String newVersionCode;
    public String releaseDate;

    public String getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNewVersionCode() {
        return newVersionCode;
    }

    public void setNewVersionCode(String newVersionCode) {
        this.newVersionCode = newVersionCode;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getUpdateInfo() {
        return updateInfo;
    }

    public void setUpdateInfo(String updateInfo) {
        this.updateInfo = updateInfo;
    }

    @Override
    public String toString() {
        return "CheckRefresh{" +
                "isUpdate='" + isUpdate + '\'' +
                ", type='" + type + '\'' +
                ", updateInfo='" + updateInfo + '\'' +
                ", newVersionCode='" + newVersionCode + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                '}';
    }
}
