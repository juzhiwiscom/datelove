package com.yueai.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/10/29.
 */

public class SeeMeList extends BaseModel {
    private String totalCount;
    private String pageNum;
    private String pageSize;
    private List<SeeMe> seeMeList;

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public List<SeeMe> getSeeMeList() {
        return seeMeList;
    }

    public void setSeeMeList(List<SeeMe> seeMeList) {
        this.seeMeList = seeMeList;
    }

    @Override
    public String toString() {
        return "SeeMeList{" +
                "totalCount='" + totalCount + '\'' +
                ", pageNum='" + pageNum + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", seeMeList=" + seeMeList +
                '}';
    }

}
