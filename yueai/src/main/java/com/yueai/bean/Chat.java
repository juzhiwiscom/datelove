package com.yueai.bean;

/**
 * 聊天记录对象
 * Created by zhangdroid on 2016/8/23.
 */
public class Chat {
    private long id;
    private ChatMsg chat;// 聊天消息
    private UserBase userBaseEnglish;// 聊天用户信息

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ChatMsg getChat() {
        return chat;
    }

    public void setChat(ChatMsg chat) {
        this.chat = chat;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", chat=" + chat +
                ", userBaseEnglish=" + userBaseEnglish +
                '}';
    }

}
