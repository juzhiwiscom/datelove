package com.yueai.constant;

import android.os.Environment;

import com.yueai.R;
import com.yueai.base.BaseApplication;

import java.io.File;

/**
 * 全局参数，统一配置
 * Created by zhangdroid on 2016/6/22.
 */
public interface IConfigConstant {
    /**
     * appsFlyer dev_key
     */
    String APPSFLYER_DEV_KEY = "7EhHiNjd6ef8TGUyZSGzF3";
    /**
     * 百度推送api_key
     */
    String BAIDU_PUSH_API_KEY = "cIcNUTGxXTFu8csiGlxhDZ7H";

    /**
     * Google pay app key
     */
    String GOOGLE_APP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm8+/JSkXyD7/YasXpQN44bnJhrLgSSNvkWpCmylBInoLOg5OkQQSlemSGfINZpxJzuvuit2u2jkJF0F3uAgvqv4s/r8xmCbg2HbfBavYCS97eVWC0ZNXY5H7E1GvABmQxi2GqVDVX/yHdqGvErofILadbvQMXvkeXnjv+970b7UR7gCQvEBHf7nw7wDLr2R9SdNEII0Is1xDtP8nB85+voJghlcm1g/XsHodItNm4q6IO/zvgEwWHSNsaUjpy/AeW6uCQqflGp1wHxWzdncvULMFDTtDDjEd5TmwRes+gNi9pOmr5V0EkKasBp+UcLcNJlHrsTnEYYQiZ1fMpdpF2wIDAQAB";
    /**
     * 产品号
     */
    String PRODUCT_ID = "08";

    /**
     * 渠道号
     */
    String F_ID = "10808";

    /**
     * 客服邮箱
     */
    String E_MAIL = "Dating1420@yahoo.com";

    // Google支付SKU
    String SKU_BEAN300 = "bean1";
    String SKU_BEAN600 = "bean2";
    String SKU_BEAN1500 = "bean3";
    String SKU_MONTH1 = "onemonth";
    String SKU_MONTH3 = "threemonths";
    String SKU_MONTH12 = "oneyear";

    /**
     * 支付来源：用户详情页
     */
    String PAY_TAG_FROM_DETAIL = "2";
    /**
     * 支付来源：聊天页
     */
    String PAY_TAG_FROM_CHAT = "3";
    /**
     * 支付来源：弹窗
     */
    String PAY_TAG_FROM_POPUP = "6";
    /**
     * 支付入口：包月
     */
    String PAY_WAY_MONTH = "1";
    /**
     * 支付入口：豆币
     */
    String PAY_WAY_BEAN = "0";
    /**
     * 支付入口: URL
     */
    String PAY_WAY_ALL = "-1";

    /**
     * /**
     * 本地头像保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_AVATAR_PATH = BaseApplication.getGlobalContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            + File.separator + "avatar.jpg";

    /**
     * 本地图片保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_PIC_PATH = BaseApplication.getGlobalContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            + File.separator + "photo.jpg";

    /**
     * 全局默认圆角头像
     */
    int COMMON_DEFAULT_AVATAR = R.drawable.img_default;
    /**
     * 全局默认圆形头像
     */
    int COMMON_DEFAULT_AVATAR_CIRCLE = R.drawable.img_default_circle;

}
