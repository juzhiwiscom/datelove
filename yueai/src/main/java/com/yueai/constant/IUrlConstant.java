package com.yueai.constant;

/**
 * 所有接口常量
 * <p>
 * 1、该接口中配置所有接口；
 * 2、接口配置时请放入相应模块注释下方，并写明接口作用，统一管理
 * 3、接口命名规则请参照：URL_模块名_接口名.
 * </p>
 * Created by zhangdroid on 2016/8/6.
 */
public interface IUrlConstant {

    //***************************** 正式服 ***************************/

    /**
     * Google支付
     */
    String GOOGLE_PAY = "http://andpanpay.ilove.ren:8093/pandorapaycenter/totalBack/getGooglePay.pay";
    String URL_BASE = "http://andpan.ilove.ren:8092/loveapk_english";

    //***************************** 测试服 ***************************/

    /**
     * Google支付
     */
//    String GOOGLE_PAY = "http://47.89.208.14:10093/paycenter/totalBack/getIAPInApplePay.pay";
//    String URL_BASE = "http://47.89.208.14:10092/loveapk_english";
//    String URL_BASE = "http://192.168.1.100:8080/loveapk"; // 如宝本地
//    String URL_BASE = "http://192.168.199.215:8090/loveapk"; // 希磊本地


    //***************************** 公用（注册/登陆） ***************************/


    /**
     * 激活客户端
     */
    String URL_CLENT_ACTIVATION = URL_BASE + "/sys/clientActivation.json";
    /**
     * 初始化数据字典
     */
    String URL_INIT = URL_BASE + "/sys/init.json";
    /**
     * 普通注册
     */
    String URL_REGISTER = URL_BASE + "/user/register.json";
    /**
     * 登陆
     */
    String URL_LOGIN = URL_BASE + "/user/login.json";
    /**
     * 检查更新
     */
    String URL_CHECK_UPDATE = URL_BASE + "/setting/checkVersion.json";
    /**
     * 获取支付方式（网页支付/本地支付）
     */
    String URL_PAY_WAY = URL_BASE + "/pay/payWay.json";
    /**
     * 获取页眉
     */
    String URL_HEAD_NOTIFICATION = URL_BASE + "/msg/getHeadMenuNotice.json";
    /**
     * 获取新页眉
     */
    String URL_HEAD_NOTIFICATION2 = URL_BASE + "/msg/getHeadNotice2.json";
    /**
     * 获取推荐用户
     */
    String URL_GET_RECOMMEND_USER = URL_BASE + "/msg/getRecommendUser.json";
    /**
     * 上传位置信息
     */
    String URL_UPLOAD_LOCATION = URL_BASE + "/sys/uploadLocation.json";
    /**
     * 修改密码
     */
    String URL_MODIFY_PWD = URL_BASE + "/setting/modifyPwd.json";
    /**
     * 找回密码
     */
    String URL_FIND_PWD = URL_BASE + "/user/getPwd.json";
    /**
     * 上傳自我介紹语音消息
     */
    String URL_UPLOAD_INTRODUCE_VOICE = URL_BASE + "/audio/uploadRegAudio.json";
    /**
     * 测试图片
     */
    String URL_TESTIMAGE = URL_BASE + "/photo/getMarkPhotos.json";
    /**
     * 上传测试图片
     */
    String URL_UP_TESTIMAGE = URL_BASE + "/photo/markPhotos.json";


    //***************************** 缘分 ***************************/

    /**
     * 缘分
     */
    String URL_FATE = URL_BASE + "/search/getYuanfen.json";


    //***************************** 搜索 ***************************/

    /**
     * 根据条件搜索用户
     */
    String URL_SEARCH = URL_BASE + "/search/searchUser.json";


    //***************************** 信箱 ***************************/
    /**
     * 获取所有的聊天列表（聊天（不论是否发过消息）、配对）
     */
    String URL_GET_ALL_CHAT_LIST = URL_BASE + "/msg/getChatListNew.json";
    /**
     * 获取聊天信箱列表(只有消息记录的列表)
     */
    String URL_GET_MSGBOX_LIST = URL_BASE + "/msg/getMsgBoxList.json";
    /**
     * 获取聊天内容列表（查询一定时间段内）
     */
    String URL_GET_CHAT_MSG = URL_BASE + "/msg/getMsgListByTime.json";
    /**
     * 删除聊天记录
     */
    String URL_DEL_CHAT_HISOTRY = URL_BASE + "/msg/deleteMsg.json";
    /**
     * 发送文字消息
     */
    String URL_SEND_TEXT_MSG = URL_BASE + "/msg/writeMsg.json";
    /**
     * 发送语音消息
     */
    String URL_SEND_VOICE_MSG = URL_BASE + "/msg/sendAudioMsg.json";
    /**
     * 更新语音消息状态
     */
    String URL_UPDATE_VOICE_MSG_STATUS = URL_BASE + "/msg/updateAudioMsgStatus.json";
    /**
     * 发送图片消息
     */
    String URL_SEND_PHOTO_MSG = URL_BASE + "/msg/sendChatPhoto.json";


    //***************************** 附近 ***************************/

    /**
     * 附近的人
     */
    String URL_NEARBY = URL_BASE + "/nearby/getNearbyUser.json";


    //***************************** 我的 ***************************/

    /**
     * 获取登陆用户个人信息
     */
    String URL_USER_INFO = URL_BASE + "/space/myInfo.json";
    /**
     * 修改用户个人信息
     */
    String URL_UPLOAD_USER_INFO = URL_BASE + "/setting/uploadMyInfoEnglish.json";
    /**
     * 设置为头像
     */
    String URL_SET_HEAD_ICON = URL_BASE + "/photo/setHeadImg.json";
    /**
     * 上传图片
     */
    String URL_UPLOAD_PHOTO = URL_BASE + "/photo/uploadImg.json";
    /**
     * 删除照片
     */
    String URL_DEL_PHOTO = URL_BASE + "/photo/deleteImg.json";
    /**
     * 获取我关注的用户列表
     */
    String URL_GET_FOLLOW_LIST = URL_BASE + "/user/getFollowUser.json";
    /**
     * 获取最近访客列表
     */
    String URL_GET_GUEST_LIST = URL_BASE + "/space/getSeeMeList.json";


    //***************************** 用户信息（对方空间页） ***************************/

    /**
     * 获取某个用户详情
     */
    String URL_GET_USER_INFO = URL_BASE + "/space/userInfo.json";
    /**
     * 打招呼
     */
    String URL_SAY_HELLO = URL_BASE + "/msg/sayHello.json";
    /**
     * 关注
     */
    String URL_FOLLOW = URL_BASE + "/space/follow.json";
    /**
     * 取消关注
     */
    String URL_CANCEL_FOLLOW = URL_BASE + "/space/canFollow.json";
    /**
     * 拉黑
     */
    String URL_PULL_TO_BLACK = URL_BASE + "/space/dragBlackList.json";
    /**
     * 取消拉黑
     */
    String URL_CANCEL_BLACK = URL_BASE + "/space/canceldragBlack.json";
    /**
     * 举报
     */
    String URL_REPORT = URL_BASE + "/space/report.json";
    /**
     * 下一位用户
     */
    String URL_NEXT_USER = URL_BASE + "/space/nextUsers.json";

}
