package com.yueai.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.yueai.R;
import com.yueai.bean.Image;
import com.yueai.bean.SeeMe;
import com.yueai.bean.UserBase;
import com.yueai.constant.IConfigConstant;

import java.util.List;

public class SeeMeAdapter extends CommonRecyclerViewAdapter<SeeMe> {

    public SeeMeAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public SeeMeAdapter(Context context, int layoutResId, List<SeeMe> list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, SeeMe bean) {
        if (bean != null) {
            UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                Image image = userBase.getImage();
                if (image != null) {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(image.getThumbnailUrl()).imageView((ImageView) viewHolder.getView(R.id.item_seeme_iv))
                            .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR).build());
                }
            }
        }
    }

}
