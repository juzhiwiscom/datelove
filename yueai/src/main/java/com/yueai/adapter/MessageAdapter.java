package com.yueai.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.TimeUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.activity.UserInfoDetailActivity;
import com.yueai.bean.Chat;
import com.yueai.bean.ChatMsg;
import com.yueai.bean.Image;
import com.yueai.bean.UserBase;
import com.yueai.constant.IConfigConstant;

import java.util.List;
/**
 * 聊天记录适配器
 * Created by zhangdroid on 2016/10/29.
 */
public class MessageAdapter extends CommonRecyclerViewAdapter<Chat> {

    private long mServerSystemTime;

    private Handler handler = new Handler();

    public MessageAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public MessageAdapter(Context context, int layoutResId, List list) {
        super(context, layoutResId, list);
    }

    public void setServerSystemTime(long serverSystemTime) {
        this.mServerSystemTime = serverSystemTime;
    }

    @Override
    protected void convert(int position, final RecyclerViewHolder viewHolder, Chat bean) {
        if (bean != null) {
            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_message_avatar);
                final TextView lastTime = (TextView)viewHolder.getView(R.id.item_message_last_time);
                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 查看详情
                        UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, userBase.getId(),
                                null, UserInfoDetailActivity.SOURCE_FROM_CHAT_LIST);
                    }
                });
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_message_nickname, nickname);
                }
                // 最新一条消息内容和时间
                final ChatMsg chatMsg = bean.getChat();
                if (chatMsg != null) {
                    String lastMsgContent = chatMsg.getLastContent();
                    if (!TextUtils.isEmpty(lastMsgContent)) {
                        viewHolder.setText(R.id.item_message_lastmsg, Util.convertText(lastMsgContent));
                    }
//                    viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime,
//                            chatMsg.getAddTimeMills()));
                    if ( chatMsg.getLastTimeMills()!=0) {
                        viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime,
                                chatMsg.getLastTimeMills()));
                    }else{
                        viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime,
                            chatMsg.getAddTimeMills()));
                    }

                    // 未读消息
                   int unreadCount=chatMsg.getUnreadCount();
                    viewHolder.getView(R.id.item_message_is_unread).setVisibility((unreadCount > 0) ? View.VISIBLE : View.GONE);
                    viewHolder.setText(R.id.item_message_is_unread,String.valueOf(unreadCount));
                    // 未读消息
//                    int unreadCount=chatMsg.getUnreadCount();
//                    if(unreadCount<=0){
//                        viewHolder.getView(R.id.item_message_is_unread).setVisibility(View.GONE);
//                        viewHolder.setText(R.id.item_message_lastmsg, Util.convertText(lastMsgContent));
////                        viewHolder.getView(R.id.item_message_lastmsg).setVisibility(View.GONE);
//                    }else{
//                        viewHolder.getView(R.id.item_message_is_unread).setVisibility(View.VISIBLE);
//                        viewHolder.setText(R.id.item_message_is_unread, String.valueOf(unreadCount));
//                        if (!TextUtils.isEmpty(lastMsgContent)){
//                            viewHolder.getView(R.id.item_message_lastmsg).setVisibility(View.VISIBLE);
//                            String get_msg = mContext.getResources().getString(R.string.get_msg);
//                            viewHolder.setText(R.id.item_message_lastmsg,get_msg);
//                        }
//
//                    }

//                     Runnable runnable = new Runnable() {
//                        public void run() {
//                            this.update();
//                            handler.postDelayed(this, 1000*20);// 间隔20秒
//                        }
//                        void update() {
//                           String updateTime= TimeUtil.getLocalTime(mContext, mServerSystemTime, chatMsg.getAddTimeMills());
//                            String text = (String) lastTime.getText();
//                            if(updateTime!=text){
//                                //刷新出最新时间
//                                viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime, chatMsg.getAddTimeMills()));
//                            }
//
//                        }
//                    };
//                    handler.postDelayed(runnable, 1000*20); //开始刷新

                }
            }
        }
    }

}













///**
// * 聊天记录适配器
// * Created by zhangdroid on 2016/10/29.
// */
//public class MessageAdapter extends CommonRecyclerViewAdapter<Chat> {
//    private long mServerSystemTime;
//
//    public MessageAdapter(Context context, int layoutResId) {
//        super(context, layoutResId);
//    }
//
//    public MessageAdapter(Context context, int layoutResId, List list) {
//        super(context, layoutResId, list);
//    }
//
//    public void setServerSystemTime(long serverSystemTime) {
//        this.mServerSystemTime = serverSystemTime;
//    }
//
//    @Override
//    protected void convert(int position, RecyclerViewHolder viewHolder, Chat bean) {
//        if (bean != null) {
//            final UserBase userBase = bean.getUserBaseEnglish();
//            if (userBase != null) {
//                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_message_avatar);
//                Image image = userBase.getImage();
//                if (image != null) {
//                    String imgUrl = image.getThumbnailUrl();
//                    if (!TextUtils.isEmpty(imgUrl)) {
//                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
//                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
//                    } else {
//                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
//                    }
//                } else {
//                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
//                }
//                ivAvatar.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // 查看详情
//                        UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, userBase.getId(),
//                                null, UserInfoDetailActivity.SOURCE_FROM_CHAT_LIST);
//                    }
//                });
//                // 昵称
//                String nickname = userBase.getNickName();
//                if (!TextUtils.isEmpty(nickname)) {
//                    viewHolder.setText(R.id.item_message_nickname, nickname);
//                }
//                // 最新一条消息内容和时间
//                ChatMsg chatMsg = bean.getChat();
//                if (chatMsg != null) {
//                    String lastMsgContent = chatMsg.getLastContent();
//                    if (!TextUtils.isEmpty(lastMsgContent)) {
//                        viewHolder.setText(R.id.item_message_lastmsg, Util.convertText(lastMsgContent));
//                    }
//                    viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime, chatMsg.getAddTimeMills()));
//                    // 未读消息
//                    viewHolder.getView(R.id.item_message_is_unread).setVisibility((chatMsg.getUnreadCount() > 0) ? View.VISIBLE : View.GONE);
//                }
//            }
//        }
//    }
//
//}
