package com.yueai.adapter;

import android.content.Context;

import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.yueai.R;
import com.yueai.bean.QaAnswer;

import java.util.List;

/**
 * 聊天QA信适配器
 * Created by zhangdroid on 2017/2/23.
 */
public class QaAnswerAdapter extends CommonRecyclerViewAdapter<QaAnswer> {

    public QaAnswerAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public QaAnswerAdapter(Context context, int layoutResId, List<QaAnswer> list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, QaAnswer bean) {
        if (bean != null) {
            viewHolder.setText(R.id.item_chat_qa_answer, bean.getContent());
        }
    }
}
